# Notes for medium article/thesis

## The story

* Javascript is an amazing language! There are many good things about it.
* But the language has some deeply annoying features.
* So I made a language that fixes many of JS's problems.
* But anyone can fix them, and, in fact, lots of people have.
* But what's special about my proposition is that it's compatible with JS.

## The structure (outline and notes)

I tried putting it together into a structure. I'm not sure if that structure
works exactly right, but it's a place to start. We should be able to edit this
monster both into an article and a thesis.

1. JS is good.

   * JS is the backbone of the Internet and the most popular language in
     general for many consecutive years according to polls.
   * The popularity is well-earned: the language has many features that make
     programming in it efficient. Examples, eg. comparing something
     exceptionally good in JS vs another big language. Especially good if JS
     beats another language at something this language at its own raison
     d'etre.
   * It also has a good standard library and an ecosystem of packages that
     support it. Examples.
2. But JS is bad.

   * Despite it's many advantages, JS has some serious problems.
     Examples of most obvious problems with small explanations, one par each.
   * Overall: explanation why these problems are there (language evolution,
     backwards compatibility, specific considerations).
   * This is not just my opinion. You can find people addressing this in
     reference, reference, reference.
   * I also conducted a small study among # professional JS devs asking about
     the behavior of some fairly common JS snippets and found that 37% of
     JavaScript developers are two kids in a trenchcoat (each).
3. How fixed.

   * These problems can be addressed by building on a language with a more
     consistsent syntax/semantics. As you may suspect, I built a language
     that does just that. But so what? Anyone can do that.
   * In fact many people have already tried fixing various problems with their
     own languages. For instance X fixes the problem Y described earlier by...
     A, fixes problem B with... P gets around Q by... (for the first draft
     this should be exhausive).
   * The existing solutions are bad and/ or insufficient in specific ways:
     explanations with examples.
   * Since a huge strength of JS is the multiplicity of frameworks and
     packagers that make up its ecosystem, it's also not enough to patch up
     the language itself, the trick is to ensure compatibility of the language
     and its constructs with the ecosystem as it stands.
4. How **I** fix.

   * I would like to present a language that fixes most problems I identified
     with JS. I call it JonScript.
   * But in addition, this language aims to be extreemely well and nigh
     seemlessly integrated into JS. It compiles into TS and JS (experimental)
     and takes great pains to ensure that objects passing the boundaries
     between the languages maintain whatever invariants are important. I also
     ensure that the houshold-names among JS frameworks work with JonScript.
5. JonScript Language

   * Comprehensive list of features and the JS problems that they fix with
     in-depth explanations. One subsection each.
6. I said it's compatible, but how do we **know**?

   * Comprehensive list of compatibility considerations with in-depth
     explanations and examples.
   * Explanation of compilation into JS and TS: how it works, what results are
     produced.
   * Examples of JS frameworks working with JnS.
7. Who needs a new language?

   * I can hear you complaining "I already know JS and learning some new
     language is hard, even if it is better?!" I would like to answer this in
     two parts: you don't know JS, and picking up JnS is, like, super easy.
     I'll explain in the next two sections.
   * I did a test where I asked JS programmers to test themselves against a
     gauntlet of JS semantics and syntax questions. Here are my findings. I
     did a similar test for JnS and here are my findings. (Hopefully) we can
     see JnS is more intuitive.
   * I did a test where I had JS programmers write small programs using JnS.
     Here is my analysis of what they wrote.
8. Concluding thoughts

   * Hopes and dreams
   * Lessons learned
   * Open questions and future work
   * Shout outs
   * Links to npm packages, manuals and things

Appendicies (in no particular order, mostly for thesis):

1. Installation guide for JnS
2. JnS spec (EBNF of parser, description of operational semantics, whatever is available)
3. Description of any tools: eg. code highlighters (whatever is available)
4. Full text and results of any surveys

Notes:

A sketch of the first intro apragraph I did as warm up. Discard, keep, or use
for ideas.

JavaScript is the language the Internet runs on. It is *de facto* the standard
language for web development both client-side, and, with the advent of node.js,
also server-side. In broader terms, it is the single most common programming
language in the world, consistently topping popularity charts for years [1].

[1] https://medium.com/javascript-scene/how-popular-is-javascript-in-2019-823712f7c4b1

# Materials

**Please watch this**. It's extremely good advice:

* How to Write Papers So People Can Read Them. https://www.youtube.com/watch?v=L_6xoMjFr70

Some references for you for technical writing that I recommend to everyone:

* How to write in plain English. http://www.plainenglish.co.uk/how-to-write-in-plain-english.html
* How to write a great research paper by  SPJ. https://www.youtube.com/watch?v=WP-FkUaOcOM
* Joseph Williams and Joseph Bizup. Style: Lessons in Clarity and Grace. https://tinyurl.com/y4uzznof

Less useful to you directly, but also informative, if you have the time and the need:

* How to Write a Thesis, According to Umberto Ecco. https://thereader.mitpress.mit.edu/umberto-eco-how-to-write-a-thesis/
* Learning Technical Writing Using the Engineering Method. A Handbook for Groups. https://www.cs.tufts.edu/~nr/pubs/learn.pdf
