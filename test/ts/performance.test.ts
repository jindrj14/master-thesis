import { parseJonScriptTS as parseJonScript } from '../../dist/testExport';
import { saveFileOnly } from './util/run';
import { Test } from './testproject/src/jons-temp/performanceTest';
import { Test2 } from './testproject/src/jons-temp/performanceTestFib';
import { Test3 } from './testproject/src/jons-temp/performanceTestLang';
import { writeFileSync } from 'fs';

describe("Performance testing", () => {
    test("Merge sort speed test", async () => {
        // % Language, Iteration n., algorithm, Elapsed time in ms.
        const jonScriptMergeSort = parseJonScript(`
            Test {
                TestClass(array: number[]) {
                    jsMergeArrays: (a: number[], b: number[]): number[] => !a.length
                    ? b
                    : (
                        !b.length
                            ? a
                            : (
                                a[0] < b[0]
                                    ? [a[0], ...jsMergeArrays(a.slice(1), b)]
                                    : [b[0], ...jsMergeArrays(a, b.slice(1))]
                                )
                    ),
                    jsFirstHalf: (array: number[]) => array.slice(0, Math.floor(array.length / 2)),
                    jsSecondHalf: (array: number[]) => array.slice(Math.floor(array.length / 2)),
                    jsMergeSort: (array: number[]): number[] => array.length == 1 ? array : jsMergeArrays(
                        jsMergeSort(jsFirstHalf(array)),
                        jsMergeSort(jsSecondHalf(array)),
                    ),
                    {
                        test: () => Array<number>(100).fill(0).map((_, i) => {
                            perf: Date().getTime(),
                            _: jsMergeSort(array),
                            \`JonScript,\${i},Merge Sort (700 elements),\${Date().getTime() - perf};\`
                        }),
                    }
                }
            }
        `, true);
        const jsMergeArrays = (a: number[], b: number[]): number[] => !a.length
            ? b
            : (
                !b.length
                    ? a
                    : (
                        a[0] < b[0]
                            ? [a[0], ...jsMergeArrays(a.slice(1), b)]
                            : [b[0], ...jsMergeArrays(a, b.slice(1))]
                        )
            );
        const jsFirstHalf = (array: number[]) => array.slice(0, Math.floor(array.length / 2));
        const jsSecondHalf = (array: number[]) => array.slice(Math.floor(array.length / 2));
        const jsMergeSort = (array: number[]): number[] => array.length <= 1
            ? array
            : jsMergeArrays(
                jsMergeSort(jsFirstHalf(array)),
                jsMergeSort(jsSecondHalf(array)),
            );
        const arrayGenerator = new Array(700).fill(0).map(() => Math.floor(Math.random() * 10));
        // Ensure merge sort works
        expect(jsMergeSort(arrayGenerator)).toEqual([...arrayGenerator].sort());
        // JonScript run
        saveFileOnly(jonScriptMergeSort, "performanceTest");
        writeFileSync("./test/ts/performance/jonscript-mergesort.csv", Test.TestClass(arrayGenerator).test().join("\n"));
        // Javascript run
        writeFileSync("./test/ts/performance/javascript-mergesort.csv", Array(100).fill(0).map((_, i) => {
            const perf = new Date().getTime();
            jsMergeSort(arrayGenerator);
            return `JavaScript,${i},Merge Sort (700 elements),${new Date().getTime() - perf};`;
        }).join("\n"));
    });
    test("Fibonnaci speed test", async () => {
        // % Language, Iteration n., algorithm, Elapsed time in ms.
        const jonScriptFibonacci = parseJonScript(`
            Test2 {
                TestClass2(testnum: number) {
                    fibonacci: (test: number, start: number = 0, end: number = 1): number => !test ? start : fibonacci(test - 1, end, start + end),
                    {
                        test: () => Array<number>(100).fill(0).map((_, i) => {
                            perf: Date().getTime(),
                            _: Array<number>(100).fill(0).forEach(() => fibonacci(testnum)),
                            \`JonScript,\${i},5000th fibonacci number (100 times over),\${Date().getTime() - perf};\`
                        }),
                    }
                }
            }
        `, true);
        const fibonacci = (test: number, start: number = 0, end: number = 1): number => !test ? start : fibonacci(test - 1, end, start + end);
        const randomNumber = 5000;
        // Ensure fib works
        expect(fibonacci(40)).toEqual(102334155);
        // JonScript run
        saveFileOnly(jonScriptFibonacci, "performanceTestFib");
        writeFileSync("./test/ts/performance/jonscript-fib.csv", Test2.TestClass2(randomNumber).test().join("\n"));
        // Javascript run
        writeFileSync("./test/ts/performance/javascript-fib.csv", Array(100).fill(0).map((_, i) => {
            const perf = new Date().getTime();
            Array<number>(100).fill(0).forEach(() => fibonacci(randomNumber))
            return `JavaScript,${i},5000th fibonacci number (100 times over),${new Date().getTime() - perf};`;
        }).join("\n"));
    });
    /*
        An ant moves on a regular grid of squares that are coloured either black or white.
        The ant is always oriented in one of the cardinal directions (left, right, up or down) and moves from square to adjacent square according to the following rules:
        - if it is on a black square, it flips the colour of the square to white, rotates 90 degrees counterclockwise and moves forward one square.
        - if it is on a white square, it flips the colour of the square to black, rotates 90 degrees clockwise and moves forward one square.

        Starting with a grid that is entirely white, how many squares are black after 10^18 moves of the ant?
    */
    test("Langtons ant speed test", async () => {
        // % Language, Iteration n., algorithm, Elapsed time in ms.
        const jonScriptLangton = parseJonScript(`
            Test3 {
                PureTupleSet(set: Set<string> = Set<string>()) {
                    tupleString: (tuple: [number, number]) => \`\${tuple[0]}/\${tuple[1]}\`,
                    {
                        add: (tuple: [number, number]) => PureTupleSet(Set([tupleString(tuple), ...Array.from(set.values())])),
                        del: (tuple: [number, number]) => {
                            remove: tupleString(tuple),
                            PureTupleSet(
                                Set(
                                    Array.from(
                                        set.values()
                                    ).filter(
                                        value => value != remove
                                    )
                                )
                            )
                        },
                        has: (tuple: [number, number]) => set.has(tupleString(tuple)),
                        size: () => set.size,
                    }
                }
                Direction(
                    cardinality: "up" | "left" | "down" | "right" = "up",
                    x: number = 0,
                    y: number = 0,
                ) {
                    mapper: {{
                        up: {{
                            clockwise: () => Direction("left", x + 1, y),
                            counter: () => Direction("right", x - 1, y),
                        }},
                        left: {{
                            clockwise: () => Direction("down", x, y - 1),
                            counter: () => Direction("up", x, y + 1),
                        }},
                        down: {{
                            clockwise: () => Direction("right", x - 1, y),
                            counter: () => Direction("left", x + 1, y),
                        }},
                        right: {{
                            clockwise: () => Direction("up", x, y + 1),
                            counter: () => Direction("down", x, y - 1),
                        }},
                    }},
                    {
                        clockwise: () => mapper[cardinality].clockwise(),
                        counter: () => mapper[cardinality].counter(),
                        coords: (): [number, number] => [x, y],
                    }
                }
                TestClass3(testnum: number) {
                    lang: (
                        testnum: number,
                        steppedOn = PureTupleSet(),
                        direction = Direction(),
                    ): number => {
                        isBlack: steppedOn.has(direction.coords()),
                        !testnum ? steppedOn.size() : lang(
                            testnum - 1,
                            isBlack
                                ? steppedOn.del(direction.coords())
                                : steppedOn.add(direction.coords()),
                            isBlack
                                ? direction.clockwise()
                                : direction.counter(),
                        )
                    },
                    {
                        test: () => Array<number>(100).fill(0).map((_, i) => {
                            perf: Date().getTime(),
                            _: lang(testnum),
                            \`JonScript,\${i},Langtons ant 100,\${Date().getTime() - perf};\`
                        }),
                    }
                }
            }
        `, true);
        class PureTupleSet {
            private set: Set<string>;
            constructor(set: Set<string> = new Set<string>()) {
                this.set = set;
            }
            private tupleString = (tuple: [number, number]) => `${tuple[0]}/${tuple[1]}`;
            public add = (tuple: [number, number]) => new PureTupleSet(new Set([this.tupleString(tuple), ...this.set.values()]));
            public del = (tuple: [number, number]) => {
                const remove = this.tupleString(tuple);
                return new PureTupleSet(new Set(Array.from(this.set.values()).filter(value => value != remove)));
            };
            public has = (tuple: [number, number]) => this.set.has(this.tupleString(tuple));
            public size = () => this.set.size;
        }
        type Cardinality = "up" | "left" | "down" | "right";
        class Direction {
            cardinality: Cardinality;
            x: number;
            y: number;
            constructor(
                cardinality: Cardinality  = "up",
                x: number = 0,
                y: number = 0,
            ) {
                this.cardinality = cardinality;
                this.x = x;
                this.y = y;
            }
            private mapper = {
                up: {
                    clockwise: () => new Direction("left", this.x + 1, this.y),
                    counter: () => new Direction("right", this.x - 1, this.y),
                },
                left: {
                    clockwise: () => new Direction("down", this.x, this.y - 1),
                    counter: () => new Direction("up", this.x, this.y + 1),
                },
                down: {
                    clockwise: () => new Direction("right", this.x - 1, this.y),
                    counter: () => new Direction("left", this.x + 1, this.y),
                },
                right: {
                    clockwise: () => new Direction("up", this.x, this.y + 1),
                    counter: () => new Direction("down", this.x, this.y - 1),
                },
            };
            public clockwise = () => this.mapper[this.cardinality].clockwise();
            public counter = () => this.mapper[this.cardinality].counter();
            public coords = (): [number, number] => [this.x, this.y];
        }
        const lang = (
            testnum: number,
            steppedOn = new PureTupleSet(),
            direction = new Direction(),
        ): number => {
            const isBlack = steppedOn.has(direction.coords());
            return !testnum ? steppedOn.size() : lang(
                testnum - 1,
                isBlack
                    ? steppedOn.del(direction.coords())
                    : steppedOn.add(direction.coords()),
                isBlack
                    ? direction.clockwise()
                    : direction.counter(),
            )
        }
        const randomNumber = 1000;
        // Ensure lang works
        expect(lang(40)).toEqual(12);
        // JonScript run
        saveFileOnly(jonScriptLangton, "performanceTestLang");
        writeFileSync("./test/ts/performance/jonscript-lang.csv", Test3.TestClass3(randomNumber).test().join("\n"));
        // Javascript run
        writeFileSync("./test/ts/performance/javascript-lang.csv", Array(100).fill(0).map((_, i) => {
            const perf = new Date().getTime();
            lang(randomNumber);
            return `JavaScript,${i},Langtons ant 100,${new Date().getTime() - perf};`;
        }).join("\n"));
    });
});