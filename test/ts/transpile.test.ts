import { parseJonScriptTS as parseJonScript } from '../../dist/testExport';

describe("Can create basic class structure in Typescript", () => {
    test("Can create empty module", () => {
        expect(parseJonScript(`
            Test {
            
            }
        `)).toMatchSnapshot();
    });
    test("Can create empty class", () => {
        expect(parseJonScript(`
            Test {
                TestClass() {

                }
            }
        `).replace(/\n/g, '')).toMatchSnapshot();
    });
    test("Can create basic typed constructor", () => {
        expect(parseJonScript(`
            Test {
                TestClass(first: string, second, third: number) {

                }
            }
        `).replace(/\n/g, '')).toMatchSnapshot();
    });
    test("Can create basic empty return value from class", () => {
        expect(parseJonScript(`
            Test {
                TestClass(first: string, second, third: number) {
                    {

                    }
                }
            }
        `).replace(/\n/g, '')).toMatchSnapshot();
    });
    test("Can create basic empty return functor from class", () => {
        expect(parseJonScript(`
            Test {
                TestClass(first: string, second, third: number) {
                    () => {

                    }
                }
            }
        `).replace(/\n/g, '')).toMatchSnapshot();
    });
    test("Can create empty class with private variables", () => {
        expect(parseJonScript(`
            Test {
                TestClass(first: string, second, third: number) {
                    first: second,
                    fourth: 1,
                }
            }
        `).replace(/\n/g, '')).toMatchSnapshot();
    });
    test("Can create class with functor returning an expression", () => {
        expect(parseJonScript(`
            Test {
                TestClass() {
                    () => 2 + 5
                }
            }
        `).replace(/\n/g, '')).toMatchSnapshot();
    });
    test("Can create class with with public and private api", () => {
        expect(parseJonScript(`
            Test {
                TestClass(first: number) {
                    second: first + 1,
                    {
                        third: second + 2,
                    }
                }
            }
        `).replace(/\n/g, '')).toMatchSnapshot();
    });
    test("Can use inheritance", () => {
        expect(parseJonScript(`
            Test {
                TestClass1() {
                    {
                        five: 2,
                        six: 5,
                        seven: 7,
                    }
                }
                TestClass2() {
                    one: 2,
                    ...TestClass1(),
                    {
                        eight: one + five + six + seven,
                    }
                }
            }
        `).replace(/\n/g, '')).toMatchSnapshot();
    });
    test("Can use grandparent inheritance", () => {
        expect(parseJonScript(`
            Test {
                TestClass1() {
                    {
                        five: 2,
                        six: 5,
                        seven: 7,
                    }
                }
                TestClass2() {
                    one: 2,
                    ...TestClass1(),
                    {
                        eight: one + five + six + seven,
                    }
                }
                OtherTestClass() {
                    {
                        eleven: 11,
                    }
                }
                TestClass3() {
                    ...TestClass2(),
                    ...OtherTestClass(),
                    {
                        nine: (eight * 2) + eleven,
                    }
                }
            }
        `).replace(/\n/g, '')).toMatchSnapshot();
    })
    test("Can use object access", () => {
        expect(parseJonScript(`
            Test {
                TestClass1() {
                    {
                        five: 2,
                        six: 5,
                        seven: 7,
                    }
                }
                TestClass2() {
                    one: TestClass1(),
                    {
                        eight: one.five,
                        nine: one.five.seven,
                    }
                }
            }
        `).replace(/\n/g, '')).toMatchSnapshot();
    });
    test("Can use array access", () => {
        expect(parseJonScript(`
            Test {
                TestClass() {
                    one: [1, 2, 3, [4, 5]],
                    {
                        eight: one[1],
                        nine: one[3][1],
                    }
                }
            }
        `).replace(/\n/g, '')).toMatchSnapshot();
    });
    test("Can use placeholders", () => {
        expect(parseJonScript(`
            Test {
                TestClass(first: number, _, _, _) {
                    _: console.log(first),
                    {
                        placeholder: (_, _, third: number) => third,
                    }
                }
            }
        `).replace(/\n/g, '')).toMatchSnapshot();
    });
    test("Can use strings", () => {
        expect(parseJonScript(`
            Test {
                TestClass(first: number) {
                    () => "Hello" + \`World \${first}\` + \`\` + \` nicely \${first} interpolated\`
                }
            }
        `).replace(/\n/g, '')).toMatchSnapshot();
    });
    test("Objects can inherit from classes", () => {
        expect(parseJonScript(`
            Test {
                TestClass(first: number) {
                    {
                        first,
                    }
                }
                TestClass2() {
                    one: {
                        two: 2,
                        ...TestClass(two),
                        two + first
                    },
                    () => one
                }
            }
        `).replace(/\n/g, '')).toMatchSnapshot();
    });
    test("Can use currying", () => {
        expect(parseJonScript(`
            Test {
                TestClass(first: number) {
                    curry: () => (second: number) => first + second,
                    {
                        curryOn: () => (third: number) => curry(2) + third,
                    }
                }
                TestClass2(first: number) {
                    curry: () => (second: number) => first + second,
                    () => (third: number) => curry(2) + third
                }
            }
        `).replace(/\n/g, '')).toMatchSnapshot();
    });
    test("Can define function return type", () => {
        expect(parseJonScript(`
            Test {
                TestClass() {
                    priv: (): number => 0,
                    (): number => 2 + 5
                }
            }
        `).replace(/\n/g, '')).toMatchSnapshot();
    });
    test("Can call other classes", () => {
        expect(parseJonScript(`
            Test {
                TestClass(num: number) {
                    () => num + 1
                }
                TestClass1(num: number) {
                    () => num + 2
                }
                TestClass2() {
                    testClass: TestClass1(5),
                    (): number => TestClass(2)() + testClass()
                }
            }
        `).replace(/\n/g, '')).toMatchSnapshot();
    });
    test("Complex test", () => {
        expect(parseJonScript(`
            import $ from "jquery"
            import { Questions } from "./questions"
            
            Quiz {
                Run() {
                    questions: [Questions.Question1(), Questions.Question2(), Questions.Question3(), Questions.Question4(), Questions.Question5()],
                    getWrongAnswers: () => questions.filter(f => !f.isCorrect()),
                    checkAnswers: () => {
                        wrongAnswers: getWrongAnswers(),
                        getWrongAnswers.length > 0 ? alert(\`Vyskytly se chyby: \${JSON.stringify(wrongAnswers)}\`) : alert("Vše správně")
                    },
                    _: questions.map(f => f()),
                    _: $(".root").append(\`
                        <button>
                            Zkontrolovat
                        </button>
                    \`),
                    _: $(".root button").on("click", checkAnswers),
                }
            }
        `).replace(/\n/g, '')).toMatchSnapshot();
    });
    test("Multiline string test", () => {
        expect(parseJonScript(`
            Test {
                TestClass() {
                    fn: (value: string): string => value,
                    {
                        test: () => fn(\`
                            <div id="six">
                                This is some html!
                                <span>
                                    :( throw me a catch ball with finally the right paint + size,
                                </span>
                            </div>\`).replace("html", "text"),
                    }
                }
            }
        `).replace(/\n/g, '')).toMatchSnapshot();
    });
    test("Test with escaped quotes", () => {
        expect(parseJonScript(`
        Test {
            TestClass() {
                b: "\\"Hello\\"",
            }
        }
        `).replace(/\n/g, '')).toMatchSnapshot();
    });
    test("Long interpolated string with 3 pauses", () => {
        expect(parseJonScript(`
            Questions {
                QuestionBuilder(
                    correct: string,
                    answerId: string,
                    questions: Class<Question>[]
                ) {{
                    isCorrect: () => correct == $(\`#\${answerId}\`).find("[checked='true']").parent().attr("id"),
                    createQuestion: () => $(".root").append(\`
                        <ul id="\${answerId}">
                            \${questions.map(q => \`
                                <li id="\${q.id}">
                                    <label>\${q.label}</label>
                                    <input type="radio" name="\${answerId}" />\\\`Test\\\`
                                </li>
                            \`).join("")}
                        </ul>
                    \`),
                }}
            }
        `).replace(/\n/g, '')).toMatchSnapshot();
    });
    test("Functor type combination", () => {
        expect(parseJonScript(`
            Module {
                Test() {
                    f: {
                        e: -5,
                        {
                            (g: string, k: string) => g + k,
                            ll: 21,
                        }
                    },
                    {
                        (a: number, b: number) => a + b,
                        c: 10,
                        d: 11,
                    }
                }
            }
        `).replace(/\n/g, '')).toMatchSnapshot();
    });
    test("Can define function types inline", () => {
        expect(parseJonScript(`
            Module {
                Test(fn: <T extends string = "Hello Kitty", E = number>(param1: T, param2: E) => T & E & []) {
                    
                }
            }
        `).replace(/\n/g, '')).toMatchSnapshot();
    });
    test("Can derive function interface from derived class", () => {
        expect(parseJonScript(`
            Module {
                T1() {
                    () => 2
                }
                T2() {{
                    a: 5,
                }}
                T3() {
                    ...T1(),
                }
                T4() {
                    ...T2(),
                    ...T1(),
                }
                T5() {
                    ...T1(),
                    ...T2(),
                }
                T6() {
                    ...T2(),
                    ...T1(),
                    {
                        b: 6,
                    }
                }
                T7() {
                    ...T1(),
                    ...T2(),
                    () => 3
                }
            }
        `).replace(/\n/g, '')).toMatchSnapshot();
    });
});