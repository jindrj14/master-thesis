import { _o, _sanitize, _sanitizeObject, _is, _safeCopy } from "jonscript-util";

describe("Can use standard api functions", () => {
    test("Can cleanup objects", () => {
        expect(_sanitizeObject({ a: 5, b: undefined, c: 0, d: false, e: undefined })).toEqual({ a: 5, c: 0, d: false });
    });
    test("Can cleanup expressions", () => {
        expect(_sanitize(false)).toBe(false);
        expect(_sanitize(true)).toBe(true);
        expect(_sanitize({})).toEqual({});
        expect(_sanitize([])).toEqual([]);
        expect(_sanitize(undefined)).toBe(undefined);
        expect(_sanitize([1, 2])).toEqual([1, 2]);
        expect(_sanitize({ a: 5 })).toEqual({ a: 5 });
        expect(_sanitize(NaN)).toBe(undefined);
        expect(_sanitize(null)).toBe(undefined);
        expect(_sanitize("")).toBe("");
        expect(_sanitize("abc")).toBe("abc");
    });
    test("Can use operator overloading +", () => {
        type Valid = {
            value: number;
            plus: (b: Valid) => Valid;
        };
        const createValidPlus = (val: number) => {
            const plus = (obj: Valid) => (next: Valid): Valid => {
                const toReturn: Partial<Valid> = {
                    value: obj.value + next.value,
                };
                toReturn.plus = plus(toReturn as Valid);
                return toReturn as Valid;
            };
            return plus({ value: val } as Valid)({ value: 0 } as Valid);
        };
        
        expect(_o.plus(4, 5)).toEqual(9);
        expect(_o.plus(4, "5")).toEqual("45");
        expect(_o.plus(new String("4"), 5).valueOf()).toEqual("45");
        expect(_o.plus(4, new String("5")).valueOf()).toEqual("45");
        expect(_o.plus(new Number(4), new Number(5)).valueOf()).toEqual(9);
        expect(_o.plus(new String(4), new String(5)).valueOf()).toEqual("45");
        expect(_o.plus(new Number(4), new String(5)).valueOf()).toEqual("45");
        expect(_o.plus(null, 5)).toEqual(undefined);
        expect(_o.plus(4, null)).toEqual(undefined);
        expect(_o.plus(null, "5")).toEqual("5");
        expect(_o.plus("4", null)).toEqual("4");
        expect(_o.plus(undefined, 5)).toEqual(undefined);
        expect(_o.plus(undefined, "5")).toEqual("5");
        expect(_o.plus(false, 5 as any)).toEqual(undefined);
        expect(_o.plus("5", "6")).toEqual("56");
        expect(_o.plus(createValidPlus(5), createValidPlus(6)).value).toBe(11);
    });
    test("Can use equals sign operator overloading", () => {
        type Valid = {
            value: number;
            compare: (b: Valid) => number;
        };
        const createValidEquals = (val: number): Valid => {
            return {
                value: val,
                compare: b => b.value - val,
            };
        };
        
        expect(_o.equals(4, 5)).toEqual(false);
        expect(_o.equals(4, 4)).toEqual(true);
        expect(_o.equals(4, new Number(4))).toEqual(true);
        expect(_o.equals(4, "5")).toEqual(false);
        expect(_o.equals("5", "5")).toEqual(true);
        expect(_o.equals("5", new String("5"))).toEqual(true);
        expect(_o.equals(new String("4"), 5)).toEqual(false);
        expect(_o.equals(4, new String("5"))).toEqual(false);
        expect(_o.equals(new Number(4), new Number(5))).toEqual(false);
        expect(_o.equals(new String(4), new String(5))).toEqual(false);
        expect(_o.equals(new String(4), new String(4))).toEqual(true);
        expect(_o.equals(new Number(4), new String(5))).toEqual(false);
        expect(_o.equals(new Number(4), new String(4))).toEqual(false);
        expect(_o.equals(null, 5)).toEqual(false);
        expect(_o.equals(4, null)).toEqual(false);
        expect(_o.equals(undefined, 5)).toEqual(false);
        expect(_o.equals(false, 5 as any)).toEqual(false);
        expect(_o.equals(false, undefined)).toEqual(false);
        expect(_o.equals(false, null)).toEqual(false);
        expect(_o.equals("5", "6")).toEqual(false);
        expect(_o.equals(createValidEquals(5), createValidEquals(6))).toBe(false);
        expect(_o.equals(createValidEquals(5), createValidEquals(5))).toBe(true);
    });
    test("Can use greater sign operator overloading", () => {
        type Valid = {
            value: number;
            compare: (b: Valid) => number;
        };
        const createValidEquals = (val: number): Valid => {
            return {
                value: val,
                compare: b => val - b.value,
            };
        };
        
        expect(_o.greater(4, 5)).toEqual(false);
        expect(_o.greater(4, 4)).toEqual(false);
        expect(_o.greater(5, 4)).toEqual(true);
        expect(_o.greater("4", "5")).toEqual(false);
        expect(_o.greater("b", "a")).toEqual(true);
        expect(_o.greater("c", "dabble")).toEqual(false);
        expect(_o.less("4", "5")).toEqual(true);
        expect(_o.less("b", "a")).toEqual(false);
        expect(_o.less("c", "dabble")).toEqual(true);
        expect(_o.greater(createValidEquals(5), createValidEquals(6))).toBe(false);
        expect(_o.greater(createValidEquals(6), createValidEquals(5))).toBe(true);
        expect(_o.greater(createValidEquals(5), createValidEquals(5))).toBe(false);
    });
    test("Will not compare objects nonsensically", () => {
        const o = {};
        const o1 = {};
        expect(_o.greater(o, o1)).toEqual(false);
        expect(_o.less(o, o1)).toEqual(false);
        expect(_o.greaterOrEquals(o, o1)).toEqual(false);
        expect(_o.lessOrEquals(o, o1)).toEqual(false);
        expect(_o.equals(o, o1)).toEqual(false);
        expect(_o.nequals(o, o1)).toEqual(true);
        expect(_o.equals(o, o)).toEqual(true);
        expect(_o.nequals(o, o)).toEqual(false);
    });
    test("Can use operator overloading /", () => {
        type Valid = {
            value: number;
            divide: (b: Valid) => Valid;
        };
        const createValidDivide = (val: number) => {
            const divide = (obj: Valid) => (next: Valid): Valid => {
                const toReturn: Partial<Valid> = {
                    value: obj.value / next.value,
                };
                toReturn.divide = divide(toReturn as Valid);
                return toReturn as Valid;
            };
            return divide({ value: val } as Valid)({ value: 1 } as Valid);
        };
        
        expect(_o.divide(6, 2)).toEqual(3);
        expect(_o.divide(new Number(6), 2).valueOf()).toEqual(3);
        expect(_o.divide(6, new Number(2)).valueOf()).toEqual(3);
        expect(_o.divide(new Number(6), new Number(2)).valueOf()).toEqual(3);
        expect(_o.divide(null, 5)).toEqual(undefined);
        expect(_o.divide(4, null)).toEqual(undefined);
        expect(_o.divide(undefined, 5)).toEqual(undefined);
        expect(_o.divide(false, 5 as any)).toEqual(undefined);
        expect(_o.divide(createValidDivide(6), createValidDivide(3)).value).toBe(2);
    });
    test("Can use basic pattern matching to determine string/number/boolean", () => {
        expect(_is("", Object)).toBe(false);
        expect(_is("", String)).toBe(true);
        expect(_is("", Boolean)).toBe(false);
        ///@ts-ignore
        expect(_is("", Number)).toBe(false);
        expect(_is(0, Number)).toBe(true);
        expect(_is(0, Boolean)).toBe(false);
        ///@ts-ignore
        expect(_is(0, String)).toBe(false);
        expect(_is(true, Boolean)).toBe(true);
        ///@ts-ignore
        expect(_is(true, Number)).toBe(false);
        ///@ts-ignore
        expect(_is(true, String)).toBe(false);
    });
    test("Can use basic patter matching to determine arrays and objects", () => {
        expect(_is([1, 2, 3], Array)).toBe(true);
        expect(_is([], Array)).toBe(true);
        expect(_is([1, 2, 3], [1, 2, 3])).toBe(true);
        expect(_is([1, 2, 3], [1, 2, 3, 4])).toBe(false);
        expect(_is([1, 2, 3], [1, 2])).toBe(true);
        expect(_is({ a: 6 }, {})).toBe(true);
        expect(_is({ a: 6 }, { a: 6 })).toBe(true);
        expect(_is({ a: 7 }, { a: 6 })).toBe(false);
        ///@ts-ignore
        expect(_is({ b: 6 }, { a: 6 })).toBe(false);
    });
    test("Safe copy does not taint the object functions", () => {
        expect(_safeCopy(new Number(5)) instanceof Number).toBe(true);
        expect(_safeCopy(new Number(5)).toString()).toBe("5");
        expect(_safeCopy(new Number(5)).toFixed()).toBe((new Number(5)).toFixed());
        expect(_safeCopy(new Number(5)).toLocaleString()).toBe((new Number(5)).toLocaleString());
        expect(_safeCopy(new Number(5)).toPrecision()).toBe((new Number(5)).toPrecision());
        expect(_safeCopy(new Number(5)).valueOf()).toBe(5);
        expect(_safeCopy(new Boolean(true)).valueOf()).toBe(true);
        expect(_safeCopy(new String("5")).valueOf()).toBe("5");
        expect(_safeCopy({ a: 5, b: 7 })).toEqual({ a: 5, b: 7 });
        expect((() => {
            const arr = _safeCopy([1, 2]);
            arr.push(3);
            return arr[2];
        })()).toBe(3);
        expect((() => {
            const orig = { a: 7 };
            const copy = _safeCopy(orig);
            (copy as any).b = 3; 
            return orig;
        })()).toEqual({ a: 7 });
        expect(_safeCopy([1, 2, 3]).map(i => i + 1)).toEqual([2, 3, 4]);
        expect(_safeCopy(5)).toBe(5);
        expect(_safeCopy(true)).toBe(true);
        expect(_safeCopy("5")).toBe("5");
        expect(_safeCopy(() => 5)()).toBe(5);
    });
});