import { parseJonScriptTS as parseJonScript } from '../../dist/testExport';
import { run } from './util/run';

describe("Can run typescript transpiled code and return correct result", () => {
    describe("Can overload the + operator", () => {
        test("Overload +", async () => {
            expect(await run(parseJonScript(`
                Test {
                    TestClass(val: number) {
                        {
                            value: val,
                            plus: (next: Class<TestClass>) => TestClass(next.value + value),
                        }
                    }
                    TestClass2(a: number, b: number) {
                        testA: TestClass(a),
                        testB: TestClass(b),
                        {
                            result: () => (testA + testB).value,
                        }
                    }
                }
            `, true), {
                requireName: "addition",
                className: "TestClass2",
                construct: [6, 7],
                methodName: "result",
                params: [],
            }, false)).toBe(13);
        });
    });
    describe("Can avoid NaN in code", () => {
        test("Overload division operator and then convert resulting NaN to undefined", async () => {
            expect(await run(parseJonScript(`
                Test {
                    TestClass(val: number) {
                        {
                            value: val,
                            divide: (next: Class<TestClass>) => TestClass(value / next.value),
                        }
                    }
                    TestClass2(a: number, b: number) {
                        testA: TestClass(a),
                        testB: TestClass(b),
                        {
                            result: () => (testA / testB).value / Infinity,
                        }
                    }
                }
            `, true), {
                requireName: "division",
                className: "TestClass2",
                construct: [6, 0],
                methodName: "result",
                params: [],
            }, false)).toBeUndefined();
        });
    });
    describe("Can do fibonnaci", () => {
        test("Can calculate an item from fibonnaci sequence", async () => {
            expect(await run(parseJonScript(`
                Test {
                    TestClass(len: number) {
                        calc: (len: number, first: number, second: number): number => len == 0 ? second : calc(len - 1, second, first + second),
                        () => calc(len, 0, 1)
                    }
                    TestClass2(len: number) {
                        {
                            test: () => TestClass(len)(),
                        }
                    }
                }
            `, true), {
                requireName: "fib",
                className: "TestClass2",
                construct: [5],
                methodName: "test",
                params: [],
            }, false)).toBe(8);
        });
    });
    describe("Can work with arrays", () => {
        test("Can use standard JavaScript API function reduce on array", async () => {
            expect(await run(parseJonScript(`
                Test {
                    TestClass(array: number[]) {
                        () => array.reduce((p: number, c) => p + c, 0)
                    }
                    TestClass2() {
                        {
                            test: (array: number[]) => TestClass(array)(),
                        }
                    }
                }
            `, true), {
                requireName: "reduce",
                className: "TestClass2",
                construct: [],
                methodName: "test",
                params: [[1, 2, 3]],
            }, false)).toBe(6);
        });
    });
    describe("Is forgiving", () => {
        test("Will not crash when accessing properties on undefined value", async () => {
            expect(await run(parseJonScript(`
                Test {
                    TestClass(object: any) {
                        {
                            test: () => {
                                {
                                    a: object.a,
                                    b: object.b,
                                    c: object.c.value,
                                    d: object.c.d.value,
                                    e: object.c.d.e,
                                }
                            },
                        }
                    }
                }
            `, true), {
                requireName: "forgive",
                className: "TestClass",
                construct: [{ a: 5, b: 7, c: { d: { e: 7 } } }],
                methodName: "test",
                params: [],
            }, false)).toEqual({ a: 5, b: 7, e: 7 });
        });
    });
    describe("Can do qsort", () => {
        test("Can run custom qsort algorithm", async () => {
            expect(await run(parseJonScript(`
                Test {
                    TestClass(array: number[]) {
                        concat: (a: number[], b: number[], c: number[]) => a.concat(b).concat(c),
                        getLessThan: (a: number[], than: number) => a.filter((val) => val < than),
                        getMoreThan: (a: number[], than: number) => a.filter((val) => val > than),
                        getEquals: (a: number[], than: number) => a.filter((val) => val == than),
                        run: (arr: number[]): number[] => arr.length == getEquals(arr, arr[0]).length
                            ? arr
                            : concat(
                                run(getLessThan(arr, arr[0])),
                                run(getEquals(arr, arr[0])),
                                run(getMoreThan(arr, arr[0]))
                            ),
                        () => run(array)
                    }
                    TestClass2() {
                        {
                            test: (array: number[]) => TestClass(array)(),
                        }
                    }
                }
            `, true), {
                requireName: "qsort",
                className: "TestClass2",
                construct: [],
                methodName: "test",
                params: [[2, -1, 3, 0, 0, 0, -4, 6, -7]],
            }, false)).toEqual([-7, -4, -1, 0, 0, 0, 2, 3, 6]);
        });
    });
    describe("Carry over inheritance from functor", () => {
        test("Can use inheritance, inherits function public API correctly", async () => {
            expect(await run(parseJonScript(`
                Test {
                    TestClass() {
                        {
                            a: 1,
                            b: 2,
                        }
                    }
                    TestClass2() {
                        ...TestClass(),
                        () => a + b
                    }
                    TestClass3() {
                        ...TestClass2(),
                        {
                            c: () => (a * 2) + b,
                        }
                     }
                }
            `, true), {
                requireName: "carry",
                className: "TestClass3",
                construct: [],
                methodName: "c",
                params: [],
            }, false)).toBe(4);
        });
    });
    describe("Test imports from typescript files", () => {
        test("Can import local TypeScript files", async () => {
            expect(await run(parseJonScript(`
                import { tsTest } from "./util/tsTest"
                Test {
                    TestClass() {
                        {
                            test: () => tsTest([1, 2, 3]),
                        }
                    }
                }
            `, true), {
                requireName: "importTest",
                className: "TestClass",
                construct: [],
                methodName: "test",
                params: [],
            }, false)).toEqual([2, 3, 4]);
        });
    });
    describe("Test renamed imports from typescript files", () => {
        test("Can do a named import and then rename it from TypeScript file", async () => {
            expect(await run(parseJonScript(`
                import { tsTest as otherTest } from "./util/tsTest"
                Test {
                    TestClass() {
                        {
                            test: () => otherTest([1, 2, 3]),
                        }
                    }
                }
            `, true), {
                requireName: "importTest",
                className: "TestClass",
                construct: [],
                methodName: "test",
                params: [],
            }, false)).toEqual([2, 3, 4]);
        });
    });
    describe("Test multiple renamed imports from typescript files", () => {
        test("Can import multiple named imports from local TypeScript file", async () => {
            expect(await run(parseJonScript(`
                import { tsTest as otherTest, differentTest } from "./util/tsTest"
                Test {
                    TestClass() {
                        {
                            test: () => otherTest([1, 2, 3]).concat(differentTest([1, 2, 3])),
                        }
                    }
                }
            `, true), {
                requireName: "renameTest",
                className: "TestClass",
                construct: [],
                methodName: "test",
                params: [],
            }, false)).toEqual([2, 3, 4, 2, 4, 6]);
        });
    });
    describe("Equation test", () => {
        test("Quadratic equation test + modulo", async () => {
            expect(await run(parseJonScript(`
                Test {
                    TestClass() {
                        {
                            test: (a: number, b: number, c: number) => [
                                (-1 * b + Math.sqrt(Math.pow(b, 2) + -4 * a * c)) / (2 * a),
                                (-1 * b - Math.sqrt(Math.pow(b, 2) + -4 * a * c)) / (2 * a),
                            ].map(result => result % 3 * -0.4),
                        }
                    }
                }
            `, true), {
                requireName: "quadTest",
                className: "TestClass",
                construct: [],
                methodName: "test",
                params: [1, -10, 24],
            }, false)).toEqual([-0, -0.4]);
        });
    });
    describe("Import inheritance with rename", () => {
        test("Can import and inherit from a prototype from renamed JonScript module", async () => {
            const imports = {
                transpiled: parseJonScript(`
                    Test2 {
                        TestClass2(array: number[]) {
                            {
                                mapper: () => array.map(i => i + 1),
                            }
                        }
                    }
                `, true),
                requireName: "renamedPackage",
            };
            expect(await run(parseJonScript(`
                import { Test2 as Test3 } from "./renamedPackage"
                Test {
                    TestClass() {
                        ...Test3.TestClass2([1, 2, 3]),
                        {
                            test: () => mapper(),
                        }
                    }
                }
            `, true), {
                requireName: "renameModuleTest",
                className: "TestClass",
                construct: [],
                methodName: "test",
                params: [],
            }, false, [imports])).toEqual([2, 3, 4]);
        });
    });
});