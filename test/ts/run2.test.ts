import { parseJonScriptTS as parseJonScript } from '../../dist/testExport';
import { run } from './util/run';

describe("Can run typescript transpiled code and return correct result", () => {
   describe("Can use default import, which imports all exported variables", () => {
        test("Can import default import from utility", async () => {
            expect(await run(parseJonScript(`
                import defTest from "./util/defTestImport"
                Test {
                    TestClass() {
                        {
                            test: () => defTest([1, 2, 3]),
                        }
                    }
                }
            `, true), {
                requireName: "defTest",
                className: "TestClass",
                construct: [],
                methodName: "test",
                params: [],
            }, false)).toEqual([0, 1, 2]);
        });
    });
    describe("Can import JonScript modules", () => {
        test("Can import and utilize JonScript module", async () => {
            const imports = {
                transpiled: parseJonScript(`
                    Test2 {
                        TestClass2(array: number[]) {
                            () => array.map((i) => i + 1)
                        }
                    }
                `, true),
                requireName: "package",
            };
            expect(await run(parseJonScript(`
                import { Test2 } from "./package"
                Test {
                    TestClass() {
                        {
                            test: () => Test2.TestClass2([1, 2, 3])(),
                        }
                    }
                }
            `, true), {
                requireName: "moduleTest",
                className: "TestClass",
                construct: [],
                methodName: "test",
                params: [],
            }, false, [imports])).toEqual([2, 3, 4]);
        });
    });
    describe("Can use shorthand functor and property assignment", () => {
        test("Can use anonymous functions with a single parameter without parentheses", async () => {
            expect(await run(parseJonScript(`
                Test {
                    TestClass(a: string, b: string[]) {
                        a,
                        c: a + "1",
                        {
                            a,
                            test: () => a + b.map(i => i + c).join(","),
                        }
                    }
                }
            `, true), {
                requireName: "shorthandTest",
                className: "TestClass",
                construct: ["1", ["1", "2", "3"]],
                methodName: "test",
                params: [],
            }, false)).toEqual("1111,211,311");
        });
    });
    describe("Can do qsort with templates", () => {
        test("Can use qsort with generic types", async () => {
            expect(await run(parseJonScript(`
                Test {
                    TestClass<T extends (number | string)>(array: T[]) {
                        concat: (a: T[], b: T[], c: T[]) => a.concat(b).concat(c),
                        getLessThan: (a: T[], than: T) => a.filter(val => val < than),
                        getMoreThan: (a: T[], than: T) => a.filter(val => val > than),
                        getEquals: (a: T[], than: T) => a.filter(val => val == than),
                        run: (arr: T[]): T[] => arr.length == getEquals(arr, arr[0]).length
                            ? arr
                            : concat(
                                run(getLessThan(arr, arr[0])),
                                run(getEquals(arr, arr[0])),
                                run(getMoreThan(arr, arr[0]))
                            ),
                        () => run(array)
                    }
                    TestClass2() {
                        {
                            test: <T extends number>(array: T[]) => TestClass<T>(array)(),
                        }
                    }
                }
            `, true), {
                requireName: "templated",
                className: "TestClass2",
                construct: [],
                methodName: "test",
                params: [[2, -1, 3, 0, 0, 0, -4, 6, -7]],
            }, false)).toEqual([-7, -4, -1, 0, 0, 0, 2, 3, 6]);
        });
    });
    describe("Can use inversion control", () => {
        test("Can create classes which use inversion control", async () => {
            expect(await run(parseJonScript(`
                Test {
                    TestClass(a: string = "Hello", b: string = "World") {
                        () => [a, b].join(" ")
                    }
                    TestClass2(testClass: Class<TestClass> = TestClass()) {
                        {
                            test: () => testClass(),
                        }
                    }
                }
            `, true), {
                requireName: "inversion",
                className: "TestClass2",
                construct: [],
                methodName: "test",
                params: [],
            }, false)).toBe("Hello World");
        });
    });
    describe("Can use rudimentary pattern matching", () => {
        test("Can use the is operator", async () => {
            expect(await run(parseJonScript(`
                Test {
                    TestClass() {
                        {
                            value: {{
                                a: "Hello",
                                b: {{
                                    c: "World",
                                    d: 6,
                                }},
                            }},
                            pattern: {{
                                a: String,
                                b: {{
                                    c: String,
                                    d: Number,
                                }},
                            }},
                            test: () => {{
                                a: value is pattern ? value.b.d : "Error!",
                            }},
                        }
                    }
                }
            `, true), {
                requireName: "isoperator",
                className: "TestClass",
                construct: [],
                methodName: "test",
                params: [],
            }, false)).toEqual({ a: 6 });
        });
    });
    describe("Can use spreads", () => {
        test("Using spread operators on arrays gives correct result and will not crash on nil value", async () => {
            expect(await run(parseJonScript(`
                Test {
                    TestClass(...x: number[]) {
                        add: (aNumber: number, y: number[]) => [...x, ...y, 2, ...x, aNumber, ...nil],
                        {
                            test: (anything: any) => add(2, anything),
                        }
                    }
                }
            `, true), {
                requireName: "spreadTest",
                className: "TestClass",
                construct: [1, 2],
                methodName: "test",
                params: [{ a: 5, b: 6 }],
            }, false)).toEqual([1, 2, 5, 6, 2, 1, 2, 2]);
        });
    });
    describe("Import inheritance", () => {
        test("Can import and inherit from JonScript module", async () => {
            const imports = {
                transpiled: parseJonScript(`
                    Test2 {
                        TestClass2(array: number[]) {
                            {
                                mapper: () => array.map(i => i + 1),
                            }
                        }
                    }
                `, true),
                requireName: "package",
            };
            expect(await run(parseJonScript(`
                import { Test2 } from "./package"
                Test {
                    TestClass() {
                        ...Test2.TestClass2([1, 2, 3]),
                        {
                            test: () => mapper(),
                        }
                    }
                }
            `, true), {
                requireName: "moduleTest",
                className: "TestClass",
                construct: [],
                methodName: "test",
                params: [],
            }, false, [imports])).toEqual([2, 3, 4]);
        });
    });
});