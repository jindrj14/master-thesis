import { parseJonScriptTS as parseJonScript } from '../../dist/testExport';
import { run } from './util/run';

describe("Can run typescript transpiled code and return correct result", () => {
    describe("Simplified functors", () => {
        test("Can create a single-expression functor", async () => {
            expect(await run(parseJonScript(`
                Test {
                    FunctorClass() {
                        a: 5,
                        b: 10,
                        c: {{
                            () => a + b,
                        }},
                        {
                            () => d + c(),
                            d: 5,
                        }
                    }
                    TestClass() {
                        {
                            test: () => FunctorClass()(),
                        }
                    }
                }
            `, true), {
                requireName: "simplifyFunctor",
                className: "TestClass",
                construct: [],
                methodName: "test",
                params: [],
            }, false)).toEqual(20);
        });
    });
    describe("Inline function types", () => {
        test("Can use a function type as a type of parameter", async () => {
            expect(await run(parseJonScript(`
                Test {
                    TestClass(fn: <T, E>(a: T, b: E) => T & E) {
                        {
                            test: () => fn<string, string>("Hello", "World"),
                        }
                    }
                }
            `, true), {
                requireName: "inlineFnType",
                className: "TestClass",
                construct: [(a: string, b: string) => [a, b].join(" ")],
                methodName: "test",
                params: [],
            }, false)).toEqual("Hello World");
        });
    });
    describe("Import class type", () => {
        test("Can use the Class template to get type of prototype imported from another module", async () => {
            const imports = {
                transpiled: parseJonScript(`
                    Test2 {
                        TestClass2(array: number[]) {
                            {
                                mapper: () => array.map(i => i + 1),
                            }
                        }
                    }
                `, true),
                requireName: "inferTypeFrom",
            };
            expect(await run(parseJonScript(`
                import { Test2 } from "./inferTypeFrom"
                Test {
                    TestClass() {
                        cls: Test2.TestClass2([1, 2, 3]),
                        run: (val: Class<Test2.TestClass2>) => val.mapper(),
                        {
                            test: () => run(cls),
                        }
                    }
                }
            `, true), {
                requireName: "importTypeTest",
                className: "TestClass",
                construct: [],
                methodName: "test",
                params: [],
            }, false, [imports])).toEqual([2, 3, 4]);
        });
    });
    describe("Usage of complex inheritance", () => {
        test("Can work with multi-level inheritance with prototypes having multiple parents", async () => {
            expect(await run(parseJonScript(`
                Test {
                    T1() {
                        () => 2
                    }
                    T2() {{
                        a: 5,
                    }}
                    T3() {
                        ...T1(),
                    }
                    T4() {
                        ...T2(),
                        ...T1(),
                    }
                    T5() {
                        ...T1(),
                        ...T2(),
                    }
                    T6() {
                        ...T2(),
                        ...T1(),
                        {
                            b: 6,
                        }
                    }
                    T7() {
                        ...T1(),
                        ...T2(),
                        () => 3
                    }
                    TestClass() {{
                        test: () => [
                            T1()(),
                            T2().a,
                            T3()(),
                            T4()(),
                            T5().a,
                            T6().b,
                            T7()(),
                        ],
                    }}
                }
            `, true), {
                requireName: "weirdInheritanceTest",
                className: "TestClass",
                construct: [],
                methodName: "test",
                params: [],
            }, false, [])).toEqual([
                2, 5, 2, 2, 5, 6, 3,
            ]);
        });
    });
    describe("Inherited properties override", () => {
        test("Can remove inherited properties from public API", async () => {
            expect(await run(parseJonScript(`
                Test {
                    Parent() {{
                        a: 2, b: 3, c: 3,
                    }}
                    Child() {
                        ...Parent(),
                        {
                            a: nil,
                            c: 4,
                            d: 5,
                        }
                    }
                    OtherParent() {{
                        f: 5,
                        g: 6,
                        ch: -1,
                    }}
                    OtherParentSimilar() {{
                        f: nil,
                        g: 7,
                        ch: 8,
                    }}
                    TestClass() {
                        ...Child(),
                        {
                            test: () => {
                                ...OtherParent(),
                                ...OtherParentSimilar(),
                                {
                                    a, b, c, d, g: nil, h: "\\"",
                                }
                            },
                        }
                    }
                }
            `, true), {
                requireName: "overridePropsTest",
                className: "TestClass",
                construct: [],
                methodName: "test",
                params: [],
            }, false)).toEqual({
                b: 3,
                c: 4,
                d: 5,
                h: "\"",
                ch: 8,
            });
        });
    });
    describe("Can use custom inheritance", () => {
        test("One object can inherit from another", async () => {
            expect(await run(parseJonScript(`
                Test {
                    TestClass() {
                        {
                            otherParent: () => {{
                                c: 10, d: 11,
                            }},
                            parent: {{
                                a: 5, b: 10,
                            }},
                            child: {
                                ...parent,
                                ...otherParent(),
                                {
                                    e: 12,
                                }
                            },
                            test: () => child,
                        }
                    }
                }
            `, true), {
                requireName: "customInheritanceTest",
                className: "TestClass",
                construct: [],
                methodName: "test",
                params: [],
            }, false)).toEqual({
                a: 5,
                b: 10,
                c: 10,
                d: 11,
                e: 12,
            });
        });
    });
    describe("Can inline object", () => {
        test("Can use object type as method parameter type", async () => {
            expect(await run(parseJonScript(`
                Test {
                    TestClass() {
                        {
                            test: (valid: { a: true, b: false, c?: { d: 10, e?: 20 } }) => valid.c.e ? {{
                                a: 5,
                            }} : {{
                                b: 10,
                            }},
                        }
                    }
                }
            `, true), {
                requireName: "inlineObjectTest",
                className: "TestClass",
                construct: [],
                methodName: "test",
                params: [{ a: true, b: false, c: { d: 10, e: 20 } }],
            }, false)).toEqual({
                a: 5,
            });
        });
    });
    describe("Usage of tree structure inheritance", () => {
        test("Can work with multi-level object inheritance from other objects", async () => {
            expect(await run(parseJonScript(`
                Test {
                    T1() {{
                        a: 5,
                    }}
                    T2() {{
                        t3: {
                            ...T1(),
                            {
                                t4: {
                                    ...T1(),
                                    {
                                        b: 6
                                    }
                                },
                                t5: {
                                    ...t4,
                                    ...T1(),
                                },
                            }
                        },
                    }}
                    TestClass() {{
                        test: () => [
                            T2().t3.t5,
                        ],
                    }}
                }
            `, true), {
                requireName: "multilevelInheritanceTest",
                className: "TestClass",
                construct: [],
                methodName: "test",
                params: [],
            }, false, [])).toEqual([
                { a: 5, b: 6 },
            ]);
        });
    });
    describe("No keyword confusion", () => {
        test("Can use the method Array.from despite from being a keyword in JonScript", async () => {
            expect(await run(parseJonScript(`
                Test {
                    TestClass() {{
                        test: () => Array.from([1]),
                    }}
                }
            `, true), {
                requireName: "arrayFromTest",
                className: "TestClass",
                construct: [],
                methodName: "test",
                params: [],
            }, false, [])).toEqual([
                1
            ]);
        });
    });
});