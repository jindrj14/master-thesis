import { _coerce, _condMerge } from "jonscript-util";

class Test {
    a = 5;
    b = 6;
    constructor(a: number, b: number) {
        this.a = a;
        this.b = b;
    }
    plus = (test: Test) => new Test(test.a, test.b);
}

(async () => {
    const prom = () => new Promise<{ a: 5 }>(resolve => resolve({ a: 5 }));

    /** #!ifinheritance */
    const _a = /** #!coerce-lets */ (await prom());
    /** #!list-properties _a */ _a;
    const _b = /** #!coerce-lets */ (new Test(2, 3));
    /** #!list-properties _b */ _b;
    const _c = /** #!coerce-lets */ ({ a: 3, b: "Hello" });
    /** #!list-properties _c */ _c;
    const _d = /** #!coerce-lets */ ({ a: 3, b: "Hello" });
    /** #!list-properties _d */ _d;
    const _e = /** #!coerce-lets */ (await prom());
    /** #!list-properties _e */ _e;
    const _inheritanceApi = /** #!coerce-lets */ (_condMerge({ }, _c));
    (() => {
        /** #!list-properties _inheritanceApi typed */ _inheritanceApi;
    })();
    /** #!endifinheritance */ _inheritanceApi;

    (async () => {
        const prom = () => new Promise<{ a: 5 }>(resolve => resolve({ a: 5 }));

        /** #!ifinheritance */
        const _a = /** #!coerce-lets */ (await prom());
        /** #!list-properties _a */ _a;
        const _b = /** #!coerce-lets */ (new Test(2, 3));
        /** #!list-properties _b */ _b;
        const _c = /** #!coerce-lets */ ({ a: 3, b: "Hello" });
        /** #!list-properties _c */ _c;
        const _inheritanceApi = /** #!coerce-lets */ (_condMerge({ }, _c));
        (() => {
            /** #!list-properties _inheritanceApi typed */ _inheritanceApi;
        })();
        /** #!endifinheritance */ _inheritanceApi;
    })
})
