export const hoist = <T>(object: T, at: string) => {
    (window as any)[at] = object;
    return object;
}