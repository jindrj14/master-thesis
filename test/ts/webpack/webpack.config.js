const path = require("path");

module.exports = {
  entry: {
    "jons": "./src/index.jons",
    "index": "./src/index.ts",
  },
  mode: "development",
  module: {
    rules: [
      {
        test: /\.jons$/,
        use: "jonscript",
        exclude: /node_modules/,
      },
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    extensions: [ ".tsx", ".ts", ".js", ".jons" ],
  },
  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, "dist"),
  },
};