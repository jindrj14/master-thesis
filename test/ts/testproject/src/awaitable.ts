export const testFunction = async (requireName: string, className: string, construct: any[], methodName: string, params: any[]) => {
    return await (await (await require(`./jons-temp/${requireName}`)?.Test[className](...construct))?.[methodName])?.(...params);
};