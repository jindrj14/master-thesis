export const tsTest = (array: number[]) => array.map(i => i + 1);
export const differentTest = (array: number[]) => array.map(i => i * 2);