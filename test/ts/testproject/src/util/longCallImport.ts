export const callMe = () => ({
    next: () => ({
        a: 5,
        cont: () => ({
            b: 5,
        }),
        functor: (): (() => number) & { a: 5 } => {
            const fn: any = () => 5;
            fn.a = 5;
            return fn;
        },
    }),
    cls: class Berry {
        public i: number;
        constructor(i: number) {
            this.i = i;
        }
        run = () => this.i;
    }
});