import { _o, _enhance, _safeCopy, _is, _sanitize, _sanitizeObject, _try, _throw, _coerce, _safeKeys, _safeValues, _safeEntries, _safePush, _condMerge, _silentSpread } from "jonscript-util";
const PureTupleSet = (set: Set<string> = (() => { const _c = Set; return _c ? new _c<string>() : undefined!; })()) => { let tupleString = _sanitize((tuple: [
    number,
    number
]) => _o.plus("", _o.plus(_o.plus(tuple?.[0], _o.plus("/", tuple?.[1])), ""))); let _publicApi = (() => {
    let add = _sanitize((tuple: [
        number,
        number
    ]) => PureTupleSet?.(_sanitize((() => { const _c = Set; return _c ? new _c(_sanitize([_sanitize(tupleString?.(_sanitize(tuple))), ..._safeValues(Object)(_sanitize((() => { const _expr = Array; const _toBind = _expr?.from; return _enhance(_toBind.bind(_expr), _toBind); })()?.(_sanitize((() => { const _expr = set; const _toBind = _expr?.values; return _enhance(_toBind.bind(_expr), _toBind); })()?.()))) ?? [])])) : undefined!; })())));
    let del = _sanitize((tuple: [
        number,
        number
    ]) => { let remove = _sanitize(tupleString?.(_sanitize(tuple))); let _publicApi = (() => {
        return _safeCopy(PureTupleSet?.(_sanitize((() => { const _c = Set; return _c ? new _c(_sanitize((() => { const _expr = (() => { const _expr = Array; const _toBind = _expr?.from; return _enhance(_toBind.bind(_expr), _toBind); })()?.(_sanitize((() => { const _expr = set; const _toBind = _expr?.values; return _enhance(_toBind.bind(_expr), _toBind); })()?.())); const _toBind = _expr?.filter; return _enhance(_toBind.bind(_expr), _toBind); })()?.(_sanitize(value => _o.nequals(value, remove))))) : undefined!; })())));
    })(); return _sanitizeObject(_publicApi); });
    let has = _sanitize((tuple: [
        number,
        number
    ]) => (() => { const _expr = set; const _toBind = _expr?.has; return _enhance(_toBind.bind(_expr), _toBind); })()?.(_sanitize(tupleString?.(_sanitize(tuple)))));
    let size = _sanitize(() => set?.size);
    return _silentSpread(new class _publicApi {
        add = add;
        del = del;
        has = has;
        size = size;
    });
})(); return _sanitizeObject(_publicApi); };
const Direction = (cardinality: (("up" | "left") | "down") | "right" = "up", x: number = 0, y: number = 0) => { let mapper = (() => {
    let _publicApi = (() => {
        let up = (() => {
            let _publicApi = (() => {
                let clockwise = _sanitize(() => Direction?.(_sanitize("left"), _sanitize(_o.plus(x, 1)), _sanitize(y)));
                let counter = _sanitize(() => Direction?.(_sanitize("right"), _sanitize(_o.minus(x, 1)), _sanitize(y)));
                return _silentSpread(new class _publicApi {
                    clockwise = clockwise;
                    counter = counter;
                });
            })();
            return _sanitizeObject(_publicApi);
        })();
        let left = (() => {
            let _publicApi = (() => {
                let clockwise = _sanitize(() => Direction?.(_sanitize("down"), _sanitize(x), _sanitize(_o.minus(y, 1))));
                let counter = _sanitize(() => Direction?.(_sanitize("up"), _sanitize(x), _sanitize(_o.plus(y, 1))));
                return _silentSpread(new class _publicApi {
                    clockwise = clockwise;
                    counter = counter;
                });
            })();
            return _sanitizeObject(_publicApi);
        })();
        let down = (() => {
            let _publicApi = (() => {
                let clockwise = _sanitize(() => Direction?.(_sanitize("right"), _sanitize(_o.minus(x, 1)), _sanitize(y)));
                let counter = _sanitize(() => Direction?.(_sanitize("left"), _sanitize(_o.plus(x, 1)), _sanitize(y)));
                return _silentSpread(new class _publicApi {
                    clockwise = clockwise;
                    counter = counter;
                });
            })();
            return _sanitizeObject(_publicApi);
        })();
        let right = (() => {
            let _publicApi = (() => {
                let clockwise = _sanitize(() => Direction?.(_sanitize("up"), _sanitize(x), _sanitize(_o.plus(y, 1))));
                let counter = _sanitize(() => Direction?.(_sanitize("down"), _sanitize(x), _sanitize(_o.minus(y, 1))));
                return _silentSpread(new class _publicApi {
                    clockwise = clockwise;
                    counter = counter;
                });
            })();
            return _sanitizeObject(_publicApi);
        })();
        return _silentSpread(new class _publicApi {
            up = up;
            left = left;
            down = down;
            right = right;
        });
    })();
    return _sanitizeObject(_publicApi);
})(); let _publicApi = (() => {
    let clockwise = _sanitize(() => mapper?.[cardinality]?.clockwise?.());
    let counter = _sanitize(() => mapper?.[cardinality]?.counter?.());
    let coords = _sanitize((): [
        number,
        number
    ] => [_sanitize(x), _sanitize(y)]);
    return _silentSpread(new class _publicApi {
        clockwise = clockwise;
        counter = counter;
        coords = coords;
    });
})(); return _sanitizeObject(_publicApi); };
const TestClass3 = (testnum: number) => { let lang = _sanitize((testnum: number, steppedOn = PureTupleSet?.(), direction = Direction?.()): number => { let isBlack = _sanitize(steppedOn?.has?.(_sanitize(direction?.coords?.()))); let _publicApi = (() => {
    return _safeCopy(!testnum ? steppedOn?.size?.() : lang?.(_sanitize(_o.minus(testnum, 1)), _sanitize(isBlack ? steppedOn?.del?.(_sanitize(direction?.coords?.())) : steppedOn?.add?.(_sanitize(direction?.coords?.()))), _sanitize(isBlack ? direction?.clockwise?.() : direction?.counter?.())));
})(); return _sanitizeObject(_publicApi); }); let _publicApi = (() => {
    let test = _sanitize(() => (() => { const _expr = (() => { const _expr = (() => { const _c = Array; return _c ? new _c<number>(_sanitize(100)) : undefined!; })(); const _toBind = _expr?.fill; return _enhance(_toBind.bind(_expr), _toBind); })()?.(_sanitize(0)); const _toBind = _expr?.map; return _enhance(_toBind.bind(_expr), _toBind); })()?.(_sanitize((_0, i) => { let perf = _sanitize((() => { const _expr = (() => { const _c = Date; return _c ? new _c() : undefined!; })(); const _toBind = _expr?.getTime; return _enhance(_toBind.bind(_expr), _toBind); })()?.()); lang?.(_sanitize(testnum)); let _publicApi = (() => {
        return _safeCopy(_o.plus("JonScript,", _o.plus(_o.plus(i, _o.plus(",Langtons ant 100,", _o.minus((() => { const _expr = (() => { const _c = Date; return _c ? new _c() : undefined!; })(); const _toBind = _expr?.getTime; return _enhance(_toBind.bind(_expr), _toBind); })()?.(), perf))), ";")));
    })(); return _sanitizeObject(_publicApi); })));
    return _silentSpread(new class _publicApi {
        test = test;
    });
})(); return _sanitizeObject(_publicApi); };
export const Test3 = { PureTupleSet: PureTupleSet, Direction: Direction, TestClass3: TestClass3 };