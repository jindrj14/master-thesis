export const testFunction = (requireName: string, className: string, construct: any[], methodName: string, params: any[]) => {
    return require(`./jons-temp/${requireName}`)?.Test[className](...construct)?.[methodName]?.(...params);
};