import { parseJonScriptTS as parseJonScript } from '../../dist/testExport';
import { run } from './util/run';

/**
 * Strip class signatures
 */
const normalize = <T>(val: T): T => {
    const anyVal = (val as any);
    if (Array.isArray(anyVal)) {
        return anyVal.map(normalize) as any;
    }
    Object.keys(anyVal).forEach(key => {
        if (Array.isArray(anyVal[key])) {
            anyVal[key] = anyVal[key].map(normalize);
        } else if (typeof anyVal[key] === "object") {
            anyVal[key] = normalize({ ...anyVal[key] });
        }
    });
    return typeof anyVal === "object" ? { ...anyVal } : anyVal;
}

describe("Postprocessing gives out correct result", () => {
    test("Can run a complex class system with pattern matching, and with automatic async and new keyword insertion", async () => {
        expect(normalize(await run(parseJonScript(`
            import defTestImport from "./util/defTestImport"
            import { callMe } from "./util/longCallImport"

            Test {
                PromiseCls() {{
                    aPromise: await Promise<number>(resolve => setTimeout(() => resolve(1), 1)),
                }}
                Map() {{
                    a: "a",
                    b: "b",
                    c: "c",
                }}
                Promisify() {
                    promise: Promise<number>(resolve => setTimeout(() => resolve(5), 100)),
                    {
                        promise,
                    }
                }
                EnhanceNumberObject() {
                    {
                        extraMethod: () => "extra",
                    }
                }
                EnhanceNumber() {{
                    enhance: () => {
                        ...EnhanceNumberObject(),
                        Number()
                    },
                }}
                TestClass(first: boolean = true) {
                    obj: {{
                        first: callMe().next().a,
                        second: callMe().next().a,
                        third: callMe().next().cont().b,
                        fourth: callMe().next().a,
                        fifth: callMe().next().functor().a,
                        sixth: callMe().next().functor().bind is Function,
                    }},
                    pattern: {{
                        first: Number,
                        second: Number,
                        third: Number,
                        fourth: Number,
                        fifth: 5,
                        sixth: true,
                    }},
                    classBased: {{
                        date: Date(2020, 1, 1).getTime(),
                        num: Number(0) ? true : false,
                        str: String("") ? true : false,
                        bool: Boolean(false) ? true : false,
                        berry: callMe().cls(2).run(),
                    }},
                    recursive: (next: Class<TestClass>): Class<TestClass> => next,
                    mapper: [1, 2, 3].map,
                    {
                        result: await Promisify().promise,
                        mapped: mapper(i => i + 1),
                        test: (impure: any) => {
                            helper: Object,
                            randomArray: [1, 2, 3],
                            randomArrayPush: randomArray.push,
                            randomArrayPushed: randomArrayPush(2),
                            randomArrayPushedTwice: randomArray.push(2),
                            mapping: Map(),
                            ...await PromiseCls(),
                            {
                                result,
                                test: obj is pattern,
                                mapped,
                                classBased,
                                rec: first ? (await recursive(TestClass(false))).mapped : nil,
                                error: try throw "error" catch e => e,
                                fin: try 1 + 2 catch () => 1 finally res => res + 2,
                                enhancedType: EnhanceNumber().enhance() is Number,
                                enhancedValue: EnhanceNumber().enhance().extraMethod(),
                                defImp: defTestImport([1]),
                                keys: helper.keys(impure),
                                values: helper.values(impure),
                                entries: Object.entries(impure),
                                randomArray,
                                randomArrayPushed,
                                randomArrayPushedTwice,
                                typeMapped: Object.keys(mapping).map(key => mapping[key]),
                                bool: !Boolean(false),
                            }
                        },
                    }
                }
            }
        `, true), {
            requireName: "postprocess1",
            className: "TestClass",
            construct: [],
            methodName: "test",
            params: [{ a: undefined, b: null, c: 4, d: NaN, e: "" }],
        }, true))).toEqual({
            aPromise: 1,
            result: 5,
            test: true,
            classBased: {
                date: 1580511600000,
                num: true,
                str: true,
                bool: false,
                berry: 2,
            },
            mapped: [2, 3, 4],
            rec: [2, 3, 4],
            error: "error",
            fin: 5,
            enhancedType: true,
            enhancedValue: "extra",
            defImp: [0],
            keys: ["c", "e"],
            values: [4, ""],
            entries: [["c", 4], ["e", ""]],
            randomArray: [1, 2, 3],
            randomArrayPushed: [1, 2, 3, 2],
            randomArrayPushedTwice: [1, 2, 3, 2],
            typeMapped: ["a", "b", "c"],
            bool: true,
        });
    });

    test("Prototype can inherit from an array", async () => {
        expect(normalize(await run(parseJonScript(`
            Test {
                TestClass(array: number[]) {
                    ...array,
                    {
                        test: () => {
                            _: push(4),
                            map(i => i + 1)
                        },
                    }
                }
            }
        `, true), {
            requireName: "postprocess2",
            className: "TestClass",
            construct: [[1, 2, 3]],
            methodName: "test",
            params: [],
        }, false))).toEqual([2, 3, 4]);
    });

    test("Prototype can have an expression as public API", async () => {
        expect(normalize(await run(parseJonScript(`
            Test {
                Complex(r: number, i: number) {
                    abs: () => Math.sqrt(Math.pow(r, 2) + Math.pow(i, 2)),
                    op: {{
                        absolute: abs,
                        type: "Complex",
                        real: r,
                        imaginary: i,
                        plus: (next: number | Object<op>) => next is {{ type: "Complex" }}
                            ? Complex(next.real + r, next.imaginary + i)
                            : Complex(next + real, i),
                    }},
                    ...op,
                }
                TestClass() {
                    reduced: [1, Complex(3, 4), 4, 5, Complex(4, 3)].reduce((p, c) => p + c || 0, 0),
                    {
                        test: () => reduced is {{ type: "Complex" }} ? reduced.absolute() : reduced,
                    }
                }
            }
        `, true), {
            requireName: "postprocess3",
            className: "TestClass",
            construct: [],
            methodName: "test",
            params: [],
        }, false))).toBe(18.384776310850235);
    });
});