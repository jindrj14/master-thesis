import { writeFileSync, unlinkSync, existsSync } from "fs";
import { execSync } from "child_process";

import { processTs } from "../../../dist/testExport";

const pathToTestFile = "./test/ts/testproject/src/jons-temp/";

export const run = async (transpiled: string, testParams: {
    requireName: string,
    className: string,
    construct: any[],
    methodName: string,
    params: any[]
}, awaitable: boolean, imports?: { transpiled: string, requireName: string }[]) => {
    imports?.forEach(({ transpiled, requireName }) => writeFileSync(pathToTestFile + requireName + ".ts", transpiled));
    writeFileSync(pathToTestFile + testParams.requireName + ".ts", transpiled);
    processTs(pathToTestFile, [testParams.requireName, ...imports?.map(i => i.requireName) || []]);
    execSync("yarn build:ts-test");
    try {
        const result = await require("../testproject/dist" + (awaitable ? "/awaitable" : "")).testFunction(
            testParams.requireName,
            testParams.className,
            testParams.construct,
            testParams.methodName,
            testParams.params,
        );
        return result;
    } catch (e) {
        console.error(e);
    } finally {
        unlinkSync(pathToTestFile + testParams.requireName + ".ts");
        imports?.forEach(({ requireName }) => unlinkSync(pathToTestFile + requireName + ".ts"));
    }
};

export const saveFileOnly = (transpiled: string, requireName: string) => {
    if (existsSync(pathToTestFile + requireName + ".ts")) {
        unlinkSync(pathToTestFile + requireName + ".ts");
    }
    writeFileSync(pathToTestFile + requireName + ".ts", transpiled);
    processTs(pathToTestFile, [requireName]);
}
