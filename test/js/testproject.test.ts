import { parseJonScriptJS as parseJonScript } from "../../dist/testExport";
import { writeFileSync, existsSync, unlinkSync } from "fs";

describe("Can transpile and run expressions", () => {
    let counter = 0;
    const evaluate = (value: string) => {
        counter += 1;
        const randomFileName = "random" + counter;
        writeFileSync("./test/js/" + randomFileName + ".js", value);
        return require("./" + randomFileName);
    };
    const wrapper = (jonscript: string) => {
        const wrapped = `module.exports = (() => {
            const _o = {
                bind: (object, key, value) => {
                    if (value !== undefined) {
                        object[key] = value;
                    } else {
                        delete object[key];
                    }
                },
                sanitize: (value) => {
                    if (value !== null && ((typeof value !== "number") || !isNaN(value))) {
                        return value;
                    }
                    return undefined;
                },
                safeCall: (obj) => {
                    if (!obj) {
                        return undefined;
                    }
                    return (...args) => {
                        try {
                            return obj(...args);
                        } catch (e) {
                            if (e.toString().includes("is not a function")) {
                                return undefined;
                            } else {
                                throw e;
                            }
                        }
                    };
                },
                truePrimitives: ["string", "number"],
                isLikeNumber: (a, b) => {
                    const typeofA = typeof a;
                    const typeofB = typeof b;
                    return _o.truePrimitives.includes(typeofA) && _o.truePrimitives.includes(typeofB);
                },
                plus: (a, b) => {
                    let result;
                    if (_o.isLikeNumber(a, b)) {
                        result = a + b;
                    } else {
                        result = a?.plus?.apply?.(undefined, [b]);
                    }
                    return _o.sanitize(result);
                },
                minus: (a, b) => {
                    let result;
                    if (_o.isLikeNumber(a, b)) {
                        result = a - b;
                    } else {
                        result = a?.minus?.apply?.(undefined, [b]);
                    }
                    return _o.sanitize(result);
                },
                multiply: (a, b) => {
                    let result;
                    if (_o.isLikeNumber(a, b)) {
                        result = a * b;
                    } else {
                        result = a?.multiply?.apply?.(undefined, [b]);
                    }
                    return _o.sanitize(result);
                },
                divide: (a, b) => {
                    let result;
                    if (_o.isLikeNumber(a, b)) {
                        result = a / b;
                    } else {
                        result = a?.divide?.apply?.(undefined, [b]);
                    }
                    return _o.sanitize(result);
                },
                modulo: (a, b) => {
                    let result;
                    if (_o.isLikeNumber(a, b)) {
                        result = a % b;
                    } else {
                        result = a?.modulo?.apply?.(undefined, [b]);
                    }
                    return _o.sanitize(result);
                },
                equals: (a, b) => {
                    if (a?.compare) {
                        return a.compare?.apply?.(undefined, [b]) === 0;
                    } else {
                        return a === b;
                    }
                },
                nequals: (a, b) => !_o.equals(a, b),
                greater: (a, b) => {
                    if (a?.compare) {
                        return a.compare?.apply?.(undefined, [b]) > 0;
                    } else {
                        return a > b;
                    }
                },
                less: (a, b) => {
                    if (a?.compare) {
                        return a.compare?.apply?.(undefined, [b]) < 0;
                    } else {
                        return a < b;
                    }
                },
                greaterOrEquals: (a, b) => {
                    if (a?.compare) {
                        return a.compare?.apply?.(undefined, [b]) >= 0;
                    } else {
                        return a >= b;
                    }
                },
                lessOrEquals: (a, b) => {
                    if (a?.compare) {
                        return a.compare?.apply?.(undefined, [b]) <= 0;
                    } else {
                        return a <= b;
                    }
                },
            };
            ${jonscript}
            return Test;
        })()`;
        return wrapped;
    }; // Make jonscript into expressive form. Remove conditional dot operators. Add operators, overloading and forgiving access fn
    test("Can print matrix", () => {
        expect(evaluate(wrapper(parseJonScript(`
            Test {
                Printer(arrayNN) {
                    iterator: (arrayN) => {
                        print: (value, index) => index == (arrayN.length - 1) ? value : (value + " "),
                        arrayN.map(print).join("")
                    },
                    () => arrayNN.map(iterator)
                }
            }
        `))).Printer([[1, 2, 3], [1, 2, 3]])()).toEqual(["1 2 3", "1 2 3"]);
    });
    afterEach(() => {
        const randomFileName = "random" + counter;
        const path = "./test/js/" + randomFileName + ".js";
        if (existsSync(path)) {
            unlinkSync(path);
        }
    });
});