import { parseJonScriptJS as parseJonScript } from "../../dist/testExport";
import { writeFileSync, existsSync, unlinkSync } from "fs";

describe("Can transpile and run expressions", () => {
    let counter = 0;
    const evaluate = (value: string) => {
        counter += 1;
        const randomFileName = "random" + counter;
        writeFileSync("./test/js/" + randomFileName + ".js", value);
        return require("./" + randomFileName);
    };
    const wrapper = (jonscript: string) => {
        const wrapped = `module.exports = (() => {
            const _o = {
                bind: (object, key, value) => {
                    if (value !== undefined) {
                        object[key] = value;
                    } else {
                        delete object[key];
                    }
                },
                sanitize: (value) => {
                    if (value !== null && ((typeof value !== "number") || !isNaN(value))) {
                        return value;
                    }
                    return undefined;
                },
                safeCall: (obj) => {
                    if (!obj) {
                        return undefined;
                    }
                    return (...args) => {
                        try {
                            return obj(...args);
                        } catch (e) {
                            if (e.toString().includes("is not a function")) {
                                return undefined;
                            } else {
                                throw e;
                            }
                        }
                    };
                },
                truePrimitives: ["string", "number"],
                isLikeNumber: (a, b) => {
                    const typeofA = typeof a;
                    const typeofB = typeof b;
                    return _o.truePrimitives.includes(typeofA) && _o.truePrimitives.includes(typeofB);
                },
                plus: (a, b) => {
                    let result;
                    if (_o.isLikeNumber(a, b)) {
                        result = a + b;
                    } else {
                        result = a?.plus?.apply?.(undefined, [b]);
                    }
                    return _o.sanitize(result);
                },
                minus: (a, b) => {
                    let result;
                    if (_o.isLikeNumber(a, b)) {
                        result = a - b;
                    } else {
                        result = a?.minus?.apply?.(undefined, [b]);
                    }
                    return _o.sanitize(result);
                },
                multiply: (a, b) => {
                    let result;
                    if (_o.isLikeNumber(a, b)) {
                        result = a * b;
                    } else {
                        result = a?.multiply?.apply?.(undefined, [b]);
                    }
                    return _o.sanitize(result);
                },
                divide: (a, b) => {
                    let result;
                    if (_o.isLikeNumber(a, b)) {
                        result = a / b;
                    } else {
                        result = a?.divide?.apply?.(undefined, [b]);
                    }
                    return _o.sanitize(result);
                },
                modulo: (a, b) => {
                    let result;
                    if (_o.isLikeNumber(a, b)) {
                        result = a % b;
                    } else {
                        result = a?.modulo?.apply?.(undefined, [b]);
                    }
                    return _o.sanitize(result);
                },
                equals: (a, b) => {
                    if (a?.compare) {
                        return a.compare?.apply?.(undefined, [b]) === 0;
                    } else {
                        return a === b;
                    }
                },
                nequals: (a, b) => !_o.equals(a, b),
                greater: (a, b) => {
                    if (a?.compare) {
                        return a.compare?.apply?.(undefined, [b]) > 0;
                    } else {
                        return a > b;
                    }
                },
                less: (a, b) => {
                    if (a?.compare) {
                        return a.compare?.apply?.(undefined, [b]) < 0;
                    } else {
                        return a < b;
                    }
                },
                greaterOrEquals: (a, b) => {
                    if (a?.compare) {
                        return a.compare?.apply?.(undefined, [b]) >= 0;
                    } else {
                        return a >= b;
                    }
                },
                lessOrEquals: (a, b) => {
                    if (a?.compare) {
                        return a.compare?.apply?.(undefined, [b]) <= 0;
                    } else {
                        return a <= b;
                    }
                },
            };
            ${jonscript}
            return Test;
        })()`;
        return wrapped;
    }; // Make jonscript into expressive form. Remove conditional dot operators. Add operators, overloading and forgiving access fn
    test("Can I use forgiveness in this node?", () => {
        expect(({ a: 5 } as any).a?.b?.c).toBeUndefined();
    });
    test("Can do addition", () => {
        expect(evaluate(wrapper(parseJonScript(`
            Test {
                TestClass() {
                    _: 2 + 3,
                }
            }
        `))).TestClass()).toEqual({});
    });
    test("Can return numerical operation", () => {
        expect(evaluate(wrapper(parseJonScript(`
            Test {
                TestClass() {
                    () => ((2 + 3) / 5) % 10
                }
            }
        `))).TestClass()()).toBe(1);
    });
    test("Can overload addition operator", () => {
        expect(evaluate(wrapper(parseJonScript(`
            Test {
                TestClass(num) {
                    {
                        value: num,
                        plus: (test) => test.value + value,
                    }
                }
                TestClass2() {
                    test1: TestClass(1),
                    test2: TestClass(2),
                    {
                        result: test1 + test2,
                    }
                }
            }
        `))).TestClass2().result).toBe(3);
    });
    test("Can use nil", () => {
        expect(evaluate(wrapper(parseJonScript(`
            Test {
                TestClass() {
                    () => nil
                }
            }
        `))).TestClass()()).toBeUndefined();
    });
    test("Can use arrays and array accessors", () => {
        expect(evaluate(wrapper(parseJonScript(`
            Test {
                TestClass() {
                    array: [1 + 1, 2, 3],
                    array1: [2, 3],
                    {
                        first: array[0 + 1],
                        second: array[3],
                    }
                }
            }
        `))).TestClass()).toEqual({ first: 2 });
    });
    test("Can overload equality", () => {
        expect(evaluate(wrapper(parseJonScript(`
            Test {
                TestClass(num) {
                    {
                        value: num,
                        compare: (test) => test.value - value,
                    }
                }
                TestClass2() {
                    test1: TestClass(1),
                    test2: TestClass(1),
                    {
                        result: test1 == test2,
                    }
                }
            }
        `))).TestClass2().result).toBe(true);
    });
    test("Can avoid NaN", () => {
        expect(evaluate(wrapper(parseJonScript(`
            Test {
                TestClass() {
                    () => (2 / 0) / Infinity
                }
            }
        `))).TestClass()()).toBeUndefined();
    });
    test("Test accessor forgiveness and context", () => {
        expect(evaluate(wrapper(parseJonScript(`
            Test {
                TestClass() {
                    priv: {
                        {
                            a: {
                                {
                                    b: 5,
                                }
                            },
                        }
                    },
                    () => {
                        {
                            valid: priv.a.b,
                            invalid: priv.a.b.c.d,
                        }
                    }
                }
            }
        `))).TestClass()()).toEqual({ valid: 5 });
    });
    test("Can use string interpolation", () => {
        expect(evaluate(wrapper(parseJonScript(`
            Test {
                TestClass() {
                    priv: "Hidden",
                    () => \`This is a \${priv} treasure.\`
                }
            }
        `))).TestClass()()).toBe("This is a Hidden treasure.");
    });
    test("Can use currying", () => {
        expect(evaluate(wrapper(parseJonScript(`
            Test {
                TestClass() {
                    (num) => (str) => (value) => {
                        {
                            first: num,
                            second: str,
                            third: value,
                        }
                    }
                }
                TestClass2() {
                    () => TestClass()(1)("Hello")("World")
                }
            }
        `))).TestClass2()()).toEqual({ first: 1, second: "Hello", third: "World" })
    });
    test("Can use ternary operator", () => {
        expect(evaluate(wrapper(parseJonScript(`
            Test {
                TestClass() {
                    priv: "Hidden",
                    () => priv == "Hidden" ? true : false
                }
            }
        `))).TestClass()()).toBe(true);
    });
    test("Can return an expression from function", () => {
        expect(evaluate(wrapper(parseJonScript(`
            Test {
                TestClass() {
                    priv: 2 + 5 > 7 ? 3 : 4,
                    () => priv
                }
            }
        `))).TestClass()()).toBe(4);
    });
    afterEach(() => {
        const randomFileName = "random" + counter;
        const path = "./test/js/" + randomFileName + ".js";
        if (existsSync(path)) {
            unlinkSync(path);
        }
    });
});