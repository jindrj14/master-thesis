import { parseJonScriptJS as parseJonScript } from "../../dist/testExport";

describe("Can transpile class/module structures into js and run them", () => {
    const wrapper = (jonscript: string) => {
        const wrapped = `(() => {
            const _o = {
                bind: (object, key, value) => {
                    if (value !== undefined && value !== null && !isNaN(value)) {
                        object[key] = value;
                    } else {
                        delete object[key];
                    }
                },
                safeCall: (obj) => {
                    if (!obj) {
                        return undefined;
                    }
                    return (...args) => {
                        try {
                            return obj(...args);
                        } catch (e) {
                            if (e.toString().includes("is not a function")) {
                                return undefined;
                            } else {
                                throw e;
                            }
                        }
                    };
                },
                sanitize: (value) => {
                    if (value !== null && (typeof value !== "number" || !isNaN(value))) {
                        return value;
                    }
                    return undefined;
                },
            };
            ${jonscript.replace(/\?\./g, ".")}
            return Test;
        })()`;
        return wrapped;
    }; // Make jonscript into expressive form. Remove conditional dot operators
    test("Can create empty module", () => {
        expect(eval(wrapper(parseJonScript(`
            Test {

            }
        `)))).toEqual({});
    });
    test("Can create empty class", () => {
        expect(eval(wrapper(parseJonScript(`
            Test {
                TestClass() {

                }
            }
        `))).TestClass).toBeInstanceOf(Function);
    });
    test("Can create two empty classes", () => {
        const evaled = eval(wrapper(parseJonScript(`
            Test {
                TestClass() {

                }
                TestClass2() {

                }
            }
        `)));
        expect(evaled.TestClass).toBeInstanceOf(Function);
        expect(evaled.TestClass2).toBeInstanceOf(Function);
    });
    test("Can define a private placeholder variable", () => {
        expect(eval(wrapper(parseJonScript(`
            Test {
                TestClass() {
                    _: console.log,
                }
            }
        `))).TestClass()).toEqual({});
    });
    test("Can define a private named variable", () => {
        expect(eval(wrapper(parseJonScript(`
            Test {
                TestClass() {
                    write: console.log,
                }
            }
        `))).TestClass()).toEqual({});
    });
    test("Can define empty object public api", () => {
        expect(eval(wrapper(parseJonScript(`
            Test {
                TestClass() {
                    {

                    }
                }
            }
        `))).TestClass()).toEqual({});
    });
    test("Can define a public api", () => {
        expect(eval(wrapper(parseJonScript(`
            Test {
                TestClass() {
                    {
                        define: "1",
                        define2: "2",
                    }
                }
            }
        `))).TestClass()).toEqual({"define": "1", "define2": "2"});
    });
    test("Can define basic inheritance", () => {
        expect(eval(wrapper(parseJonScript(`
            Test {
                Parent() {
                    {
                        define2: "2",
                    }
                }
                Child() {
                    ...Parent(),
                    {
                        define: "1",
                    }
                }
            }
        `))).Child()).toEqual({"define": "1", "define2": "2"});
    });
    test("Can define functor api", () => {
        expect(eval(wrapper(parseJonScript(`
            Test {
                TestClass() {
                    () => {

                    }
                }
            }
        `))).TestClass()).toBeInstanceOf(Function);
    });
    test("Can define functor with inheritance api", () => {
        expect(eval(wrapper(parseJonScript(`
            Test {
                Parent() {
                    {
                        define: 1,
                        define2: 2,
                    }
                }
                Child() {
                    ...Parent(),
                    () => define
                }
            }
        `))).Child()()).toEqual(1);
    });
    test("Can define functor returning an object", () => {
        expect(eval(wrapper(parseJonScript(`
            Test {
                Parent() {
                    {
                        define: 1,
                        define2: 2,
                    }
                }
                Child() {
                    ...Parent(),
                    () => {
                        {
                            define: define,
                        }
                    }
                }
            }
        `))).Child()()).toEqual({ define: 1 });
    });
});