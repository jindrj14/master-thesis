import { materializationBootstrap } from "../dist/tsParser/postprocessing/materializer";

describe("Can materialize specially commented types", () => {
    it("Can run materialization", () => {
        const { actual } = materializationBootstrap();
        expect(actual).toMatchSnapshot();
    });
});