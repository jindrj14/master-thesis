import { parseJonScriptTest as parseJonScript } from "../../dist/testExport";

/**
 * Generates garbage output, but proves that my .g4 files are correct
 */
describe("Can parse basic expressions into pseudo-code", () => {
    test("Can call function", () => {
        expect(parseJonScript(`
            Test {
                TestClass() {
                    call: () => true,
                    value: call(),
                }
            }
        `)).toMatchSnapshot();
    });
    test("Can define array", () => {
        expect(parseJonScript(`
            Test {
                TestClass() {
                    defined: [1, 2, 3],
                }
            }
        `)).toMatchSnapshot();
    });
    test("Can access array", () => {
        expect(parseJonScript(`
            Test {
                TestClass() {
                    defined: [1, 2, 3],
                    first: defined[0],
                    second: [1, 2, 3][1],
                }
            }
        `)).toMatchSnapshot();
    });
    test("Can use ignore attribute", () => {
        expect(parseJonScript(`
            Test {
                TestClass(_, param1, _) {
                    define: (_, param1, _) => true,
                }
            }
        `)).toMatchSnapshot();
    });
    test("Can use functor as expression", () => {
        expect(parseJonScript(`
            Test {
                TestClass() {
                    define: (2 + 3 > 4) ? (param1, param2, _) => true : (_, _, param2) => false,
                }
            }
        `)).toMatchSnapshot();
    });
    test("Can use ternary expression", () => {
        expect(parseJonScript(`
            Test {
                TestClass() {
                    define: (2 + 3 > 4) ? 4 + 5 : 2 + 7,
                }
            }
        `)).toMatchSnapshot();
    });
    test("Can use dot notation", () => {
        expect(parseJonScript(`
            Test {
                TestClass() {
                    defined: TestClass().defined,
                    next: defined.defined,
                }
            }
        `)).toMatchSnapshot();
    });
    test("Can use binary expressions", () => {
        expect(parseJonScript(`
            Test {
                TestClass() {
                    defined: 2 + 3,
                }
            }
        `)).toMatchSnapshot();
    });
    test("Can use unary expressions", () => {
        expect(parseJonScript(`
            Test {
                TestClass() {
                    defined: !true,
                }
            }
        `)).toMatchSnapshot();
    });
    test("Can use a curry and expression return", () => {
        expect(parseJonScript(`
            Test {
                TestClass() {
                    {
                        defined: (value) => true,
                        defined: (value, value1) => (value2) => defined(value2),
                    }
                }
            }
        `)).toMatchSnapshot();
    });
    test("Define a class to print 2D array", () => {
        expect(parseJonScript(`
            Test {
                Printer(arrayNN) {
                    iterator: (arrayN, index) => {
                        print: (value, index) => {
                            newline: () => console.log(value + "\n"),
                            space: () => console.log(value + " "),
                            _: index == arrayN.length - 1 ? newline : space,
                        },
                        _: arrayN.map(print),
                    },
                    _: arrayNN.forEach(iterator),
                }
            }
        `)).toMatchSnapshot();
    });
    test("Define a class to check for duplicities in sudoku", () => {
        expect(parseJonScript(`
            Test {
                Solver() {
                    isSafe: (gridNN, row, col, num) => {
                        equalsRow: (value) => value == num,
                        equalsCol: (col) => (row) => row[col] == num,
                        reduced: (p, c) => p ? p : c,
                        rowSafe: gridNN[row].map(equalsRow).reduce(reduced, false),
                        colSafe: gridNN.map(equalsCol(col)).reduce(reduced, false),
                        square: [0, 1, 2],
                        equalsAt: (x, y) => gridNN[x][y] == num,
                        squareIterDeeper: (realIndex, startCol) => (squareIndex) => equalsAt(realIndex, startCol + squareIndex),
                        squareIter: (startRow, startCol) => (squareIndex) => square.map(squareIterDeeper(startRow + squareIndex, startCol)).reduce(reduced, false),
                        squareSafe: (i, j) => square.map(squareIter(i - i % 3, j - j % 3)).reduce(reduced, false),
                        (rowSafe && colSafe && squareSafe(row, col))
                    },
                }
            }
        `)).toMatchSnapshot();
    });
    test("Define a class to solve sudoku riddle", () => {
        expect(parseJonScript(`
            Test {
                Solver() {
                    options: [1, 2, 3, 4, 5, 6, 7, 8, 9],
                    goNext: (gridNN, row, col, option) => {
                        _: gridNN[row].splice(col, 1, option),
                        solve(gridNN, row, col + 1) || !!(gridNN[row].splice(col, 1, 0) && false)
                    },
                    resolve: (gridNN, row, col) => (option) => isSafe(gridNN, row, col, option) && goNext(gridNN, row, col, option),
                    solve: (gridNN, row, col) => {
                        solved: row == gridNN.length - 1 && col == gridNN.length,
                        solved 
                            ? true
                            : (
                                col == gridNN.length
                                    ? solve(gridNN, row + 1, 0)
                                    : (
                                        gridNN[row][col] > 0
                                            ? solve(gridNN, row, col + 1)
                                            : options.map(resolve(gridNN, row, col)).reduce(reduced, false)
                                    )
                            )
                    },
                    (gridNN) => solve(gridNN, 0, 0)
                }
            }
        `)).toMatchSnapshot();
    });
    test("Can use interpolated strings", () => {
        expect(parseJonScript(`
            Test {
                Solver() {
                    define: \`Addition: \${3 + 5} and \${"Hello" + "World"} text\`,
                    define: \`\${3 + 5} and \${"Hello" + "World"} text\`,
                    define: \`Addition: \${3 + 5} \${"Hello" + "World"} text\`,
                    define: \`Addition: \${3 + 5} \${"Hello" + "World"}\`,
                }
            }
        `)).toMatchSnapshot();
    })
});