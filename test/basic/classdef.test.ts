import { parseJonScriptTest as parseJonScript } from "../../dist/testExport";

describe("Can define class structure and a module", () => {
    test("Can create a module", () => {
        expect(parseJonScript(`Test {
        
        }`)).toBe("module Test\n");
    });
    test("Can define a class", () => {
        expect(parseJonScript(`Test {
            TestClass(param, param2, param3) {

            }
            TestClass1() {

            }
        }`)).toMatchSnapshot();
    });
    test("Can define a public api (object)", () => {
        expect(parseJonScript(`Test {
            TestClass() {
                {

                }
            }
        }`)).toMatchSnapshot();
    });
    test("Can define a public api (functor)", () => {
        expect(parseJonScript(`Test {
            TestClass() {
                () => {

                }
            }
        }`)).toMatchSnapshot();
    });
    test("Can transpile single line comments", () => {
        expect(parseJonScript(`
            // This is a single line comment
            Test {

            }
        `)).toMatchSnapshot();
    });
    test("Can transpile multi line comments", () => {
        expect(parseJonScript(`
            /**
             * This is a multi line comment
             */
            Test {

            }
        `)).toMatchSnapshot();
    });
    test("Can do inheritance", () => {
        expect(parseJonScript(`Test {
            TestClass(param, param2, param3) {

            }
            TestClass1() {
                define: 5,
                ...TestClass(1, 2, define),
            }
        }`)).toMatchSnapshot();
    });
});