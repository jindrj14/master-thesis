import { parseJonScriptTest as parseJonScript } from "../../dist/testExport";

// Source: https://www.geeksforgeeks.org/sudoku-backtracking-7/
/**
 * This is very much just a mock, without real syntax generated. The antlr however, is valid and being parsed
 */
describe("Can use JonScript to create algorithm", () => {
    test("Use JonScript to solve sudoku", () => {
        expect(parseJonScript(`
        Sudoku {
            Printer(arrayNN) {
                iterator: (arrayN, index) => {
                    print: (value, index) => {
                        newline: () => console.log(value + "\n"),
                        space: () => console.log(value + " "),
                        _: index == arrayN.length - 1 ? newline : space,
                    },
                    _: arrayN.map(print),
                },
                _: arrayNN.forEach(iterator),
            }
            Solver() {
                isSafe: (gridNN, row, col, num) => {
                    equalsRow: (value) => value == num,
                    equalsCol: (col) => (row) => row[col] == num,
                    reduced: (p, c) => p ? p : c,
                    rowSafe: gridNN[row].map(equalsRow).reduce(reduced, false),
                    colSafe: gridNN.map(equalsCol(col)).reduce(reduced, false),
                    square: [0, 1, 2],
                    equalsAt: (x, y) => gridNN[x][y] == num,
                    squareIterDeeper: (realIndex, startCol) => (squareIndex) => equalsAt(realIndex, startCol + squareIndex),
                    squareIter: (startRow, startCol) => (squareIndex) => square.map(squareIterDeeper(startRow + squareIndex, startCol)).reduce(reduced, false),
                    squareSafe: (i, j) => square.map(squareIter(i - i % 3, j - j % 3)).reduce(reduced, false),
                    (rowSafe && colSafe && squareSafe(row, col))
                },
                options: [1, 2, 3, 4, 5, 6, 7, 8, 9],
                goNext: (gridNN, row, col, option) => {
                    _: gridNN[row].splice(col, 1, option),
                    solve(gridNN, row, col + 1) || !!(gridNN[row].splice(col, 1, 0) && false)
                },
                resolve: (gridNN, row, col) => (option) => isSafe(gridNN, row, col, option) && goNext(gridNN, row, col, option),
                solve: (gridNN, row, col) => {
                    solved: row == gridNN.length - 1 && col == gridNN.length,
                    solved 
                        ? true
                        : (
                            col == gridNN.length
                                ? solve(gridNN, row + 1, 0)
                                : (
                                    gridNN[row][col] > 0
                                        ? solve(gridNN, row, col + 1)
                                        : options.map(resolve(gridNN, row, col)).reduce(reduced, false)
                                )
                        )
                },
                (gridNN) => solve(gridNN, 0, 0)
            }
        }
        `)).toMatchSnapshot();
    });
});