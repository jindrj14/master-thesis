# Abstract

Javascript is the worlds most used language. It is the backbone of web development.
JS has many amazing features and a wide range of frameworks and extensions.
JS has both prototypal, and a class based inheritance, method and property attributes,
operators for determining type of variable - typeof and instanceof - similar to 'as' and 'in' operators in C#.

JS has powerful functional programming features, such as lambda functions, similar to C# or Python.
Standard library in JS includes powerful functions for array manipulation, .map, .filter, .sort etc, reminiscent of LINQ.
For JS, this means that you can pass functions as parameters, return functions and functions - essentially, you can treat a function
just like any other kind of object/primitive.

Consider an example, where you need to create map-reduce algorithm function, which returns an array of word lengths from text.
I'm creating two different versions of this algorithm in Typescript in order to showcase how useful functional programming is.

Here is an implementation of the map-reduce without aforementioned features:

```typescript

const wordCount = (text: string) => {
    const split = text.split(" ");
    const map: Record<number, number> = {}; // Hash-map
    for (let i = 0; i < split.length; i++) {
        if (map[split[i].length]) {
            map[split[i].length] += 1;
        } else {
            map[split[i].length] = 1;
        }
    }
    return map;
};

```

In the example, we are missing any kind of abstraction - in order to implement a slightly different kind of map-reduce,
I would need to write out another function without sharing code.

Here is an example of the same algo, but with JS array functions:

```typescript

const wordCount = (text: string) => text
    .split(" ")
    .map(word => word.length) // This is equivalent to SELECT query in SQL
    .reduce((p: Record<number, number>, length) => ({ ...p, [c]: (p[length] || 0) + 1 }), {}); // Copy map, rewrite element at index 'length'

```

The example is shorter, but the main advantage is that we can create an abstraction.
Consider this example:

```typescript
/**
 * Library function to provide map-reduce functionality 
 */
const mapReduce = <TArrayElement, TReturnType>(
    values: TArrayElement[], // Array of input values
    map: (element: TArrayElement) => TReturnType, // Function to map whatever value passed in into the desired type
    indexer: (element: TReturnType) => (string | number), // Function to provide index signature for hashmap
) => values
    .map(map) // Pass map function into .map() - function as parameter
    .map(mapped => [mapped, indexer(mapped)]) // Create a tuple of [value, indexed]
    .reduce((p: Record<string | number, TReturnType>, [mapped, index]) => ({ ...p, [index]: ((p[index] || 0) + 1) })); // Finally, reduce the array into a hash-map

```

Now we have defined a generic function with two type parameters that can take any map function, together with index provider.
Using this generic reduce function we can simplify other functions - not just the word count.

Consider these examples:

```typescript

/**
 * Single line wordcount!
 */
const wordCount = (text: string) => mapReduce(text.split(" "), word => word.length, i => i); // Since we are dealing with strings, they can be an index without help

/**
 * Counts how many words start with certain letter
 */
const firstLetterCount = (text: string) => mapReduce(text.split(" "), word => word[0], i => i);

```

Many object oriented language developers aim for the same features. The unique thing about JS is that
such behaviour is perfectly natural and very easily available.

Consider a similar example in C#:

```C#

using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;

public class Program
{
	public static void Main()
	{
		var values = new List<string> { "Hello", "World", "Twice", "World" }.AsQueryable();
		MapReduce(
			values,
			(string word) => word.Length,
			(int length) => length.ToString()
		);
	}
	private static List<int> MapReduce<TArrayElement, TReturnType>(
		IQueryable<TArrayElement> values,
		Expression<Func<TArrayElement, TReturnType>> map,
		Func<TReturnType, string> indexer
	) => values
		.Select(map)
		.Select(mapped => new { Mapped = mapped, Index = indexer(mapped) })
		.Aggregate(new Dictionary<string, int>(), (previous, current) => Accumulate(previous, current.Index))
		.ToList()
		.Select(pair => pair.Value)
		.ToList();
	private static Dictionary<string, int> Accumulate(Dictionary<string, int> previous, string current)
	{
		if (previous.ContainsKey(current))
			previous[current] += 1;
		else
			previous[current] = 0;
		return previous;
	}
}

```

In Javascript, or Typescript, functional programming comes naturally and you can use function-as-parameter,
or function-as-return-value without the need to change existing code - any function can be taken and passed as such.

The JS package repository, NPM, offers a huge selection of libraries,
language extensions, compilers and linters. Well known examples are: Typescript, ESLint,
React, Knockout, Vue and Angular. Typescript and ESLint are transpile-time libraries which
affect the quality of the code produced, while React, Knockout, Vue and Angular are frameworks,
used to manipulate the HTML/CSS UI of a webpage, or an app.

Furthermore, React and Angular work with JSX. JSX is a syntax extension for JS/TS, which allows you to quickly
define HTML elements on a page by transpiling
HTML tags within the code into function calls of document.createElement. // Expand and cite like crazy here

However, the language is old and underwent many changes, while maintaining backwards compatibility.
This leads to a problem: many different ways of doing the same thing.

Consider an example, where I simply want to create an object, which has a single property.
The value of this property is a function, which returns an object that has only one property as well.

To start with, I'll need to define a variable - there are three ways of doing it:
* const - defines a "constant" variable - if said variable is an object, you can mutate it's properties
* let - defines a block-scoped variable - you can assign another value into it at any other time.
* var - defines a function-scoped variable - it's similar to let, however there are key differences:
    * var is hoisted - you can access it in the same scope before it's defined. It's value will be the primitive 'undefined'
    * var can be redefined - you can define the same variable multiple times in the same scope
    * when defined inside the global scope, var becomes the property of the global object (usually window). Therefore,
        var defined variables will mutate the object, even before they are defined.

Let's choose 'const', since the variable does not need to be mutated later on. So far, our code will look like this: 

```typescript

const Module = {}; // assigns an empty object

```

Next, we are going to have to create a property on Module. We can either define it by simply creating a property,
using a getter/setter combination, or by calling the function 'Object.defineProperty'. If we use getter and setter,
we run the risk of this unexpected behaviour:

```typescript

const o = { get length() { return this._len }, set length(value) { this._len = value }, _len: 5 };
const a = { ...o }; // We copy object properties through a short-hand call to Object.assign
a.length = 6; // We use the setter to mutate _len property
console.log(a.length); // 6 - as expected

console.log(a._len); // 5 - This is the strange part

o.length = 7; // We use the setter to mutate o
console.log(o.length); // 7 - Works as expected
console.log(o._len); // 7 - Works as expected

```

This is caused by the property 'length' losing it's getter/setter function after it has been copied.
If you create only getter for your property and try to assign into it, it will throw an error in strict mode.
However, said property may be deleted using the 'delete' operator and then reassigned again. To truly create a read-only
property, one must use the 'Object.defineProperty':

```typescript
// Example taken from: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty
const object1 = {};

Object.defineProperty(object1, 'property1', {
  value: 42,
  writable: false, // Note the difference between configurable and writeable! If the property is only non-writeable, it can still be deleted.
  configurable: false,
});

object1.property1 = 77;
// throws an error in strict mode

console.log(object1.property1);
// expected output: 42

```

For now, let's use simple assignment. Now, our code looks like this:

```typescript

const Module = {
    Example: ?
}

```

Now we need to define a function, which returns an object. There are 3 main ways of doing this.
First, we will discuss classic JS function.

* A function definition is initialized by the keyword 'function', followed by an optional name and a code block.
* Function may have a set of parameters, last one of which can be a spread
* The output of a function may depend on its execution scope. One can access this scope by 'this' keyword.
    * 'this' in a global scope usually refers to 'window' (in strict mode, it will be undefined)
    * 'this' can be redefined by using functions .call, .apply or .bind on said function
    * one can redefine the contents of 'this' by running the function in different scope
* each function also has 'length', which refers to the number of parameters and the 'arguments' object,
    which refers to function arguments. Arguments object also has the .callee property, which refers to the function itself.

Secondly, lets discuss the arrow functions:
* An arrow function does not change it's context based on execution.
* Arrow function cannot use 'yield' operator
* Arrow functions also cannot be manipulated by .bind, .call or .apply functions
* An arrow function cannot be used for constructors and does not have the new.target property, which let's you see whether or not it has been called using the 'new' keyword

And last, but not least, we could define the function as class. Classes in JS are essentially just functions, which return an object.
There are few differences between functions and classes:
* A class must be called with the 'new' keyword. (You may omit the parentheses, if you don't need any parameters in the constructor)
* A class will automatically assign a 'prototype' property to the 'returned' object

Both class and the prototype are simply objects/functors and can be changed during runtime at will. Consider this example: 

```typescript

class Test {
    a = 5;
    b = 3;
}

Test.prototype.add = function() {
    return this.a + this.b;
}
function add()

new Test() // Object { a: 5, b: 3 }

new Test().add() // 8

```

Let's choose the arrow function and continue. Next, we need to define an array and return the desired value.

There are three ways of defining an array, 'new Array(1, 2, 3)', 'Array(1, 2, 3)' and '[1, 2, 3]'. The difference between
'[1, 2, 3]' and calling Array is that is there is a single parameter of type number passed into the Array function/class,
it will generate an array of empty elements of certain length. There are no other differences between these,
so let's simply define a variable with array like this '[1, 2, 3]' and now we have:

```typescript

const Module = {
    Example: () => {
        const map = [1, 2, 3];
    }
}

```

Since JS has a strong support of functional programming, let's go ahead and take the '.map' function and store it in a variable:

```typescript

const Module = {
    Example: () => {
        const map = [1, 2, 3].map;
    }
}

```

Since JS only has only one kind of return statement, we can use it and return the desired value:

```typescript

export const Module = { // var? let?
    Example: () => { // class? function()? class Example? function Example()?
        const map = [1, 2, 3].map; // new Array? Array? .map is similar to SELECT in SQL
        return { // new class? We can define inline anonymous classes as well
            result: map(i => i + 1), // Throws an error, cannot call .map on undefined! <-- Execution context error!
        };
    },
};

```

In the text above, I outline at least six different ways of creating the same object.
All of those ways will end with the error outlined in the last comment. Each possible example
differs from the other one in subtle ways that may break your program.

I created a test to showcase that even JS devs have trouble with the language and distributed the test
to a focus group of JS devs with differing levels of experience. Each question had three possible answers.
Only one answer was correct. The participants had unlimited time to think about each answer.
Below is a list of each question, coupled with an explanation of why such question was asked and the correct answer

* List of questions:

* What does 'Date()' return?
    * Date object
    * string
    * Error, since we are calling a class without a constructor

Reasons for asking:
    This is a really easy mistake to make - you just call a class as a function.
    The trick is that depending on the class, you may get a variety of results.
Answer:
    'Date()' returns a string. This is a common theme with some of JS classes, such as String or Number.
    Calling any of these will result in a JS primitive.

Which expression equals 'true'?
    typeof new Number === "object"
    typeof new Number === "number"
    typeof Number(4) === "object"

Reasons for asking:
    During my development of my pattern matching library, I have encountered
    this exact problem - there are subtle differences between classes and primitives in JS
    that a developer may be unaware of, especially given that if one calls a method on
    a primitive, that primitive is automatically wrapped with a class before call.
    This gives a developer an impression that certain classes are equivalent to a primitive.

Answer:
    Since 'new Number' (even without parentheses) is equivalent to 'new Number()' and we are
    calling a class with the 'new' keyword, the resulting expression has the 'typeof' value 'object'. 

If 'window' has 222 properties, what is the value of variable 'c' after executing: 'console.log(t); const c = Object.keys(window).length; var t = 1;'?
    undefined
    222
    223

Reasons for asking:
    In most programming language, defining a variable will not automatically affect a global object.
    While some developers may be aware of hoisting 'var', they may not realize that such hoisting creates
    an enumerable property on the 'window' object.

Answer:
    Since 'var t' is hoisted, it is defined at the topmost position in the codeblock. Since the hoisting
    affects the global object window, a new property is defined on it. The answer is therefore 223.

What are some of the benefits of 'let' over 'var'?
    None
    'let' will not mutate the window object
    'let' cannot be undefined

Reasons for asking:
    To determine whether a JS dev realizes the differences between block-scoped and function scoped variable.
    This is especially important in legacy projects, but 'var' can still be used today, for example,
    if one wishes to have a short-hand way of defining a global variable.

Answer:
    Let is a block scoped variable, which behaves much like a variable may in other languages. It cannot be
    redefined, can be assigned to multiple times, it is not hoisted and it does not mutate the global object.

Which expression is truthy?
    new Boolean(false)
    new String("" + new String("a")) === new String + "a"
    NaN === NaN

Reasons for asking:
    Equality and truthyness of expressions in JS has specific properties which are not ordinarily found
    in other languages. NaN, for example, is never equal to itself. Triple equals will only compare pointers
    when passed an object. The Boolean class is a strange concept. Double equals will equal it to false,
    since double equals operator uses coercion and valueOf functions, however, it is always truthy.

Answer: new Boolean(false)

Which of the following will create an infinite loop?
    while(new Boolean(false)) {}
    const arr = []; for(var i = 0; i < 3; i++) arr.push(() => i); while(arr[0]() !== arr[1]()) {}
    while(1 / 0 !== 1 / 0) {}

Reasons for asking:
    This question tests the developers knowledge on three things: How does Javascript handle division by zero?
    Returns Infinity number. Second answer deals with function scoped variables - a function scoped variable
    will change it's value even after it has been passed into an anonymous function. The variable 'arr' will therefore
    contain three functions with the same result if executed. The first answer asks whether 'while' uses truthyness
    or '==' for evaluating an expression.

Answer:
    Since while uses truthyness to determine whether or not it runs, the correct answer is 'while(new Boolean(false)) {}'

Is the operator '==' reflexive or transitive?
    Yes, both
    '==' is reflexive, but not transitive
    Neither

Reasons for asking:
    Equality operators in Javascript may result in uncommon behaviour.
Answer:
    Double equals is neither reflexive (NaN is not equal to itself), nor transitive: '[] == 0' is true and so is '0 == "0"',
    but '[] == "0"' returns false.

Which of following options will throw an error in ES6?
    new function() {}
    var l = t + 1; var t = 0;
    const a = [1, 2, 3].map; a(i => i + 1);

Reasons for asking:
    This question was designed to determine whether JS developers would notice a function context change / error.

Answer:
    Third answer is correct - .map function is context-dependent upon execution. Assigning it into a variable
    changes it's context and that change results in an error upon execution. One could use the, for example,
    the .bind function to create a .map function with context-independent execution.

Which expression is truthy?
    "" instanceof String
    typeof new String === 'string'
    typeof (new String + new String) === 'string'

Reasons for asking:
    This question was designed to test whether or not JS developers understand the difference between
    instanceof and typeof operator, and to test if they know how the plus operator works.

Answer:
    A primitive string is not an instanceof String, since it's not an object. This behaviour may seem
    strange, as you can call methods on a string, but this is in fact not true. Primitive strings
    are wrapped automatically in a class constructor and then the class methods are called.

    This makes the first question false. The second question is also untrue, since the typeof operator
    will determine a String class as an object, not a string (that would be the case with primitive strings).

    The third answer is correct - when using the plus operator on any object, Javascript will automatically
    call valueOf on this class. This behaviour is similar to double equals. Since the plus operation results
    in a primitive, not a class, typeof will determine the value to be a string.

Which expression will result in an error?
    new Date?.()
    Date?.()
    RegExp?.()

Reasons for asking:
    Since JS syntax keeps expanding over time, I wanted to know whether JS developers understood the optional chaining
    when used as a part of a call expression.

Answer:
    While functions can be called using this operator (the result will be undefined if the expression being called is a null or undefined,
    will throw an error if it is anything else than a function, null or undefined), the 'new' operator cannot be combined with the optional chaining.
    Therefore, the first answer is right.

Which expression will result in an error?
    +"0"
    new Date(2020, 1, 32)
    \[...Object.values()\]

Reasons for asking:
    Javascript operators and basic functions have a large variety of outcomes from non-standart inputs. Consider the functions encodeURI and
    decodeURI - when no parameter is passed inside them, they return the string "undefined". On the other hand, the Date constructor always
    returns a valid date, even when passed invalid values into it. Months in JS date object are counted from zero. This question was
    designed to identify whether or not JS devs remember some of the subtle differences in how JS functions and operators react to non standart
    input.

Answer:
    Object.values will throw an error when called without parameter.


Here is the feedback from the group:

* Ing. Tomáš Jákl, Web developer, moderate JS experience - 2/11 questions answered correctly, guessed both correct ones by own admission.
* Miloš Chmel, professional JS dev for ? years: 4/11 questions.
* Jiří Holub, professional JS dev from ? years: 7/11 questions.


The results from my focus group show that even experienced JS developers have trouble with certain JS concepts.
In order to fix this problem, I created a new programming language, which transpiles into Javascript.
I named it JonScript. The main ideas of this language are:

* Consistency: Only one way to do things;
* Compatibility: Compatible with Typescript libraries, Typescript code can be imported; and
* Ease of use: A current Javascript developer should be able to quickly understand the language.


Here is a list of features:

* All functions used in JonScript are guaranteed arrow functions, including imports.
    * Consider the example above. Given the answers from the focus group, most JS devs would
      assume that the execution of function assigned to a variable will not change based on the
      context in which it is executed.

      JonScript can detect this kind of assignment when it's being transpiled and automatically
      assigns every function the context of the object from which it was accessed.

      Consider this example:

      ```typescript
        const map = (() => { const _e = [1, 2, 3]; return _e.map.bind(_e) })(); // Creates a .map function bound permanently to _e
      ```

      This is what JonScript does automatically.

* High Functional Programming support and encouragement
    * Jonscript abandons all reassignment operators, such as '=', '+=' and more.
      Instead, one may assign variables only once. This encourages thinking in terms of pure functions,
      but with simple way of defining a helper variable.
* Simple class definitions with familiar syntax
    * JonScript is an object oriented language with support for classes. It has prototypal inheritance
      and supports inversion control, promises in construtor, public and private variable creation,
      multiple parent inheritance and limited dynamic dispatch.
* LINQ-like automatic null forgiveness
    * JonScript automatically uses optional chaining operators whenever one uses property access, element access or function call.
      This forgiveness extends to all numeric operators - all of them will return 'undefined' (in JonScript terms, 'nil'), when being used
      on a 'nil' value.
* Single string type
    * JS/TS has two kinds of strings: '' and "". Usually a programmer would assume that one of them is a char type. This is wrong.
      JonScript only support "this" kind of string.
* No 'undefined' defined values
    * JonScript guarantees all values are nil will not create a key on object. Object.keys/values/entries will behave accordingly, even when
      being called on objects passed from outside of JonScript. Consider this example:

      ```typescript

      const values = { a: 1, b: 2: c: 3 };
      Object.values(values).reduce((previous, current) => previous.toString() + current.toString(), ""); // Result would be "123"

      ```

      Now, how would you remove the 'b' value from the object 'values'? You could assign null to 'b'? You may assume that the result would be
      "13". However, since the value of 'b' is now null, the script throws a type error. Same, if you try to assign undefined. You could solve this problem
      by using the .join function, since null + string is equal to the string value. This would, however not work for 'undefined'. The result for 'undefined' b
      would be "1undefined3".

      The correct way of 'removing' a property from an object is using the 'delete' operator, reminiscent of C/C++ syntax. JonScript forgoes all these issues by
      simply removing any null-ish properties from an object upon returning it.
* Single null value in language
    * JS has two different null-ish values, each with specific behaviour.
      * null 
      * undefined
      There are also several values which are 'falsy':
      * false
      * 0
      * ""
      * NaN
      JonScript only has 'nil' null-ish value, which transpiles to 'undefined' and replaces NaN, null and undefined.
* Compatibility with household names like jQuery and Redux
      * I have created a testproject which utilizes both jQuery and Redux with minimal TS coding for compatibility.
* Pattern matching operator
      * JS has two different ways of recognizing the type of an object:
          * typeof - can determine whether a variable is string, number, object, function, bigint, symbol or undefined.
            * This operator considers null to be an object, NaN (not a number) to be a number and anything callable to be a function.
          * instanceof - this operator follows the prototype chain to determine whether a class constructor is present within it. It always returns false
            with numbers, strings or booleans.
      * JonScript forgoes these two operators in favor of 'in'. The 'in' operator works with both primitive values and their classes, and can do a deep comparison
      of object values based on patterns. This operator uses complex pattern matching library of my own development. Furthermore, it acts in similar way as does the
      'is' operator in TS. The improvement is that it does both type coercion and pattern matching in a single line.
* Operator overloading and protection against unexpected results
    * Operators in Javascript frequently have unexpected results. E.g. '9 + "1" = "91"', '"91" - 1 = 90'. Jonscript resolves this problem
      by creating specific that prevent these results from happening. // TODO: create a table for possible +/- etc.
    * Furthermore, JS has two different equality operators. Neither is reflexive, since NaN is never equal to itself. 
        * First one is '=='. This operator tries to coerce values to the same type. This operator is not transitive, but can compare string class
          as expected (e.g. 'new String("Hello") == new String("Hello")' is true). This is true, since '==' uses valueOf() calls to do coercion.
          This operator also considers 'undefined' and 'null' to be equal, as well as '[]' and the number zero.
        * Second equality operator in JS is '==='. This operator works based on identity comparison. This means it's transitive, but it cannot be
          used to compare String classes without explicit valueOf call.
    * JonScript uses only one comparator '==', which internally uses '===' comparison. This operator also tries to call valueOf() to enforce limited coercion.
      Each operator will be symmetric, except for overloaded operators, where the left side takes preference over the right side - if you run the plus operation
      on two object with 'plus' function overload, the left 'plus' function will be the one used. If an operator call types are not described in the table
      bellow, the result from the operation will be 'nil' (traspiled into undefined). If a cell contains the word 'overload', this means that an object
      has has this function with a single parameter:
      (plus - '+', minus - '-', multiply - '*', divide - '/', modulo - '%', compare - all comparison operators, should return a number).
        * Table for binary operators:
        Operator | Left type | Right type | Result
        \+       | string    | string     | string
        \+       | string    | String     | String
        \+       | string    | Number     | string
        \+       | string    | number     | string
        \+       | String    | String     | String
        \+       | Number    | Number     | Number
        \+       | number    | Number     | Number
        \+       | number    | number     | number
        \+       | overload  | any        | return of overload
        \-       | number    | number     | number
        \-       | number    | Number     | Number
        \-       | overload  | any        | return of overload
        \*       | number    | number     | number
        \*       | number    | Number     | Number
        \*       | overload  | any        | return of overload
        \/       | number    | number     | number
        \/       | number    | Number     | Number
        \/       | overload  | any        | return of overload
        \%       | number    | number     | number
        \%       | number    | Number     | Number
        \%       | overload  | any        | return of overload
        \=       | T         | T          | boolean
        \=       | T         | Other      | false
        \=       | overload  | any        | return of overload
        \!=       | T         | T          | boolean
        \!=       | T         | Other      | false
        \!=       | overload  | any        | return of overload
        \<       | T         | T          | boolean
        \<       | T         | Other      | false
        \<       | overload  | any        | return of overload
        \>       | T         | T          | boolean
        \>       | T         | Other      | false
        \>       | overload  | any        | return of overload
        \<=      | T         | T          | boolean
        \<=      | T         | Other      | false
        \<=      | overload  | any        | return of overload
        \>=      | T         | T          | boolean
        \>=      | T         | Other      | false
        \>=      | overload  | any        | return of overload
        
        
        The 'in' operator will take a typed object and an object/primitive that fits possible object pattern based on the left type. For more
        information, check the 'matchto' documentation
        * Unary operators
            * await object: Resolves a promise (object with then function), or returns original object
            * !object: Return a false if parameter is truthy, true if falsy
            * throw object: Throws an object - any object/primitive in JS can be thrown as exception
        * Ternary operators
            * try 'object' catch 'function' finally 'function': This is essentially typical inline try/catch/finally construct. Catch and finally
              require a function being passed into them, either the result of the 'object' expression, or the resulting exception. The expression
              returns whatever catch or finally return.
            * 'condition' ? 'then' : 'else': Ternary conditional operator.
    * All numerical operators can be overloaded. Overloading operator in JonScript simply means defining a single-parameter function on any class or object.
      These functions are: 'plus', 'minus', 'divide', 'multiply', 'modulo' and 'compare'.
    * Overloaded operators will be executed primarily from left to right, secondarily from the opposite direction. What this means is that if you have
      an operator overload on an object, and use overloaded operation on number, it will not matter whether you write 'object + number' or 'number + object'.
* String and Number classes are usable again
    * Consider these examples:
        ```typescript
            new String == new String // true
            new String === new String // false, two object pointers are not equal to each other
            "" == "" // true, comparing primitive values
            "" === "" // true
            new String + new String === new String + new String // plus operation always returns a primitive
        ```
    * The changes in operators and their overloading make sure one can use String and Number classes as expected. Furthermore, one can use inheritance
      to overload their properties/operators to create objects which can act as complex numbers, currencies or matrixes.
* Automatic class definition without 'new'
    * JonScript can detect when calling a function, whether or not to automatically add the 'new' keyword during parsing. Given the results from the focus group,
      JS developers have trouble understanding how 'new' works in JS. JonScript not only calls 'new' automatically, but it also uses forgiveness during, similar to
      how functions are treated. There is only one exception, which is the Boolean class - since Boolean(false) is truthy in JS, the use of Boolean class has been
      abandoned entirely.
* Best of both worlds in object creation and class definition
    * Over time, JS has gained several ways of creating objects/classes. Consider these two examples:
      ```typescript

      class Obj {
          test = 5;
      }

      const Obj = () => ({
          test: 5;
      })

      ```
      Let's take a look at how different features are expressed in either example:
        * private properties: In class example, the private keyword would be used. In object example, you need to define a local var within the scope of function
        * inheritance: 'extends' keyword in the class example, spread operator inside returned object in the object example.
        * pattern matching / determining 'origin' of an object: 'instanceof' operator in class, no way of doing this within the object example.
        * protected properties: 'protected' in class mode, no way of doing this within the object one.
      However, there are many caveats hidden within these points: For example, 'class' inheritance does not allow for multiple parents. Another important distinction
      is that 'object' example can use await/async within the 'constructor' function.

      JonScript combines the features of both. You can use multiple parent inheritance, promises in constructors and easily define private properties.
      JonScript also allows both classes and objects to have recursive typing, except in cases of non-function expression substituting public properties.
* Automatic use of 'async' keyword
    * Any time you await a promise, the parent method automatically gets the 'async' keyword. This works within constructor, inheritance, public or private properties.
* Inline functor creation
    * JonScript has brand new syntax which allows you to easily create functors with just a single expression.
* Functor inheritance
    * One can inherit properties from functors just as easily as from objects. Consider this example:
        ```typescript
            const parent = () => 5;
            parent.a = 3;
            const child = { ...parent }; // { a: 3 }
        ```
      JonScript avoids this by treating functors just like an object which has the '()' operator overloaded. Therefore, you can work with functions/functors
      just like you would with objects.
* Powerful prototypal inheritance
    * One of the key functions of prototype inheritance is not just redefining existing methods on parent class, but also removing them.
      JonScript can remove methods and properties from a class through inheriting from a class that explicitly removes them, or by
      assigning nil to an inherited property.
    * This resolves the fragile base class problem present in most class-based OOP languages. The fragile base class problem happens
      when you continually build on present class structure. Simple example in C#:
      ```C#
        class Car {
            private number speed;
            private string color;
            public Car(int speed, string color) {
                this.speed = speed;
                this.color = color;
            }
            public string Admire() {
                return $"Admiring the {color} car";
            }
            public string Race() {
                return $"Racing at the speed of {speed}";
            }
            public string LoadFurniture() {
                return $"Loading furniture into car!";
            }
        }

        // We cannot load furniture into the racing car! Should we change the inheritance structure?
        class RacingCar : Car {
            public RacingCar() : base(250, "Red") {}
        }
        // We can't race with a truck
        class Truck : Car {
            public Truck() : base(100, "White") {}
        }
        // Who want's to admire ordinary car?
        class OrdinaryCar : Car {
            public OrdinaryCar() : base(120, "Blue") {}
        }
      ```
        As you can see from the comments in the example, we can't load furniture into a ferrari, nor race with a truck and
        nobody would admire an ordinary car. This is the basis of fragile base class problem - the more child classes you have,
        the more you need to do exponential changes in your code to accomodate to various exceptions from the parent class.

        Lets use prototypal inheritance in JS to resolve this problem:
        ```typescript
            const Car = (speed, color) => ({
                admire: () => `Admiring the ${color} car`,
                race: () => `Racing at the speed of ${speed}`,
                loadFurniture: () => `Loading furniture into car!`,
            });
            const RacingCar = () => ({
                ...Car(250, "Red"),
                loadFurniture: undefined, // or null..., or use the delete operator?
            });
        ```
        Prototypal inheritance in JS allows us to easily remove offending methods from class. However, unless you use the
        delete operator, the key 'loadFurniture' will still stay in the object. This can lead to exceptions and bugs in your code on execution.
        JonScript resolves this by automatically deleting the key from object when removed, but at the same time, remembering the 'deleted'
        keys. JonScript does this so you can use multiple parent inheritance and dynamically delete properties you want from your classes.
* Objects and classes can inherit from each other
    As we discussed previously, class is simply a function that returns an object - in JonScript, we use this principle to create
    dynamic inheritance scheme which allows one to inherit from either an object or a class in either an object or a class.
* End of 'this'
    * JonScript syntax does not contain the 'this' keyword. Instead, all properties defined are accessed directly.
      This includes inherited properties. This principle is similar to C# classes.

Future releases with include these improvements:

* JSX
    JSX support would greatly simplify creating web apps with JonScript. Such extension of the language is warranted,
    given that it would allow you to quickly define a readable HTML-like constructs with syntax highlighting and the usage of
    frameworks like React, or Angular.
* Map files
    Map files are a well-known feature of Javascript language extensions. Map file allows a debugger, browser, or an editor
    to show user what the original code, pre-transpilation, looked like. This extension will be created with the help of typescript loaders,
    where JonScript will map to typescript and from that, to the transpiled JS code. This will also allow us to create ANTLR based
    Visual Studio Code extensions for debugging and syntax highlighting for JonScript.
* Better escapes for interpolated strings
    So far, interpolated strings must escape both '$', '{' and '}' characters. In JS one must only escape '$' character within interpolated string.
    This will require fixing the underlying ANTLR.
* Continue fixing the JS standard API
    Javascript std. API is old and underwent several updates. This leaves it in a state similar to the language itself. Consider these examples:
    ```typescript
        encodeUri() // Results in "undefined"
        [1, 2, 3].sort() // Mutates original array, even though .map and .filter are pure functions
        [1, 2, 3].reverse() // Also mutates the original array
        Object.keys // .keys, .values and .entries will all output keys with undefined values
        Math.max() // Returns -Infinity
        Math.min() // Returns Infinity
    ```
    JonScript strives to fix these errors and inconsistencies during postprocessing, by replacing these functions with an equivalent
    that does not, for example, list undefined properties/values. This is a work in progress, given the number of similar functions.
    JonScript cannot - and will not - change the standard API itself, due to compatibility concerns. It will only change those functions,
    when they are executed within JonScript, not within its imports.
* Singleton and internal classes
    Two new keywords will be added in later releases of JonScript:
        * static - when put in front of class definition, this keyword will make the class act like a singleton
        * internal - when put in front of class definition, this class will not be exported inside its module to other files
* Allow access to private members within the class context
    Given how JonScript transpiles classes into functions, you don't yet have access to private properties of the class
    of the same type passed into a parameter. Consider this example:
    ```typescript
        class Gambler {
            private money: number = 0;
            public gambleWith = (gambler: Gambler) => {
                return gambler.money <= this.money; // You can access gamblers money from Gambler class context
            }
        }
    ```
    So far, this is impossible to do in JonScript. Future release will add this option through the use of non-enumerable properties with extra typing.
* Optimize the build process
    Given time constraints and compatibility concerns for TS libraries, JonScript build is not optimized properly. Long build process is not unexpected
    in modern JS development. Main cause of de-optimization is complex post processing of generated Typescript code. The post processing is type-dependent
    and responsible for features like inheritance (need to generate dynamic properties to be able to get rid of 'this'), function safety etc.
* Build and publish VS Code JonScript extension
    Visual Studio Code is a popular IDE for programmers. It's developed by Microsoft and completely open source. It has a large extension library.
    These extensions provide everything from linting to syntax highlighting for a large portfolio of languages. I will develop an extension for
    VS Code in order to provide a way to debug and write JonScript. This connects to a previous point about map files.

Important technologies used in creating JonScript:

* Typescript
    In order to create a language with features such as context-safe guarantee, automatic async and more,
    I could not rely on Javascript alone. If I were to do so, one would, for example, have to check each object access to see whether
    or not accessed property was a function. The same is true for replacing functions - one would need to compare each function pointer
    to a series of functions within the std. api in order to replace them. This would likely bloat execution time. Pure Javascript solution
    would also create problematic situations when using inheritance - since JonScript does not use 'this' keyword, it would be difficult
    to automatically define properties in local scope in order to create this feature. One possible way to do this would be the 'with' keyword.

    For these reasons, I have decided to use Typescript. Typescript is a JS superset which gives you information about typing. This information is meant
    only for compile-time type checking. The philosophy of TS is such that it won't change anything during runtime. In order to realize
    JonScript features, I borrowed TS ability to infer types, coerce them, give information about properties of an object etc.

    Typescript alone is a very popular JS framework. It makes the JS enviroment much more accessible to people who are used to typed languages.
    Typescript also allows one to use most of what makes Javascript such a powerful language and has one of the most powerful type-inference
    tools available.

    While JonScript uses Typescript types and can import TS files, it does not keep the TS philosophy of non-interference. Instead,
    it uses TS typing to generate helpful constructs within the language. When using JonScript, programmer is heavily encouraged to take advantage
    of TS type inference. 

    Consider this example:

    ```typescript
        class Berry {
            a = 5;
        }
        const isBerry = (berry: Berry) => berry instanceof Berry; // Can this return false, if berry can only be Berry?
        isBerry({ a: 5 }); // Yes it can!
    ```

    Since TS recognizes, that Berry class is simply a function returning an object with property 'a' of type 'number', it will
    accept any object, which has a property 'a' with a numerical value. This goes against the instanceof operator, which checks the
    prototype inheritance chain of an object.

    This is one of the shortcomings of Typescript. While Typescript is an extremely powerful framework for safely writing JS,
    it cannot protect the programmer against some of the common shortcomings of JS. JonScript tries to prevent most of these errors
    by removing offending operators and functions.
* TS4ANTLR
    This NPM package is used to construct an AST tree from a typescript file, or to create AST TS tree and save it to a file.
    Both of these functions are used, since type inference cannot happen, until we have created the TS files. This splits JonScript
    parsing into two distinct phases - transpilation phase, where JonScript is parsed into Typescript, and post processing phase,
    where Typescript files generated are enhanced by creating constructs, which would be impossible without type inference/typing.
    Postprocessing constructs include: Removal of harmful/badly typed functions, safe context guarantees, defining inherited variables
    within the scope of child class etc.
* Webpack
    Webpack is a NPM package used to transpile many different JS frameworks into pure JS for use in browsers. Webpack is a wildly popular
    NPM package. JonScript exists as a Webpack module, with two separate modes: transpilation into Typescript, from which webpack creates
    JS files and direct JS transpilation (this is for now experimental). One simply needs to write out JonScript syntax, parse it through
    webpack and then create a Typescript file, which initializes the app. Polyfills are not included, since most of them are used for IE11,
    which is no longer officialy supported.
* NPM 
    NPM is the most popular package manager for JS and it's frameworks. It's used to manage both JonScript dependencies, and it's onw distributions.

Compromises
    * Falsy values
        The empty string and number zero are falsy values in Javascript. However, the class equivalents of these values are truthy. This cannot be
        changed in any JS framework that transpiles into JonScript. There are two alternative approaches for unification of falsyness. The first one
        would be to remove all use of String and Number classes. This approach was abandoned, since it would make it impossible for JonScript to overload
        numeric and string classes, which can be useful. Second approach, making all strings and numbers into their class-based equivalents was also abandoned,
        since it may interfere with TS typings, already existing package functionality (which may expect the number 0 passed into it to be falsy, for example)
        and the regular use of empty string/number 0 as special cases for rendering different component, using their falsyness. The solution in JonScript
        is to simply call the function valueOf on String/Number classes, if you need to use them as truthy/falsy values. The use of primitives is still the same,
        however, you cannot overload their properties using JonScript inheritance.
    * No .bind, .call, .apply
        Since JonScript automatically makes functions context-independent, you cannot re-bing function context in JonScript
    * No deep-object sanitization
        JonScript sanitizes primitive values NaN and null and turns them into undefined. It also removes undefined keys from object. However, if you pass
        an object from a Typescript module into JonScript, for performance and compatibility reasons, it will not be sanitized, unless you use it in inheritance,
        or assign a property from it into a JonScript object (in that case, only the assigned property will be sanitized). Any object used in inheritance
        is guaranteed to not be mutated during the process, through the use of Proxy class.
    * No instanceof
        Instanceof, similar to classes and arrow functions, was meant to make JS friendlier to programmers used to non-prototypal inheritance languages.
        However, it makes the programmer make a false assumption - in class based inheritance systems, you can use operators similar to instanceof to determine,
        for example, which child class is an instance of object. In class based systems, this is useful, since the class structure is highly indicative of object properties. In prototypal inheritance however, a child class can have completely different properties from the parent. Therefore, using this kind of a pattern
        can lead to unsafe assumptions. Since
        JonScript uses exclusively prototypal inheritance, both instanceof and typeof operators were removed in favor of the 'is' operator.
    * Slower run/build process
        JonScript will naturally run slower than regular JS, given it's safety features. In future releases, I will mitigate these effect by optimizing JonScript features.
        JonScript build is also slower that TS transpilation, since it does both a TS build and has to implement all the features listed above.
    * Limited re-definition of inherited properties
        Prototypal inheritance would normally allow you to redefine any property for your child class, however you want.
        Since JonScript uses TS alongside prototypal inheritance and it shortens property chains by removing 'this' (this is done by defining block-scoped variables), it does not allow you to change the type of inherited property with a public variable. It only allows you to redefine it, or remove it.
        This restriction does not apply to inherited properties from multiple classes, which can re-define each other in any fashion you want.

        Another limitation is that a class cannot inherit from another, if a public property of parent class has the same name as a constructor parameter.
        Also, since the post processing parser needs knowledge of properties of inherited class ahead of inheritance, circular inheritance,
        as well as inheriting from a class before it is defined is so far not possible.
    * Limited property names
        Given the easy-access clause of JonScript, if you define an object with properties that cannot also be variable names (easy access being facilitated
        by automatic variable creation), you will not be able to access these properties as simple variables, or inherit from such object. This restriction does not apply to object access, where you can use the '["prop"]' operator. 
    * Two different kinds of string and number
        Similar to Javascript, there are two different kinds of these primitives. Primitive strings and numbers can be falsy (e.g "" and 0), but cannot
        bear any additional properties (you cannot extend an instance of primitive string), while class-based String and Number cannot be falsy,
        but you can add extra properties on their instances. This is done for compatibility reasons for existing frameworks/libraries,
        and to allow JonScript developers to create Number-like and String-like classes.

* JonScript syntax
    * File: each JonScript file can contain 0..N imports and exactly 1 module
    * Imports:
        There are three different kinds of imports, similar to ES6+ JS:
            * default import: 'import $ from "jquery"' - import either default import or all exports as '$' from package 'jquery'
            * named import: 'import { uniq } from "lodash"' - import function 'uniq' from package 'lodash'
            * re-named import: 'import { uniq as uniq1 } from "lodash"' - import function 'uniq' and alias it as 'uniq1' from lodash
            Examples:
            ```
                import $ from "jquery" // import default export/all exports from a module named 'jquery'
                import { uniq } from "lodash" // import exported method 'uniq' from a module named 'lodash'
                import { uniq as unique } from "lodash" // import exported method uniq from lodash and rename it as 'unique' 
            ```
            Imports transpile into Typescript as expected.
    * Module:
        Modules are similar to namespaces in JS, or C#. They are simple objects that are transpiled into named exports in transpilation process.
        Each module can contain 0..N classes.
        Example:
        ```
            Module {
                // This is an empty module named Module
            }
        ```
        Transpiles roughly into:
        ```typescript
            export const Module = {};
        ```
    * Class:
        A class is, similar to JS, simply a function that returns an object. Here is the breakdown of what each class can contain:
        * 0..N templates
        * 0..N constructor parameters
        * 0..N private properties (methods and properties are the same thing from JonScript perspective)
        * 0..N inheritance statements
        * 0..1 '()' operator overloads
        * 0..N public properties, or 0..1 expression interface
        Example:
        ```
            Module {
                TestClass<T>( // Templated class TestClass with a template type T, which can be any type
                    array: T[], // Constructor parameter of type T[] named 'array'. Is required and cannot be undefined
                ) {
                    first: array[0], // Private property named 'first', assigned with the value of 'array[0]'
                    { // List of public properties
                        second: array[1], // A public property named 'second'
                        third: second, // Second public property
                    }
                }
            }
        ```
        Transpiles rougly into:
        ```typescript
            const TestClass = <T>(array: T[]) => {
                let first = array?.[0]; // Type inference for private property 'first'. Uses conditional chaining operator to protect against undefined exception
                return (() => { // Use inline functor for greater variable shadowing and re-definition flexibility
                    let second = array?.[1];
                    let third = second; // Here, public properties are defined outright as inline properties.
                    return new class _publicApi { // Using inline class definition since TS allows for a greater recursive typing with classes
                        second = second;
                        third = third;
                    };
                })();
            };

            export const Module = { TestClass: TestClass };
        ```
    * Template:
        Templates in JonScript function just like templates in Typescript. They only affect the transpilation process and do not
        affect the runtime (besides postprocessing certain types). Templates are made up of three parts:
        * 1 Template type name
        * 0..1 Type, to which templated type must be safely assignable to
        * 0..1 Type to be used by default
        
        Templates transpile as expected of TS templates.
    * Construtor parameters
        Similarly to JS object inheritance paradigm, constructor parameters are accessible to both private and public properties.
        Simply imagine a function that returns an object - This, in essence is the JonScript class. Special case of constructor parameter
        is the '_' - this parameter is to be ignored within the class body. The placeholder can be used multiple times inside the same function / object.
        This is an improvement over JS/TS, where it can be only used once. 
    * Private properties
        Private properties are defined right under the constructor, within the class body. Private properties will be transpiled into
        block-scoped variables. Special case of a private property '_'. The underscore property will not be defined as a block scoped variable,
        instead, the assigned expression will be executed.
    * Inheritance statements
        Inheritance statements are placed right under private properties. Essentially, any object, or functor, with fixed number of properties
        (no union/interesection types) can be used as a class parent. Each parent's properties can be used in the next parent. Each parents property
        will seamlessly shadow previous ones and will be available as public property. During inheritance, if no public properties were defined and the last
        parent is a functor, the child will be a functor as well.
        
        Example:
        ```
            Module {
                TestClass() {{
                    a: 1
                }}
                TestClass2() {{
                    b: 1
                }}
                TestClass3() {{
                    a: nil
                }}
                TestClass4() {
                    ...TestClass(),
                    ...TestClass2(),
                    ...TestClass3(),
                } // TestClass will have a single public property of 'b'. However, since it shadows 'a' as undefined, it will delete 'a' from inheritance chains
            }
        ```
    * '()' operator overload
        Every class can overload the '()' operator, making it either a function or a functor. Properties inherited will be kept and added to the resulting functor.
        Each functors properties will behave during inheritance as if they were inherited from a non-functor object.
        Example:
        ```
            Module {
                TestClass() {{
                    (a: number) => a + b,
                    b: 5,
                }} // Creates a functor with a public property of b. The result of calling the functor will be 'a + 5'
                // Example of the same with inheritance
                Parent() {{
                    b: 5
                }}
                Child() {
                    ...Parent(),
                    (a: number) => a + b // Expression as public properties of class. 'b' is automatically defined through inheritance
                }
            }
        ```
    * public properties
        Every class can define it's own public properties/methods. Classes in JonScript rely on type-inference, but with an extra step which allows for an object-like
        class definition to have recursive typing within the class public properties.
    * Expressions
        Each inheritance, private and public property or a parameter can be initialized by an expression.
            * functor - inline function definition / anonymous function
            * object - inline object definition (can inherit/be inherited from)
            * binary, or unary expression - 'await' operator, '[]' array access or numeric operations
            * function calls
            * array expression
            * property access - can be done with either the dot operator, or array access operator ([""])
        An expression can be used instead of public properties of an object. This is done so that one can easily create overloaded Number classes,
        overloaded arrays etc.

        Interesting example of a class with an expression instead of public properties:
        ```
            Module {
                TestClass(a: number) {
                    ...{{ // Inline object with a single public property can be inherited from
                        squared: () => a * a
                    }}
                    Number(a) // Creates a class with interface of a class of Number and an extra method of 'squared' through inheritance
                }
            }
        ```
        
        This allows us to use the 'is' operator on TestClass like this:
        
        ```
            TestClass(2) is Number // Returns true
        ```
    * Parameters
        There are three types of parameters
        * parameter with default value: does not need it's own type, is defined by using this syntax: 'param = expression'
        * spread parameter can be only the last parameter of function, is written as '...spread: number[]'. This parameter means
          that one can call this function as: 'fn(1, 2, 3, 4, 5)', which will be translated into the spread.
        * typed parameter: Ordinary TS-like parameter
    * Types
        Each function/constructor parameter has to have a type. Return types are only for functions and in most cases are optional.
        The only time a return type for a function is not optional is when it recursively returns a non-class type. There are several kinds of types:
        * Primitive - '0', 'false', '"value"' - the parameter may only be this primitive
        * Templated - The parameter must conform to the specified template, or it determines the end-result of the templating operation implicitly.
        * Union/Intersection - Same as TS parameters
        * Standart type parameter - Same as TS, aka: String, string, Number or Number etc.
        * Class based type - Class<{classname}>. This type transpiles into ReturnType<typeof classname>. This is a special type designed to
          allow one to easily manipulate with JonScript classes
        * Type with template - Examples: Partial<Class<type>>, similar to typescript. In the example, the type resolves into an object with
          same properties as the class, except all of them are optional

        The types in JonScript are a subset of Typescript types. If you need to use full TS typing capacity, simply define a Typescript file
        with such types and import it into JonScript.

Pattern examples

In order to showcase how JonScript differs/improves existing Typescript options when writing a class, I will write several well-known patterns
both in Typescript and in JonScript and compare the differences.

Builder pattern:
    Builder patterns are an easy way to define complex objects without having to write a function with multiple optional parameters. Builder
    patterns consist of an object, which contains several functions with a - usually - single parameter, which return itself in order to be available
    for more configuration. Consider these examples:

    ```typescript

    /**
     * Builder for an e-shop offering a user to customize purchased animal
     */
    class Builder() {
        private species: string = "";
        private claws: boolean = false;
        private color: "Red" | "Black" | "Blue" | "Brown" = "Black";
        private domesticated: boolean = false;
        setSpecies = (species: string) => {
            this.species = species;
            return this;
        };
        setClaws = (claws: boolean) => {
            this.claws = claws
            return this;
        };
        setColor = (color: Builder["color"]) => {
            this.color = color;
            return this;
        };
        setDomesticated = () => {
            this.domesticated = domesticated;
            return this;
        };
        resolve = () => ({
            species, claws, color, domesticated,
        })
    }

    ```
    Now compare that to an example of JonScript
    ```
        import Color from "./Color" // External color enum import
        Builder { // Module
            Body(defined: {
                species?: string = "",
                claws?: boolean = false,
                color?: Color = "Black",
                domesticated?: boolean = false,
            }) {
                ...defined,
            }
            Builder(body = Body({})) { // Initialize empty body
                species: (species: string) => Builder({
                    ...body, // Inheritance within object
                    { species, } // Public properties are marked as '{}'
                }),
                claws: (claws: boolean) => Builder({
                    ...body,
                    { claws, }
                }),
                color: (color: Color) => Builder({
                    ...body,
                    { color, }
                }),
                domesticated: (domesticated: boolean) => Builder({
                    ...body,
                    { domesticated, }
                }),
                resolve: () => body,
            }
        }
    ```
    Since builder is more typically a pattern used in imperative OOP programming - and JonScript encourages functional programming,
    in this example JonScript would be at a disadvantage. Especially considering it needs to import the 'Claws' type from outside.
    
    Where JonScript gains more footing is the next example: Let's say we need to create a class which deals with complex numbers.
    Consider this Typescript example:
    ```typescript
        class Complex extends Number {
            private real: number;
            private i: number;
            constructor(real: number = 0, i: number = 0) {
                this.real = real;
                this.i = i;
            }
            public get i() {
                return this.i;
            };
            public plus = (complex: Complex | number) => {
                if (typeof complex === "number" || complex instanceof Number) {
                    return new Complex(this.real + complex, this.i);
                }
                return new Complex(this.real + complex.real, this.i + complex.i);
            };
            public minus = (complex: Complex | number) => {
                return new Complex(this.real - complex.real, this.i - complex.i);
            };
            // And multiply, divide and all the other operations below
        }
    ```

    However, consider the use! Javascript has no operator overloading, so each operation needs to be called as function.
    Also, typeof operation on instance of Complex will return an object, not a number (the instanceof operator will return correctly however).
    Also, operators like + or - will sort-of work on this class - they will use the function valueOf in order to determine the value to be added/subtracted.

    Now, let's see how we can do the same in JonScript:
    ```
        Test {
            Complex(r: number, i: number) {
                abs: () => Math.sqrt(Math.pow(r, 2) + Math.pow(i, 2)),
                op: {{
                    absolute: abs,
                    type: "Complex",
                    real: r,
                    imaginary: i,
                    plus: (next: number | Number | Object<op>) => next is {{ type: "Complex" }}
                        ? Complex(next.real + r, next.imaginary + i)
                        : Complex(next + real, i),
                }},
                ...op,
                Number(abs())
            }
        }
    ```

    Not only we can overload operators for numerical operations in JonScript, we can also define a fallback,
    where the Number part of the Complex class will behave correctly. This can be done in few lines and you don't need
    to concern yourself with number/Number differences (except typing). Notice that this will work regardless of order of
    operands with plus! Not only you don't have to explicitly call functions, but much of the work is done for you already.

Description of build process
    * Pre-processing - Find and collect all JonScript files pertaining to build
    * Compilation - Take all JonScript files and parse them into Typescript files inside 'jons-temp' folder. Main parser features:
        * Import parser - parses imports 1:1 to Typescript
        * Module parser - parses modules into exported variables with classes as properties of exported object
        * Class parser - parses a class into non-exported, local arrow function
        * Assignment parser - parses object assignment into a local variable and object assignment of said variable. 
        * Expression parser - parses expressions, described in detail above
            * Objects are parsed as immediately executed arrow functions returning a class
            * Rest is parsed in TS-similar fashion
        * Injects default imports for pattern matching functions, sanitizers (NaN to undefined etc.)
    * Postprocessing - Take all parsed typescript files and transpile them into AST trees. There are several operations performed on each tree:
        * Change 'var' defined variables into 'const' and 'let' variables as needed (in later versions, this will be moved into compilation part)
        * If there is a call for a function with a construct signature (constructor), a 'new' keyword is inserted
        * If there is any function call, constructor call, dot access or array access, optional chaining is automatically applied
        * If there is a call to Object.entries, .keys, .values, the call is automatically replaced with a custom function,
          which corrects typing (normally, these functions translate object keys into string - this is changed to work with string enums)
          and removes any keys, under which the values in object are NaN, null or undefined.
        * If there is a call to .push method on an array, JonScript generates a warning and replaces this method with concat
        * Automatically inserts 'async' keyword when needed
        * Defines an inheritance api with appropriate properties - every property on every inherited object is defined as local property.
          These properties can redefine each other and will have appropriate typing - one can even use inherited properties within another
          inheritance statement. Consider this example:
          ```
            Module {
                TestClass() {
                    ...{{
                        a: 5
                    }},
                    ...{{
                        b: a + 1 // a is a number
                    }},
                    ...{{
                        a: "value" // a turns to string
                    }},
                    ...{{
                        b: a + "is a string" // b turns to string
                    }},
                    () => b // b is string
                }
            }
          ```
        * Inheriting from a promise is the same as inheriting from a resolved promise

Similar works

* Coffeescript
    Coffeescript aims to remove Java influence from the Javascript language, leaving it with more similarities to functional programming language.
    To that end, it also adds FP features within the language, such as re-creating ternary conditional operator with the keyword 'if'.
    Coffeescript is compatible with any JS library and with JSX (requires transpilation).
    Coffeescript can be used with Typescript (through compiled-coffee package).

    The benefits of Coffeescript are one-to-one compilation into Javascript, easy syntax and guaranteed compatibility. JonScript differs from
    Coffeescript philosophy in that it tries to enhance existing Javascript concepts, such as classes or pattern matching and it's explicit
    dependence on Typescript.

* ESLint
    ESLint and other linting tools in Javascript allow you to avoid some common pitfalls of JS development (e.g. using == instead of ===, or
    not using .catch function with promises). However, ESLint cannot change runtime behaviour (similar to Typescript) and it may cause
    additional problems when you need to, for example, use a library where an object has a '.then' function (example: Cypress), but
    the object is not a promise.

    JonScript differs from ESLint in a similar way it differs from Typescript - it tries to influence both compile-time and runtime.

* JSX
    JSX is a powerful language extension allowing you to write HTML-like constructs within JS code. This allows you to quickly
    create a very readable version of a website purely with JS. JSX requires to be transpiled to run in a browser. JSX will
    be added to JonScript in a later version.

* JS++
    JS++ is very similar to Typescript, except it tries to impose a more strict version of typing, reminiscent of C++.
    This framework supports imperative, object oriented and functional programming. Last version was released in 2019
    and it is relatively unknown - it does not have NPM presence. It also enforces it's typing system during runtime.
    Since it affects both compile-time and runtime execution, it is perhaps the most similar package to JonScript
    on this list.

* Amber
    Amber is a SmallTalk dialect that can be compiled into Javascript. Similar to SmallTalk, it does not have any typing.
    Amber is a pure OOP language, with a small user base and a very small presence on NPM.

There are over 40 different ways of transpiling Javascript. JonScript aims to take the best parts of existing projects
and add it's own features to create a more powerful, simpler version of the most used language extensions of today.