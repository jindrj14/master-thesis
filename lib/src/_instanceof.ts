import matchto from "matchto";
import { MatchValue } from "matchto/types";
import { _sanitize } from "./_sanitize";

export declare type Simplify<T> = T extends boolean
  ? boolean
  : T extends number
  ? number
  : T extends string
  ? string
  : T extends object
  ? T
  : never;

type Resolve<T> = T extends null | undefined
  ? null | undefined
  : T extends (new (...args: any) => infer P)
  ? Simplify<P>
  : T extends string | String | RegExp
  ? string | String
  : T extends number | NumberConstructor
  ? number | Number
  : T extends symbol | SymbolConstructor
  ? symbol
  : T extends bigint | BigIntConstructor
  ? bigint
  : T extends boolean | BooleanConstructor
  ? boolean
  : T extends Array<infer U>
  ? Resolve<U>
  : T extends ArrayConstructor
  ? []
  : T extends Date | DateConstructor
  ? Date
  : T extends Object | ObjectConstructor
  ? { [K in keyof T]?: Resolve<T[K]>; }
  : never;

const matchFunction = (value: any, match: any) => {
  if (typeof value === "function" && Object.keys(value).length > 0) {
    return undefined;
  }
  if (typeof match === "function" && Object.keys(match).length > 0) {
    return undefined;
  }
  if (
    typeof value === "function" &&
    typeof match === "function" &&
    value === match
  ) {
    return true;
  }
  if (typeof match === "function" && !match.prototype?.constructor.name) {
    return false;
  }
  return undefined;
};

type AppendConstructor<T> = T extends (new (...args: any) => infer P) ? P : {};

///@ts-ignore
export const _is = <T, E extends MatchValue<T> = MatchValue<T>>(value: T, match: AppendConstructor<T> | E): value is Resolve<E> => {
  value = _sanitize(value);
  match = _sanitize(match as any);
  try {
    return Boolean(
      matchto(value as any, "first", [matchFunction])
        .to(match as any)
        .solve()
    );
  } catch {
    return false;
  }
};
