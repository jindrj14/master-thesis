type FnOrObj<T> = T extends ((...args: any[]) => any) ? ((...args: Parameters<T>) => ReturnType<T>) : {};

type Combine<T, E> = T extends Promise<infer P>
    ? Promise<Combine<P, E>>
    : T & E extends never
    ? {
        [K in Exclude<keyof T, keyof E>]: T[K]
    } & {
        [K in Exclude<keyof E, keyof T>]: E[K]
    } & {
        [K in (keyof T & keyof E)]: (T | E)[K]
    } & FnOrObj<T> : T & E;

export const _silentSpread = <T>(val: T) => ({ ...val });

export const _enhance = <T, E>(publicObj: T, object: E): Combine<T, E> => {
    if (publicObj instanceof Promise) {
        const originalThen = publicObj.then.bind(publicObj);
        publicObj.then = (...args: any[]) => {
            const fullfilled = (val: any) => args[0]?.(_enhance(val, object));
            const rejected = args[1];
            return originalThen(fullfilled, rejected);
        };
        return publicObj as any;
    }
    if (Object.getOwnPropertyNames(publicObj).length === 0
        && Object.getPrototypeOf(publicObj) === Object
        && typeof publicObj === "object") {
        return object as any;
    }
    const publicKeysSet = Object.keys(publicObj).reduce((p: Record<string, boolean>, c) => {
        p[c] = true;
        return p;
    }, {});
    Object.keys(object).forEach(key => {
        if (!publicKeysSet[key]) {
            (publicObj as any)[key] = (object as any)[key];
        }
    });

    return publicObj as any;
};

export const _condMerge = <T, E>(inheritanceApi: T, mergeInto: E): E extends Function ? (T & E) : T => {
    const safe = _safeCopy(mergeInto);
    return _enhance(safe, inheritanceApi) as any;
}

export const _coerce = <T>(_: any): _ is T => true;

const _bind = (obj: any, val: any) => {
    if (val instanceof Function) {
        return val.bind(obj);
    }
    return val;
};

export const _safeCopy = <T>(expr: T): T => {
    if (typeof expr === "object" || typeof expr === "function") {
        const overridableKeys = new Set(Object.getOwnPropertyNames(expr));
        const fakeWrapperCollection: any = {};
        const wrapper: any = new Proxy(expr as any, {
            ownKeys: () => {
                return [...new Set([...Object.getOwnPropertyNames(expr), ...Object.keys(fakeWrapperCollection)])];
            },
            getOwnPropertyDescriptor: (target, prop) => {
                const originalDescriptor = Object.getOwnPropertyDescriptor(target, prop);
                if (originalDescriptor?.configurable === false) {
                    return originalDescriptor;
                }
                if (fakeWrapperCollection[prop] !== undefined) {
                    return Object.getOwnPropertyDescriptor(fakeWrapperCollection, prop);
                }
                return originalDescriptor;
            },
            set: (target, prop, value) => {
                if (overridableKeys.has(prop as any)) {
                    target[prop] = value;
                } else {
                    fakeWrapperCollection[prop] = value;
                }
                return true;
            },
            get: (_, prop) => {
                if (Object.keys(fakeWrapperCollection).includes(prop as any)) {
                    return fakeWrapperCollection[prop];
                }
                return _bind((expr as any), (expr as any)[prop]);
            },
        });
        return wrapper;
    }
    return expr;
}