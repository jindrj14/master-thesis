export const _try = <T, E>(
    expr: () => T,
    c: (ex: any) => E,
    f?: (value?: T) => E,
) => {
    let val: T | undefined = undefined;
    let error: any | undefined = undefined;
    try {
        val = expr();
        return val;
    } catch (e) {
        error = e;
    } finally {
        if (error !== undefined) {
            return c(error);
        }
        return f?.(val) ?? val;
    }
};

export const _throw = (value: string | Error) => {
    ///@ts-ignore
    throw value;
};
