import { _allowedNullables } from "./_constants";

export const _sanitize = <T>(expression: T): T => {
    const typeless = expression as any;
    if (expression || _allowedNullables.includes(typeless)) {
        return expression;
    }
    return undefined as any;
};

export const _sanitizeObject = <T>(expression: T): T => {
    Object.keys(expression).forEach(key => {
        if ((expression as any)[key as any] === undefined) {
            delete (expression as any)[key as any];
        }
    });
    if (expression instanceof Promise) {
        const originalThen = expression.then.bind(expression);
        expression.then = (...args: any[]) => {
            const fullfilled = (val: any) => args[0]?.(_sanitizeObject(val));
            const rejected = args[1];
            return originalThen(fullfilled, rejected);
        };
    }
    return expression;
};
