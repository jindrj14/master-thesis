import { _sanitize } from "./_sanitize";

const hasValueOf = <T>(a: T | { valueOf: () => any }): a is { valueOf: () => any } => (a as any)?.valueOf instanceof Function && (a as any)?.valueOf.length === 0;

const getValueOf = (a: any) => {
    if (hasValueOf(a)) {
        return a.valueOf();
    }
    return a;
};

const hasMethod = (a: any, method: string) => a?.[method] && typeof a[method] === "function" && a[method].length === 1;

type NumOperation = "minus"
    | "multiply"
    | "divide"
    | "modulo";

type CompareOperation = "equals"
    | "nequals"
    | "greater"
    | "less"
    | "greaterOrEquals"
    | "lessOrEquals";

interface Plus {
    // a: string
    (a: string, b: string): string;
    (a: string, b: number): string;
    (a: string, b: String): String;
    (a: string, b: Number): String;
    // a: String
    (a: String, b: string): String;
    (a: String, b: number): String;
    (a: String, b: String): String;
    (a: String, b: Number): String;
    // a: number
    (a: number, b: string): string;
    (a: number, b: number): number;
    (a: number, b: String): String;
    (a: number, b: Number): Number;
    // a: Number
    (a: Number, b: string): String;
    (a: Number, b: number): Number;
    (a: Number, b: String): String;
    (a: Number, b: Number): Number;
    // a: Custom
    <T, E>(a: T, b: E): T extends { plus: (p: E) => infer TReturn } ? TReturn : undefined;
    <T, E>(a: E, b: T): T extends { plus: (p: E) => infer TReturn } ? TReturn : undefined;
}

interface NumOp<K extends NumOperation> {
        // a: number
        (a: number, b: number): number;
        (a: number, b: Number): Number;
        // a: Number
        (a: Number, b: number): Number;
        (a: Number, b: Number): Number;
        // a: Custom
        <T, E>(a: T, b: E): T extends Record<K, (p: E) => infer TReturn> ? TReturn : undefined;
        <T, E>(a: E, b: T): T extends Record<K, (p: E) => infer TReturn> ? TReturn : undefined;
}

interface BoolOp {
    <T>(a: T, b: T): boolean;
    <T, E>(a: T, b: E): T extends { compare: (p: E) => number } ? boolean : false;
    <T, E>(a: E, b: T): T extends { compare: (p: E) => number } ? boolean : false;
}

type PlusOperation = { "plus": Plus };

type NumOperations = {
    [K in NumOperation]: NumOp<K>;
};

type CompareOperations = {
    [K in CompareOperation]: BoolOp;
};

type Operation = PlusOperation & NumOperations & CompareOperations;

const validateComparisonOperation = (...args: any[]) => args.every((arg: any) => {
    if (arg instanceof String || arg instanceof Number) {
        return true;
    }
    const type = typeof arg;
    if (type === "string" || type === "number") {
        return true;
    }
    return false;
});

const prepareNumericalOperation = (a: any, b: any) => {
    const isAClass = a instanceof Number;
    const isBClass = b instanceof Number;
    const isANumber = typeof a === "number";
    const isBNumber = typeof b === "number";
    const valid = (isAClass || isANumber) && (isBClass || isBNumber);
    return {
        isClass: isAClass || isBClass,
        isValid: valid,
        valueA: valid && isAClass ? a.valueOf() : a,
        valueB: valid && isBClass ? b.valueOf() : b,
    };
};

const prepareStringOperation = (a: any, b: any) => {
    if (b === undefined && (a instanceof String || typeof a === "string")) {
        b = "";
    }
    if (a === undefined && (b instanceof String || typeof b === "string")) {
        a = "";
    }
    const isAClass = a instanceof String || a instanceof Number;
    const isBClass = b instanceof String || b instanceof Number;
    const isAString = typeof a === "string" || typeof a === "number";
    const isBString = typeof b === "string" || typeof b === "number";
    const valid = (isAClass || isAString) && (isBClass || isBString);
    return {
        isClass: isAClass || isBClass,
        isValid: valid,
        valueA: valid && isAClass ? a.valueOf() : a,
        valueB: valid && isBClass ? b.valueOf() : b,
        isNumericClass: a instanceof Number && b instanceof Number,
    };
};

const applyOverload = (a: any, b: any, method: string) => {
    if (hasMethod(a, method)) {
        return { applied: a[method](b) };
    }
    if (hasMethod(b, method)) {
        return { applied: b[method](a) };
    }
    return undefined;
};

const methods: Operation = {
    plus: (a: any, b: any) => {
        const appliedOverload = applyOverload(a, b, "plus");
        if (appliedOverload) {
            return appliedOverload.applied;
        }
        const strOp = prepareStringOperation(a, b);
        if (strOp.isValid) {
            const result = strOp.valueA + strOp.valueB;
            if (strOp.isClass) {
                if (strOp.isNumericClass) {
                    return new Number(result);
                }
                return new String(result);
            }
            return result;
        }
        return undefined as any;
    },
    minus: (a: any, b: any) => {
        const appliedOverload = applyOverload(a, b, "minus");
        if (appliedOverload) {
            return appliedOverload.applied;
        }
        const op = prepareNumericalOperation(a, b);
        if (op.isValid) {
            const result = op.valueA - op.valueB;
            if (op.isClass) {
                return new Number(result);
            }
            return result;
        }
        return undefined as any;
    },
    multiply: (a: any, b: any) => {
        const appliedOverload = applyOverload(a, b, "multiply");
        if (appliedOverload) {
            return appliedOverload.applied;
        }
        const op = prepareNumericalOperation(a, b);
        if (op.isValid) {
            const result = op.valueA * op.valueB;
            if (op.isClass) {
                return new Number(result);
            }
            return result;
        }
        return undefined as any;
    },
    divide: (a: any, b: any) => {
        const appliedOverload = applyOverload(a, b, "divide");
        if (appliedOverload) {
            return appliedOverload.applied;
        }
        const op = prepareNumericalOperation(a, b);
        if (op.isValid) {
            const result = op.valueA / op.valueB;
            if (op.isClass) {
                return new Number(result);
            }
            return result;
        }
        return undefined as any;
    },
    modulo: (a: any, b: any) => {
        const appliedOverload = applyOverload(a, b, "modulo");
        if (appliedOverload) {
            return appliedOverload.applied;
        }
        const op = prepareNumericalOperation(a, b);
        if (op.isValid) {
            const result = op.valueA % op.valueB;
            if (op.isClass) {
                return new Number(result);
            }
            return result;
        }
        return undefined as any;
    },
    equals: (a: any, b: any) => {
        const appliedOverload = applyOverload(a, b, "compare");
        if (appliedOverload) {
            return appliedOverload.applied === 0;
        }
        return getValueOf(a) === getValueOf(b);
    },
    nequals: (a: any, b: any) => {
        const appliedOverload = applyOverload(a, b, "compare");
        if (appliedOverload) {
            return appliedOverload.applied !== 0;
        }
        return getValueOf(a) !== getValueOf(b);
    },
    greater: (a: any, b: any) => {
        const appliedOverload = applyOverload(a, b, "compare");
        if (appliedOverload) {
            return appliedOverload.applied > 0;
        }
        if (!validateComparisonOperation(a, b)) {
            return false;
        }
        return getValueOf(a) > getValueOf(b);
    },
    less: (a: any, b: any) => {
        const appliedOverload = applyOverload(a, b, "compare");
        if (appliedOverload) {
            return appliedOverload.applied < 0;
        }
        if (!validateComparisonOperation(a, b)) {
            return false;
        }
        return getValueOf(a) < getValueOf(b);
    },
    greaterOrEquals: (a: any, b: any) => {
        const appliedOverload = applyOverload(a, b, "compare");
        if (appliedOverload) {
            return appliedOverload.applied >= 0;
        }
        if (!validateComparisonOperation(a, b)) {
            return false;
        }
        return getValueOf(a) >= getValueOf(b);
    },
    lessOrEquals: (a: any, b: any) => {
        const appliedOverload = applyOverload(a, b, "compare");
        if (appliedOverload) {
            return appliedOverload.applied <= 0;
        }
        if (!validateComparisonOperation(a, b)) {
            return false;
        }
        return getValueOf(a) <= getValueOf(b);
    },
};

export const _o = Object.entries(methods).reduce((p: { [key: string]: Function }, [key, value]) => {
    p[key] = (...args: any[]) => _sanitize((value as any)(...(args.map(_sanitize)))) 
    return p;
}, {}) as Operation;