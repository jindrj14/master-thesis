import { _allowedNullables } from "./_constants";

export const _safeKeys = (_ : any) => <T>(object: T): T extends [] ? string[] : Array<keyof T> => Object
    .keys(object)
    .filter(key => (object as any)[key] || _allowedNullables.includes((object as any)[key])) as any;

export const _safeValues = (_ : any) => <T>(object: T): T extends Array<infer P> ? P[] : Array<T[keyof T]> => Object
    .keys(object)
    .filter(key => (object as any)[key] || _allowedNullables.includes((object as any)[key]))
    .map(key => (object as any)[key]) as any;

type Entry<T, E extends keyof T> = [E, T[E]];

export const _safeEntries = (_ : any) => <T>(object: T): T extends Array<infer P> ? Array<[string, P]> : Array<Entry<T, keyof T>> => Object
    .keys(object)
    .filter(key => (object as any)[key] || _allowedNullables.includes((object as any)[key]))
    .map(key => [key, (object as any)[key]]) as any;

export const _safePush = <T>(array: T[]) => (item: T) => array.concat(item); 
