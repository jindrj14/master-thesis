const fs = require("fs");

const instanceOfPath = "./dist/_instanceof.d.ts";
const instanceOfDTs = fs.readFileSync(instanceOfPath, "utf-8");

fs.writeFileSync(instanceOfPath, instanceOfDTs.replace(/(\n|\r\n)export declare const _is:/g, "\n///@ts-ignore\nexport declare const _is:"));