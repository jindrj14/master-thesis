import { ArrowFunction, ElementAccessExpression, PropertyAccessExpression, ts } from "ts-morph";
import { findDeclarationType } from "./findDeclarationType";

const isPrimitive = (text: string) => {
    if (/^\"(.*)\"$/.test(text)) {
        return true;
    }
    if (!isNaN(parseInt(text))) {
        return true;
    }
    if (["undefined", "true", "false"].includes(text)) {
        return true;
    }
    return false;
};

export const functionSafety = (
    descendant: ElementAccessExpression<ts.ElementAccessExpression> | PropertyAccessExpression<ts.PropertyAccessExpression>,
    definedFns: ArrowFunction[],
    cleanup: () => void,
) => {
    if (!descendant.wasForgotten() && descendant.getType().getCallSignatures().length > 0 && !findDeclarationType(descendant, definedFns)) {
        let left = descendant.getChildren();
        left = left.filter((_, i) => i < left.length - 2);
        const right = descendant.getLastChildByKind(ts.SyntaxKind.ExpressionStatement)
            || descendant.getLastChildByKind(ts.SyntaxKind.Identifier)
            || descendant.getLastChildByKind(ts.SyntaxKind.PrivateIdentifier);
        const guaranteedParsed = ["_expr", "_toBind", "_inheritanceApi", "_o"];
        if (left && right && !right.getType().isClass() && !guaranteedParsed.some(g => left.map(l => l.getText()).join("").includes(g))) {
            const accessor = isPrimitive(right.getText()) ? `[${right.getText()}]` : right.getText();
            descendant.replaceWithText(
                `(() => { const _expr = ${
                    left.map(l => l.getText()).join("")
                }; const _toBind = _expr?.${
                    accessor
                }; return _enhance(_toBind.bind(_expr), _toBind); })()`
            );
            cleanup();
        }
    }
}