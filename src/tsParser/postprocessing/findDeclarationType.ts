import { ArrowFunction, Node, ts } from "ts-morph";

export const findDeclarationType = (
    callee: Node<ts.Node>,
    definedFns: Array<ArrowFunction>,
) => definedFns.some(fn => {
    return callee.getType().getCallSignatures().some(c => c.getDeclaration() === fn);
});