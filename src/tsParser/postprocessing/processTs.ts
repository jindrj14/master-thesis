import { Project, VariableDeclarationKind, Node, ts, ArrowFunction } from "ts-morph";
import { resolve, join } from "path";
import { listOurFiles } from "./listOurFiles";
import { replaceByType } from "./replaceByType";
import { materializeUnit } from "./materializer";
import { functionSafety } from "./functionSafety";

export const processTs = (path: string, files: string[], build?: boolean) => {
    const project = new Project({
        compilerOptions: {
            strict: true,
            sourceRoot: path,
        }
    });
    project.addSourceFilesAtPaths(path + "/**/*.ts");
    files = files.map(file => resolve(join(path, file + ".ts")));
    const definedFns: ArrowFunction[] = [];
    let ourFiles = listOurFiles(project, files);
    ourFiles.forEach(file => {
        const upperStatements = file.getVariableStatements();
        upperStatements.forEach(declaration => {
            declaration.forEachDescendantAsArray().forEach(descendant => {
                if (Node.isArrowFunction(descendant)) {
                    definedFns.push(descendant);
                }
            });
        });
    });
    ourFiles.forEach(file => {
        // Only JonScript files here!
        // Replace all 'var' with 'let', except for classes and module
        const varSt = file.getVariableStatements();
        varSt.forEach(st => {
            st.setDeclarationKind(VariableDeclarationKind.Const);
            st.forEachDescendantAsArray().forEach(descendant => {
                if (Node.isVariableStatement(descendant) && descendant.getDeclarationKindKeyword().getText().includes("var")) {
                    descendant.setDeclarationKind(VariableDeclarationKind.Let);
                }
            });
        });
    });

    const start = new Date();
    // Separate inheritance api
    const sorted = [...ourFiles].sort((a, b) => {
        const aPath = a.getFilePath();
        const bPath = b.getFilePath();
        return files.indexOf(aPath) - files.indexOf(bPath);
    });
    sorted.forEach(file => {
        let cleanup = true;
        while (cleanup) {
            cleanup = false;
            if (materializeUnit(file)) {
                cleanup = true;
            }
        }
    });
    console.log(`postprocessing inheritance took: ${Math.floor((new Date().getTime() - start.getTime()) / 1000)}s`)

    let cleanup = true;

    while (cleanup) {
        cleanup = false;

        // Add async function to any file in which there is a non/function await descentant
        ourFiles.forEach(file => file
            .getDescendantsOfKind(ts.SyntaxKind.ArrowFunction)
            .forEach(descendant => {
                if (!descendant.wasForgotten()) {
                    let hasAwait = false;
                    descendant.forEachDescendant((node, traversal) => {
                        if (!node.wasForgotten()) {
                            if (Node.isAwaitExpression(node)) {
                                hasAwait = true;
                                traversal.stop();
                            }
                        }
                    });
                    if (hasAwait && !descendant.isAsync()) {
                        descendant.setIsAsync(true);
                        cleanup = true;
                    }
                }
            }),
        );

        // Turn [].push into concat, Object.keys/values/entries into _safe versions, disable setPrototypeOf, defineProperties and defineProperty
        replaceByType(ourFiles, "ObjectConstructor", "keys", replace => `_safeKeys(${replace})`, () => (cleanup = true));
        replaceByType(ourFiles, "ObjectConstructor", "values", replace => `_safeValues(${replace})`, () => (cleanup = true));
        replaceByType(ourFiles, "ObjectConstructor", "entries", replace => `_safeEntries(${replace})`, () => (cleanup = true));
        replaceByType(
            ourFiles,
            type => type.isArray(),
            "push",
            replace => `_safePush(${replace})`,
            () => (cleanup = true),
            "Array.prototype.push method is impure. Use concat instead. Push use replaced by concat automatically.",
        );

        // Re-bind context to prevent null/undefined context errors on function() {}
        ourFiles.forEach(file => file
            .getDescendantsOfKind(ts.SyntaxKind.ElementAccessExpression)
            .forEach(descendant => {
                functionSafety(descendant, definedFns, () => (cleanup = true));
            }),
        );
        ourFiles.forEach(file => file
            .getDescendantsOfKind(ts.SyntaxKind.PropertyAccessExpression)
            .forEach(descendant => {
                functionSafety(descendant, definedFns, () => (cleanup = true));
            }),
        );
        // Always use class calls when dealing with function/class: Date() -> new Date() etc.
        ourFiles.forEach(file => file
            .getDescendantsOfKind(ts.SyntaxKind.CallExpression)
            .forEach(descendant => {
                if (!descendant.wasForgotten() && !descendant.forEachDescendantAsArray().some(d => d.getKind() === ts.SyntaxKind.NewKeyword)) {
                    const callee = descendant.getFirstChild();
                    if (Node.isExpression(callee)
                        && callee.getType().getText() !== "BooleanConstructor"
                        && callee.getType().getConstructSignatures().length > 0) {
                        descendant.replaceWithText(`(() => { const _c = ${
                                callee.getText()
                            }; return _c ? new _c${
                                descendant.getChildren().filter((_, i) => i > 1).map(d => d.getText()).join("")
                            } : undefined!; })()`);
                        cleanup = true;
                    }
                }
            }),
        );

        project.saveSync();
        ourFiles = listOurFiles(project, files);
    }
    if (build) {
        project.emitSync();
    }
};