import { ts, Type, ElementAccessExpression, PropertyAccessExpression, SourceFile } from "ts-morph";

const replaceWithExpression = (
    descendant: ElementAccessExpression<ts.ElementAccessExpression> | PropertyAccessExpression<ts.PropertyAccessExpression>,
    leftType: string | ((type: Type<ts.Type>) => boolean),
    rightMethod: string,
    replace: (left: string) => string,
    cleanup: () => void,
    warning?: string,
) => {
    if (!descendant.wasForgotten()) {
        let left = descendant.getChildren();
        left = left.filter((_, i) => i < left.length - 2);
        const right = descendant.getLastChildByKind(ts.SyntaxKind.ExpressionStatement)
            || descendant.getLastChildByKind(ts.SyntaxKind.Identifier)
            || descendant.getLastChildByKind(ts.SyntaxKind.PrivateIdentifier);
        if (left && right) {
            const leftText = left.map(l => l.getText()).join("");
            if (leftText !== "_inheritanceApi") {
                const determineFrom = left[left.length - 1];
                const determineType = determineFrom.getType();
                if ((typeof leftType === "function" ? leftType(determineType) : determineType.getText() === leftType) && right.getText() === rightMethod) {
                    descendant.replaceWithText(replace(leftText));
                    cleanup();
                    if (warning) {
                        console.warn(warning);
                    }
                }
            }
        }
    }
};

export const replaceByType = (
    ourFiles: SourceFile[],
    leftType: string | ((type: Type<ts.Type>) => boolean),
    rightMethod: string,
    replace: (left: string) => string,
    cleanup: () => void,
    warning?: string,
) => {
    ourFiles.filter(file => file.getText().includes(rightMethod)).forEach(file => file
        .getDescendantsOfKind(ts.SyntaxKind.ElementAccessExpression)
        .forEach(descendant => {
            replaceWithExpression(
                descendant,
                leftType,
                rightMethod,
                replace,
                cleanup,
                warning,
            );
        }),
    );
    ourFiles.filter(file => file.getText().includes(rightMethod)).forEach(file => file
        .getDescendantsOfKind(ts.SyntaxKind.PropertyAccessExpression)
        .forEach(descendant => {
            replaceWithExpression(
                descendant,
                leftType,
                rightMethod,
                replace,
                cleanup,
                warning,
            );
        }),
    );
}