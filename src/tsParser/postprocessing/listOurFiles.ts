import { Project } from "ts-morph";

export const listOurFiles = (
    project: Project,
    ourFiles: string[],
) => project
    .getSourceFiles()
    .filter(file => ourFiles.includes(file.getFilePath()));