import { ts, SourceFile, Project, Type, Node, printNode, Symbol, ReferencedSymbol, CallExpression, JSDoc } from "ts-morph";
import path from "path";
import { copyFileSync, unlinkSync, existsSync, readFileSync } from "fs";

const getLimits = (source: Node<ts.Node>) => {
    let end = -1;
    source.getDescendantsOfKind(ts.SyntaxKind.JSDocComment).forEach(comment => {
        if (end !== -1) {
            return;
        }
        if (!comment.wasForgotten() && comment.getText() === "/** #!endifinheritance */") {
            end = comment.getStartLineNumber();
        }
    });
    if (end === -1) {
        throw "Badly formed inheritance";
    }
    return {
        end
    };
};

const filterSymbolsByLine = (sourceFile: SourceFile, begin: number, end: number) => (symbol: Symbol) => {
    return symbol.getDeclarations().some(d => {
        const line = d.getStartLineNumber();
        return line >= begin && line <= end && d.getSourceFile().getFilePath() === sourceFile.getFilePath();
    })
};

const filterReferences = (begin: number, end: number) => (symbol: ReferencedSymbol) => {
    const startLine = symbol.getDefinition().getNode().getStartLineNumber();
    return startLine >= begin && startLine <= end;
};

const filterNodes = (begin: number, end: number) => (symbol: Node) => {
    const startLine = symbol.getStartLineNumber();
    return startLine >= begin && startLine <= end;
};

const parseType = (type: Type<ts.Type>): string[] => {
    if (type.getProperties().length) {
        const properties = type.getProperties();
        return properties?.map(prop => prop.getName()).filter(type => !type.startsWith("__@")) || [];
    }
    return [];
};
const listPropBaseRegex = /\/\*\* \#\!list\-properties (\_[^ ]*) \*\//;
const listPropTypedRegex = /\/\*\* \#\!list\-properties (\_[^ ]*)( typed) \*\//;
const coerceListPropRegex = /\/\*\* \#\!coerce\-lets \*\//;

export const materializeUnit = (source: SourceFile) => {
    let cleanup = false;
    const comments = source.getDescendantsOfKind(ts.SyntaxKind.JSDocComment);
    for (let i = 0; i < comments.length; i++) {
        const comment = comments[i];
        if (cleanup) {
            return true;
        }
        if (!comment.wasForgotten() && comment.getText() === "/** #!ifinheritance */") {
            const inheritanceRoot = comment.getParent()?.getParent()!;
            let limits = getLimits(inheritanceRoot);
            inheritanceRoot?.getDescendantsOfKind(ts.SyntaxKind.JSDocComment).forEach(descendant => {
                if (!descendant.wasForgotten() && descendant.getStartLineNumber() <= limits.end) {
                    if (listPropBaseRegex.test(descendant.getText())) {
                        materializer(
                            descendant,
                            descendant.getParent()!,
                            false,
                            source,
                            comment.getStartLineNumber(),
                            limits.end,
                        );
                        cleanup = true;
                        limits = getLimits(inheritanceRoot);
                    } else if (coerceListPropRegex.test(descendant.getText())) {
                        coercer(
                            descendant,
                            descendant.getParent()!,
                            source,
                            comment.getStartLineNumber(),
                            limits.end,
                        );
                        cleanup = true;
                    } else if (listPropTypedRegex.test(descendant.getText())) {
                        materializer(
                            descendant,
                            descendant.getParent()!,
                            true,
                            source,
                            comment.getStartLineNumber(),
                            limits.end,
                        );
                        comment.remove();
                        cleanup = true;
                    }
                }
            })
        }
    }
    return cleanup;
};

export const materializer = (
    comment: JSDoc,
    parent: Node<ts.Node>,
    isTyped: boolean,
    source: SourceFile,
    startLine: number,
    endLine: number
) => {
    const prop = parent.getChildAtIndex(1)!;
    const propName = prop.getText();
    const typedef = prop.getType();
    const type = parseType(typedef);
    if (type.length > 0) {
        parent.replaceWithText(type.map(
            property => {
                if (!isTyped) {
                    const defined = parent.getSymbolsInScope(ts.SymbolFlags.BlockScopedVariable)
                        .filter(filterSymbolsByLine(source.getSourceFile(), startLine, endLine))
                        .map(defined => defined.getName());
                    if (defined.includes(property)) {
                        return `${property} = ${propName}.${property};`;
                    }
                }
                return `let ${property}${isTyped ? `: undefined | typeof ${propName}.${property}` : ": any"} = ${propName}.${property};${isTyped ? ` ${property};` : ""}`;
            }
        ).join("\n"));
    } else {
        comment.remove();
    }
};

export const coercer = (comment: JSDoc, parent: Node<ts.Node>, source: Node<ts.Node>, startLine: number, endLine: number) => {
    const definedVars = parent
        .getSymbolsInScope(ts.SymbolFlags.BlockScopedVariable)
        .filter(filterSymbolsByLine(source.getSourceFile(), startLine, endLine));
    const coercion = Object.keys(definedVars.filter(defined => defined.getName().indexOf("_") !== 0).map(defined => {
        const varName = defined.getName();
        const usage = defined.getValueDeclaration();
        if (Node.isReferenceFindableNode(usage) && usage.getStartLineNumber() <= parent.getStartLineNumber()) {
            const findReferences = usage
                .findReferences()
                .filter(filterReferences(startLine, endLine));
            if (findReferences.length === 0) {
                return false;
            }
            const lastRef = findReferences[findReferences.length - 1];
            const findUsage = lastRef.getReferences()
                .map(ref => ref.getNode())
                .filter(filterNodes(startLine, endLine));
            if (findUsage.length === 0) {
                return false;
            }
            const lastUsage = findUsage.map(usage => {
                const lastUsageParentAccessors = usage
                    .getParent()
                    ?.getChildrenOfKind(ts.SyntaxKind.PropertyAccessExpression)
                    .filter(child => child.getStartLineNumber() <= parent.getStartLineNumber()) || [];
                const deriveTypeFrom = lastUsageParentAccessors.length > 0
                    ? lastUsageParentAccessors[lastUsageParentAccessors.length - 1]
                    : usage.getParent();
                const derivedText = deriveTypeFrom?.getText();
                if (!deriveTypeFrom
                    || derivedText?.indexOf("_") !== 0
                    || derivedText?.indexOf("_coerce") === 0) {
                    return false;
                }
                return deriveTypeFrom;
            }).filter(deriveTypeFrom => deriveTypeFrom !== false);
            if (lastUsage.length === 0) {
                return false;
            }
            return `_coerce<typeof ${(lastUsage[lastUsage.length - 1] as Node<ts.Node>).getText()}>(${varName})`;
        }
        return false;
    }).filter(result => result !== false).reduce((p: Record<string, boolean>, c) => {
        if (typeof c === "string") {
            p[c] = true;
        }
        return p;
    }, {})).join(" && ");
    const replace = coercion ? coercion + " ? " : "";
    if (parent.getChildren()[2]) {
        const maybeCall = parent.getChildren()[2];
        if (Node.isCallExpression(maybeCall)
            && maybeCall.getExpression().getText() === "_condMerge"
            && maybeCall.getArguments()[0]?.getText() === "{ }") {
                inheritanceParser(maybeCall, source, startLine, endLine);
            }
    }
    if (replace) {
        parent.replaceWithText(replace + parent.getText() + (coercion ? " : (() => { throw 'Placeholder throw'; })()" : ""));
    } else {
        comment.remove();
    }
}

export const inheritanceParser = (call: CallExpression<ts.CallExpression>, source: Node<ts.Node>, startLine: number, endLine: number) => {
    const definedVars = call
        .getSymbolsInScope(ts.SymbolFlags.BlockScopedVariable)
        .filter(filterSymbolsByLine(source.getSourceFile(), startLine, endLine));
    const props = Object.keys(
        definedVars
            .filter(defined => defined.getName().indexOf("_") !== 0)
            .reduce((p: Record<string, true>, c) => {
                p[c.getName()] = true;
                return p;
        }, {}));
    const inheritanceApi = ts.factory.createObjectLiteralExpression(
        props.map(prop => ts.factory.createPropertyAssignment(
            prop,
            ts.factory.createIdentifier(prop),
        )),
    );
    call.getArguments()[0].replaceWithText(printNode(inheritanceApi));
};

/**
 * Unit test only
 */
export const materializationBootstrap = () => {
    const source = path.resolve(path.join("test", "ts", "materializer", "materialization-source.ts"));
    const actual = path.resolve(path.join("test", "ts", "materializer", "materialization-actual.ts"));
    if (existsSync(actual)) {
        unlinkSync(actual);
    }
    copyFileSync(source, actual);
    const project = new Project({
        compilerOptions: {
            strict: true,
            sourceRoot: path.resolve(path.join("test", "ts", "materializer")),
        }
    });
    project.addSourceFileAtPath(actual);

    let cleanup = true;
    while (cleanup) {
        cleanup = false;
        if (materializeUnit(project.getSourceFile("materialization-actual.ts")!)) {
            cleanup = true;
        }
    }

    project.saveSync();

    const actualContents = readFileSync(actual, "utf-8");
    unlinkSync(actual);
    return {
        actual: actualContents,
    };
};