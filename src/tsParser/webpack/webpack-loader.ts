import { printNode } from "ts-morph";
import * as webpack from "webpack";
import { crawlerParser } from "../parsers/crawlerParser";
import { readFileSync } from "fs";
import path from "path";
import { scriptParser } from "../parsers/scriptParser";
import { processTs } from "../postprocessing/processTs";
import { clearDir, getTsFilePath, retrieveJSFile, saveFile } from "./fileManipulator";
import { injectDefaultImports } from "../util/injectDefaultImports";

let completed = false;

const jonsLoader = (jsOnly: boolean) => function(this: webpack.loader.LoaderContext, _: string, sourceMap?: string) {
    if (completed) {
        this.callback(
            null,
            "",
            sourceMap,
        ); // Placeholder
        return;
    }
    const asnc = this.async();

    // Cleanup
    clearDir(this.resourcePath);

    // Preprocess
    const mainFile = readFileSync(path.resolve(this.resourcePath), "utf-8");
    const mappedPaths = crawlerParser(path.resolve(this.resourcePath), mainFile);
    mappedPaths.forEach(mapped => {
        const mappedFile = readFileSync(path.resolve(mapped), "utf-8");
        // Transpile
        saveFile(
            this.resourcePath,
            mapped,
            injectDefaultImports(
                scriptParser(
                    mappedFile,
                    path.resolve(
                        path.dirname(this.resourcePath),
                    ),
                ),
            ).map(node => printNode(node)).join('\n'));
    });

    const tsPaths = mappedPaths.map(mapped => getTsFilePath(this.resourcePath, mapped));

    // Postprocess
    processTs(path.join(path.resolve(this.rootContext), "src", "jons-temp"), tsPaths.map(ts => path.basename(ts).replace(/\.ts$/g, "")), jsOnly);

    if (jsOnly) {
        // Collect processed files
        const jsCollect = tsPaths.map(retrieveJSFile).join("\n");

        // Emit
        if (asnc) {
            asnc(
                null,
                jsCollect,
                sourceMap,
            );
        } else {
            this.callback(
                null,
                jsCollect,
                sourceMap,
            );
        }

        // Delete files used during postprocess
        clearDir(this.resourcePath);
    } else {
        this.callback(
            null,
            "",
            sourceMap,
        ); // Placeholder, will not emit any JS files
    }

    // Stop build from repeating
    completed = true;
};

export default jonsLoader;