import path from "path";
import * as fs from "fs";

export const clearDir = (resourcePath: string) => {
    const getResourceDir = path.join(path.dirname(path.resolve(resourcePath)), "jons-temp");
    if (fs.existsSync(getResourceDir)) {
        fs.rmdirSync(getResourceDir, { recursive: true });
    }
};

export const retrieveJSFile = (tsPath: string) => {
    const jsPath = tsPath.replace(/\.ts$/, ".js");
    const contents = fs.readFileSync(jsPath, "utf-8");
    fs.unlinkSync(jsPath);
    return contents;
};

export const getTsFilePath = (resourcePath: string, currentPath: string) => {
    const resolvedDir = path.dirname(path.resolve(resourcePath));
    const getResourceDir = path.join(resolvedDir, "jons-temp");
    return path.join(getResourceDir, currentPath.replace(resolvedDir, "")).replace(/\.jons$/, "") + ".ts";
};

export const saveFile = (resourcePath: string, currentPath: string, currentFile: string) => {
    const resolvedDir = path.dirname(path.resolve(resourcePath));
    const getResourceDir = path.join(resolvedDir, "jons-temp");
    if (!fs.existsSync(getResourceDir)) {
        fs.mkdirSync(getResourceDir);
    }
    const getCurrentPath = path.join(getResourceDir, currentPath.replace(resolvedDir, "")).replace(/\.jons$/, "") + ".ts";
    fs.writeFileSync(getCurrentPath, currentFile);
};