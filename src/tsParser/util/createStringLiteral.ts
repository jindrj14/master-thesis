import { ts } from "ts-morph";

export const unescapedStringLiteral = (factory: typeof ts.factory, value: string) => factory.createStringLiteral(value.replace(/\\/g, ""));