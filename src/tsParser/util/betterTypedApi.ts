import { ts } from 'ts-morph';

// equalsGreaterThanToken: EqualsGreaterThanToken | undefined, body: ConciseBody): ArrowFunction;

export const createArrowFunction = (factory: typeof ts.factory, params: {
    modifiers?: ts.Modifier[];
    equalsGreaterThanToken?: ts.EqualsGreaterThanToken;
    name?: string | ts.Identifier;
    typeParameters?: ts.TypeParameterDeclaration[];
    parameters: ts.ParameterDeclaration[];
    type?: ts.TypeNode;
    body: ts.ConciseBody;
}) => factory.createArrowFunction(
    params.modifiers,
    params.typeParameters,
    params.parameters,
    params.type,
    params.equalsGreaterThanToken,
    params.body,
);

export const createTypeParameterDeclaration = (factory: typeof ts.factory, params: {
    name: string | ts.Identifier,
    constraint?: ts.TypeNode;
    defaultType?: ts.TypeNode;
}) => factory.createTypeParameterDeclaration(
    params.name,
    params.constraint,
    params.defaultType,
);

export const createParameterDeclaration = (factory: typeof ts.factory, params: {
    decorators?: ts.Decorator[];
    modifiers?: ts.Modifier[];
    dotDotDotToken?: ts.DotDotDotToken;
    name: string | ts.Identifier;
    questionToken?: ts.QuestionToken;
    type?: ts.TypeNode;
    initializer?: ts.Expression;
}) => factory.createParameterDeclaration(
    params.decorators,
    params.modifiers,
    params.dotDotDotToken,
    params.name,
    params.questionToken,
    params.type,
    params.initializer,
);
