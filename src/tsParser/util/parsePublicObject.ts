import { ts } from "ts-morph";
import * as p from "../../grammar/jonscriptParser";
import { parseAssign } from "./parseAssign";
import { parseFunctor } from "./parseFunctor";

export const parsePublicObject = (
    factory: typeof ts.factory,
    publicObject: p.PublicObjectContext,
    hasInheritanceApi: boolean,
): ts.Expression => {
    const publicObjectExpr = factory.createCallExpression(
        factory.createIdentifier("_silentSpread"),
        undefined,
        [
            factory.createNewExpression(
                factory.createClassExpression(
                    undefined,
                    undefined,
                    "_publicApi",
                    undefined,
                    undefined,
                    publicObject.varAssignment().map(
                        assignment => factory.createIdentifier(`${assignment.VAR()!.text} = ${assignment.VAR()!.text};`) as any,
                    ).concat(publicObject.singleVarAssignment()
                        ? [factory.createIdentifier(`${publicObject.singleVarAssignment()!.VAR()!.text} = ${publicObject.singleVarAssignment()!.VAR()!.text};`) as any]
                        : [],
                    ),
                ),
                undefined,
                undefined,
            )
        ],
    );
    const regularStatements = [
        ...publicObject.varAssignment().reduce((p: ts.Statement[], assign) => {
            p.push(...parseAssign(factory, assign, []));
            return p;
        }, publicObject.singleVarAssignment() ? parseAssign(factory, publicObject.singleVarAssignment()!, []) : []),
        factory.createReturnStatement(
            publicObject.functor() 
                ? factory.createCallExpression(
                    factory.createIdentifier("_enhance"),
                    undefined,
                    [
                        parseFunctor(factory, publicObject.functor()!),
                        publicObjectExpr,
                    ]
                )
                : publicObjectExpr,
        ),
    ];
    if (hasInheritanceApi) {
        return factory.createImmediatelyInvokedArrowFunction(
            [
                factory.createIdentifier("\n/** #!list-properties _inheritanceApi typed */  _inheritanceApi;\n") as any,
                factory.createIdentifier("/** #!endifinheritance */ _inheritanceApi;\n") as any,
                factory.createReturnStatement(
                    factory.createImmediatelyInvokedArrowFunction(regularStatements)
                ),
            ]
        );
    }
    return factory.createImmediatelyInvokedArrowFunction(regularStatements);
};