import path from "path";
import glob from "glob";

export const manageImport = (dirname: string, importPath: string): string => {
    if (importPath[0] === '.') {
        const found = glob.sync(path.join(dirname, importPath + "*"));
        if (found.length > 0 && !found.some(f => path.parse(f).ext === ".jons")) {
            return importPath.startsWith("..") ? "../" + importPath : "." + importPath;
        }    
    }
    return importPath;
};