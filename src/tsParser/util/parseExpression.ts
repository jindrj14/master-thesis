import { ts } from "ts-morph";
import * as p from "../../grammar/jonscriptParser";
import { createArrowFunction } from "./betterTypedApi";
import { unescapedStringLiteral } from "./createStringLiteral";
import { parseObject } from "./parseClassBody";
import { parseExpressionList } from "./parseExpressionList";
import { parseFunctor } from "./parseFunctor";
import { parseInterpolated } from "./parseInterpolated";
import { parseType } from "./parseType";

const expressionMap: { [key: string]: string } = {
    '+': 'plus',
    '-': 'minus',
    '*': 'multiply',
    '/': 'divide',
    '%': 'modulo',
    '==': 'equals',
    '!=': 'nequals',
    '>': 'greater',
    '<': 'less',
    '>=': 'greaterOrEquals',
    '<=': 'lessOrEquals',
};

export const expressionPropertyAccessName = (factory: typeof ts.factory, expression: p.ExpressionContext) => {
    if (expression.expression().length > 1) {
        return parseExpression(factory, expression.expression()[1]);
    }
    // THROW | IMPORT | AS | IS | TRY | CATCH | FINALLY | EXTENDS | FROM
    return factory.createIdentifier(
            (expression.THROW()?.text
                || expression.IMPORT()?.text
                || expression.AS()?.text
                || expression.IS()?.text
                || expression.TRY()?.text
                || expression.CATCH()?.text
                || expression.FINALLY()?.text
                || expression.EXTENDS()?.text
                || expression.FROM()?.text) ?? (() => { throw "Invalid expression detected"; })()
        );
};

const getOperatorFromBinaryExpr = (expression: p.ExpressionContext) => {
    return (expression.MULTIPLYDIVIDEMOD_OPERATORS()
        || expression.PLUSMINUS_OPERATORS()
        || expression.EQUALITY()
        || expression.NEQUALITY()
        || expression.EQUAL_OR_GREATER()
        || expression.EQUAL_OR_LESS()
        || expression.LESS_THAN()
        || expression.MORE_THAN()
        || expression.LOGICAL_OPERATORS())?.text;
}

const removeStrBorder = /\"(.*)\"/;

export const parseExpression = (factory: typeof ts.factory, expression: p.ExpressionContext): ts.Expression => {
    const isAtomic = Boolean(expression.ATOMIC());
    const isVar = Boolean(expression.VAR());
    const isFunctor = Boolean(expression.functor());
    const isOperation = Boolean(getOperatorFromBinaryExpr(expression));
    const isObjectOp = Boolean(expression.OBJECT_OPERATORS());
    const isEval = Boolean(expression.eval());
    const isFnCall = Boolean(expression.EVAL_BEGIN());
    const isArray = Boolean(expression.LIST_BEGIN() && expression.expressionList());
    const isArrayAccess = Boolean(expression.LIST_BEGIN() && expression.expression().length > 0);
    const isTernary = Boolean(expression.QMARK());
    const isInterpolated = expression.INTERPOLATED_SEQUENCE().length > 0;
    const isPrefixOperator = Boolean(expression.PREFIX_UNARY_OPERATORS());
    const isIsOperator = Boolean(expression.IS());
    const isThrow = Boolean(expression.THROW());
    const isTryCatch = Boolean(expression.TRY());
    const isObject = Boolean(expression.object());
    if (isObject) {
        return factory.createImmediatelyInvokedArrowFunction(
            parseObject(factory, [], expression.object()!),
        );
    }
    if (isThrow) {
        return factory.createCallExpression(
            factory.createIdentifier("_throw"),
            undefined,
            [parseExpression(factory, expression.expression()[0])],
        );
    }
    if (isTryCatch) {
        return factory.createCallExpression(
            factory.createIdentifier("_try"),
            undefined,
            [
                createArrowFunction(factory, {
                    parameters: [],
                    body: parseExpression(factory, expression.expression()[0]),
                }),
                ...expression.expression().slice(1).map(expr => parseExpression(factory, expr)),
            ]
        );
    }
    if (expression.INTERPOLATED_SEQUENCE_EMPTY()) {
        return factory.createStringLiteral("");
    }
    if (expression.INTERPOLATED_SEQUENCE_STRING()) {
        return unescapedStringLiteral(factory, expression.INTERPOLATED_SEQUENCE_STRING()!.text.replace(/(^\`)|(\`$)/g, ""));
    }
    if (isAtomic) {
        if (expression.ATOMIC()!.text === "nil") {
            return factory.createIdentifier("undefined");
        }
        if (expression.ATOMIC()!.text.includes("\"")) {
            return unescapedStringLiteral(factory, removeStrBorder.exec(expression.ATOMIC()!.text)?.[1] || "");
        }
        if (!isNaN(parseFloat(expression.ATOMIC()!.text))) {
            return factory.createNumericLiteral(expression.ATOMIC()!.text);
        }
        if (expression.ATOMIC()!.text === "true") {
            return factory.createTrue();
        }
        if (expression.ATOMIC()!.text === "false") {
            return factory.createFalse();
        }
    }
    if (isIsOperator) {
        return factory.createCallExpression(
            factory.createIdentifier("_is"),
            undefined,
            expression.expression().map(expr => parseExpression(factory, expr)),
        );
    }
    if (isVar) {
        return factory.createIdentifier(expression.VAR()!.text);
    }
    if (isFunctor) {
        return parseFunctor(factory, expression.functor()!);
    }
    if (isOperation) {
        const operator = getOperatorFromBinaryExpr(expression);
        if (operator === "&&") {
            return factory.createLogicalAnd(
                parseExpression(factory, expression.expression()[0]),
                parseExpression(factory, expression.expression()[1]),
            );
        }
        if (operator === "||") {
            return factory.createLogicalOr(
                parseExpression(factory, expression.expression()[0]),
                parseExpression(factory, expression.expression()[1]),
            );
        }
        const functionName = expressionMap[operator!];
        return factory.createCallExpression(
            factory.createPropertyAccessExpression(
                factory.createIdentifier("_o"),
                factory.createIdentifier(functionName),
            ),
            undefined,
            expression.expression().map(expr => parseExpression(factory, expr)),
        )
    }
    if (isObjectOp) {
        return factory.createPropertyAccessChain(
            parseExpression(factory, expression.expression()[0]),
            factory.createToken(ts.SyntaxKind.QuestionDotToken),
            expressionPropertyAccessName(factory, expression) as any,
        );
    }
    if (isEval) {
        return factory.createParenthesizedExpression(
            parseExpression(factory, expression.eval()!.expression()),
        );
    }
    if (isFnCall) {
        return factory.createCallChain(
            parseExpression(factory, expression.expression()[0]),
            factory.createToken(ts.SyntaxKind.QuestionDotToken),
            expression.expression()[0]?.templateType?.() ? expression.expression()[0].templateType()!.type().map(tmplt => parseType(factory, tmplt)) : undefined,
            expression.expressionList() && parseExpressionList(factory, expression.expressionList()!),
        );
    }
    if (isArray) {
        return factory.createArrayLiteralExpression(
            expression.expressionList() && parseExpressionList(factory, expression.expressionList()!),
        );
    }
    if (isArrayAccess) {
        return factory.createElementAccessChain(
            parseExpression(factory, expression.expression()[0]),
            factory.createToken(ts.SyntaxKind.QuestionDotToken),
            parseExpression(factory, expression.expression()[1]),
        );
    }
    if (isTernary) {
        return factory.createConditionalExpression(
            parseExpression(factory, expression.expression()[0]),
            factory.createToken(ts.SyntaxKind.QuestionToken),
            parseExpression(factory, expression.expression()[1]),
            factory.createToken(ts.SyntaxKind.ColonToken),
            parseExpression(factory, expression.expression()[2]),
        );
    }
    if (isInterpolated) {
        return parseInterpolated(factory, expression);
    }
    if (isPrefixOperator) {
        if (expression.PREFIX_UNARY_OPERATORS()!.text === "await") {
            return factory.createAwaitExpression(
                parseExpression(factory, expression.expression()[0]),
            );
        }
        if (expression.PREFIX_UNARY_OPERATORS()!.text === "!") {
            return factory.createLogicalNot(
                parseExpression(factory, expression.expression()[0]),
            );
        }
    }
    throw "Impossible expression detected!";
}