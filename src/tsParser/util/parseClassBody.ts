import { printNode, ts } from "ts-morph";
import * as p from "../../grammar/jonscriptParser";
import { parseAssign } from "./parseAssign";
import { parseExpression } from "./parseExpression";
import { parsePublicObject } from "./parsePublicObject";

const createCommentLine = (
    factory: typeof ts.factory,
    comment: string,
) => factory.createIdentifier(comment) as any;

export const parseObject = (
    factory: typeof ts.factory,
    paramNames: string[],
    classBody?: p.ObjectContext,
): ts.Statement[] => {
    if (!classBody) {
        return [];
    }
    const inheritanceApi = "_inheritanceApi";
    const acc: ts.Statement[] = [];

    // Parse private api!
    classBody.assignment().forEach(assignment => {
        acc.push(...parseAssign(factory, assignment, paramNames))
    });

    // Define inheritance object
    if (classBody.inheritance().length > 0) {
        acc.push(createCommentLine(factory, "\n/** #!ifinheritance */\n"));
    }
    classBody.inheritance().forEach((inherit, i) => {
        acc.push(createCommentLine(factory, `const _${i} = /** #!coerce-lets */ (${printNode(parseExpression(factory, inherit.expression()))});\n`));
        acc.push(createCommentLine(factory, `/** #!list-properties _${i} */ _${i};\n`));
    });
    if (classBody.inheritance().length > 0) {
        acc.push(createCommentLine(factory, `const _inheritanceApi = /** #!coerce-lets */ (_condMerge({ }, _${classBody.inheritance().length - 1}));\n`));
    }

    // Parse public api
    let definedApi = false;
    if (classBody.publicApi()?.expression()) {
        const inherits = classBody.inheritance().length > 0 ? [
            createCommentLine(factory, "\n/** #!list-properties _inheritanceApi typed */ _inheritanceApi;\n"),
            createCommentLine(factory, "/** #!endifinheritance */ _inheritanceApi;\n"),
        ] : [];
        acc.push(factory.createVariableStatement(
            undefined,
            [
                factory.createVariableDeclaration(
                    "_publicApi",
                    undefined,
                    undefined,
                    factory.createImmediatelyInvokedArrowFunction([
                        ...inherits,
                        factory.createReturnStatement(
                            factory.createCallExpression(
                                factory.createIdentifier("_safeCopy"),
                                undefined,
                                [parseExpression(factory, classBody.publicApi()!.expression()!)],
                            ),
                        ),
                    ]),
                ),
            ],
        ));
        definedApi = true;
    }
    if (classBody.publicApi()?.publicObject()) {
        acc.push(factory.createVariableStatement(
            undefined,
            [
                factory.createVariableDeclaration(
                    "_publicApi",
                    undefined,
                    undefined,
                    parsePublicObject(
                        factory,
                        classBody.publicApi()!.publicObject()!,
                        classBody.inheritance().length > 0,
                    ),
                ),
            ],
        ));
        definedApi = true;
    }

    if (definedApi) {
        if (classBody.inheritance().length > 0) {
            acc.push(factory.createReturnStatement(
                factory.createCallExpression(
                    factory.createIdentifier("_sanitizeObject"),
                    undefined,
                    [
                        factory.createCallExpression(
                            factory.createIdentifier("_enhance"),
                            undefined,
                            [
                                factory.createIdentifier("_publicApi"),
                                factory.createIdentifier(inheritanceApi),
                            ]
                        ),
                    ],
                ),
            ));
        } else {
            acc.push(factory.createReturnStatement(
                factory.createCallExpression(
                    factory.createIdentifier("_sanitizeObject"),
                    undefined,
                    [
                        factory.createIdentifier("_publicApi"),
                    ],
                )
            ));
        }
    } else if (classBody.inheritance().length === 0) {
        acc.push(factory.createReturnStatement(factory.createObjectLiteralExpression()));
    } else {
        acc.push(...[
            createCommentLine(factory, "/** #!endifinheritance */ _inheritanceApi;\n"),
            factory.createReturnStatement(factory.createIdentifier(inheritanceApi))
        ]);
    }

    return acc;
}