import { ts } from "ts-morph";
import * as p from "../../grammar/jonscriptParser";
import { createArrowFunction, createParameterDeclaration } from "./betterTypedApi";
import { parseObject } from "./parseClassBody";
import { parseExpression } from "./parseExpression";
import { parseIgnorableVars } from "./parseIgnorableVars";
import { parseTemplate } from "./parseTemplate";
import { parseType } from "./parseType";

export const parseFunctor = (factory: typeof ts.factory, functor: p.FunctorContext): ts.ArrowFunction => {
    return createArrowFunction(
        factory,
        {
            parameters: functor.EVAL_BEGIN() ?
                parseIgnorableVars(factory, functor.ignoreableVar(), Boolean(functor.INHERITS()))
                : [createParameterDeclaration(factory, { name: functor.VAR()!.text })],
            typeParameters: functor.template() ? parseTemplate(ts.factory, functor.template()!) : undefined,
            body: functor.expression()
                ? parseExpression(factory, functor.expression()!)
                : factory.createBlock(
                    parseObject(
                        factory,
                        functor.ignoreableVar().filter(ignorable => ignorable.VAR()).map(ignorable => ignorable.VAR()!.text),
                        functor.object(),
                    )
                ),
            type: functor.type() ? parseType(factory, functor.type()!, false) : undefined,
        },
    )
};