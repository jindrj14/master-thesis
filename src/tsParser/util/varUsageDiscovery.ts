import * as p from "../../grammar/jonscriptParser";

export const varUsageDiscovery = (expression: p.ExpressionContext, variableName: string): boolean => {
    if (expression.VAR?.()?.text === variableName) {
        return true;
    }
    return expression.expression().some(
        (child: p.ExpressionContext) => !expression.OBJECT_OPERATORS() && varUsageDiscovery(child, variableName)
    ) || false;
};