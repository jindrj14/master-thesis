import { ts } from "ts-morph";
import * as p from "../../grammar/jonscriptParser";
import { unescapedStringLiteral } from "./createStringLiteral";
import { parseExpression } from "./parseExpression";

const testStart = /^`([^]*)\$\{$/;
const testAtomic = /^`([^]*)`$/;
const testMiddle = /^\}([^]*)\$\{$/;
const testEnd = /^\}([^]*)`$/;

const concatStringExpr = (factory: typeof ts.factory, ...expressions: ts.Expression[]): ts.Expression => {
    return expressions.length > 1
        ? factory.createCallExpression(
            factory.createPropertyAccessExpression(
                factory.createIdentifier("_o"),
                factory.createIdentifier("plus")
            ),
            undefined,
            [
                expressions[0],
                concatStringExpr(
                    factory,
                    ...expressions.slice(1),
                )
            ]
        ) : (expressions.length === 1 ? expressions[0] : factory.createStringLiteral(""));
};

const createSimpleString = (factory: typeof ts.factory, interpolated: string, regex: RegExp): ts.StringLiteral => {
    const contents = regex.exec(interpolated)?.[1];
    return unescapedStringLiteral(factory, contents || "");
};

export const parseInterpolated = (factory: typeof ts.factory, expression: p.ExpressionContext): ts.Expression => {
    const start = expression.INTERPOLATED_SEQUENCE()[0].text;
    const end = expression.INTERPOLATED_SEQUENCE()[1].text;
    if (testAtomic.test(start + end)) {
        if (!testStart.test(start) && !testEnd.test(end)) {
            if (!expression.interpolated()) {
                return concatStringExpr(
                    factory,
                    createSimpleString(factory, start, testStart), 
                    createSimpleString(factory, start, testStart),
                );
            }   
        }
        if (testStart.test(start) && testEnd.test(end) && expression.interpolated()) {
            return concatStringExpr(
                factory,
                createSimpleString(factory, start, testStart),
                parseInterpolatedInside(factory, expression.interpolated()!),
                createSimpleString(factory, end, testEnd),
            );
        }
    }
    throw "Invalid interpolated sequence!";
};

const parseInterpolatedInside = (
    factory: typeof ts.factory,
    interpolated: p.InterpolatedContext,
    expect: "middle" | "expression" = "expression",
): ts.Expression => {
    if (expect === "expression") {
        if (interpolated.expression()) {
            return concatStringExpr(
                factory,
                parseExpression(factory, interpolated.expression()!),
                ...(interpolated.interpolated() ? [parseInterpolatedInside(factory, interpolated.interpolated()!, "middle")] : [])
            )
        }
    } else if (interpolated.INTERPOLATED_SEQUENCE() && testMiddle.test(interpolated.INTERPOLATED_SEQUENCE()?.text!)) {
        return concatStringExpr(
            factory,
            createSimpleString(factory, interpolated.INTERPOLATED_SEQUENCE()!.text!, testMiddle),
            ...(interpolated.interpolated() ? [parseInterpolatedInside(factory, interpolated.interpolated()!, "expression")] : []),
        );
    }
    throw "Invalid interpolated sequence!";
};