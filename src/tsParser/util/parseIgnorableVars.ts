import { ts } from "ts-morph";
import * as p from "../../grammar/jonscriptParser";
import { createParameterDeclaration } from "./betterTypedApi";
import { parseExpression } from "./parseExpression";
import { parseType } from "./parseType";

export const parseIgnorableVars = (factory: typeof ts.factory, ignorables: p.IgnoreableVarContext[], hasSpread: boolean) => {
    return ignorables.map((variable, i) => createParameterDeclaration(
        factory,
        {
            name: variable.VAR()?.text || ('_' + i),
            type: variable.type() ? parseType(ts.factory, variable.type()!) : undefined,
            questionToken: variable.QMARK() ? factory.createToken(ts.SyntaxKind.QuestionToken) : undefined,
            dotDotDotToken: ((i === ignorables.length - 1) && hasSpread) ? factory.createToken(ts.SyntaxKind.DotDotDotToken) : undefined,
            initializer: variable.EQUALS() ? parseExpression(factory, variable.expression()!) : undefined,
        }
    ))
};