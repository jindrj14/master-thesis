import { printNode, ts } from "ts-morph";
import * as p from "../../grammar/jonscriptParser";
import { createParameterDeclaration } from "./betterTypedApi";
import { unescapedStringLiteral } from "./createStringLiteral";
import { parseTemplate } from "./parseTemplate";

const parseTypeAccess = (factory: typeof ts.factory, access: p.AccessTypeContext): string => access.VAR().length === 2
            ? [
                printNode(factory.createTypeReferenceNode(access.VAR()[0].text)),
                printNode(factory.createTypeReferenceNode(access.VAR()[1].text)),
            ].join(".")
            : [
                printNode(factory.createTypeReferenceNode(access.VAR()[0].text)),
                parseTypeAccess(factory, access.accessType()!)
            ].join(".");

export const parseType = (
    factory: typeof ts.factory,
    typeContext: p.TypeContext,
    isReturnType?: boolean,
): ts.TypeNode => {
    const isAtomic = Boolean(typeContext.ATOMIC());
    if (isReturnType) {
        if (isAtomic && typeContext.ATOMIC()!.text === "nil") {
            return factory.createTypeReferenceNode("void");
        }
        return factory.createUnionTypeNode([
            factory.createTypeReferenceNode("undefined"),
            parseType(factory, typeContext),
        ]);
    }
    const isGeneric = Boolean(typeContext.templateType());
    const isEval = Boolean(typeContext.typeEval());
    const isTuple = Boolean(typeContext.tuple());
    const isArray = Boolean(typeContext.LIST_BEGIN());
    const isOperation = Boolean(typeContext.TYPE_OPERATORS());
    const isFunctor = Boolean(typeContext.functionType());
    const isTypeAccess = Boolean(typeContext.accessType());
    const isObject = Boolean(typeContext.objectType());
    const simpleCallParse = (typeContext: p.TypeContext) => parseType(factory, typeContext);
    if (isObject) {
        return factory.createTypeReferenceNode(`{${
            typeContext.objectType()!.objectParamType().map(param => `${
                param.VAR().text + (param.QMARK() ? "?: " : ": ") + printNode(simpleCallParse(param.type()))
            }`).join(";")
        }}`);
    }
    if (isTypeAccess) {
        return factory.createTypeReferenceNode(parseTypeAccess(factory, typeContext.accessType()!));
    }
    if (isEval) {
        return factory.createParenthesizedType(
            parseType(factory, typeContext.typeEval()!.type())
        );
    }
    if (isOperation) {
        if (typeContext.TYPE_OPERATORS()!.text === "&") {
            return factory.createIntersectionTypeNode(
                typeContext.type().map(simpleCallParse),
            );
        } else {
            return factory.createUnionTypeNode(
                typeContext.type().map(simpleCallParse),
            );
        }
    }
    if (isAtomic) {
        const atomicValue = typeContext.ATOMIC()!.text;
        if (atomicValue === "nil")  {
            return factory.createTypeReferenceNode(
                factory.createIdentifier("undefined"),
            );
        } else if (atomicValue === "true" || atomicValue === "false") {
            return factory.createTypeReferenceNode(
                factory.createIdentifier(atomicValue)
            );
        } else if (/^"(.*)"$/.test(atomicValue)) {
            return factory.createLiteralTypeNode(
                unescapedStringLiteral(factory, atomicValue.replace(/(^")|("$)/g, ""))
            );
        } else if (!isNaN(parseFloat(atomicValue))) {
            return factory.createTypeLiteralNode(
                factory.createNumericLiteral(atomicValue) as any
            );
        }
        throw `${atomicValue} in type is an unknown type of literal!`;
    }
    if (isGeneric) {
        const template = typeContext.VAR()
            ? typeContext.VAR()!.text
            : parseTypeAccess(factory, typeContext.accessType()!);
        if (template === "Class") {
            const textType = printNode(parseType(factory, typeContext.templateType()!.type()[0]));
            return factory.createTypeReferenceNode("ReturnType", [
                factory.createTypeReferenceNode(`typeof ${textType}`)
            ]);
        }
        if (template === "Object") {
            const textType = printNode(parseType(factory, typeContext.templateType()!.type()[0]));
            return factory.createTypeReferenceNode(`typeof ${textType}`);
        }
        return factory.createTypeReferenceNode(
            template,
            typeContext.templateType()!.type().map(simpleCallParse),
        );
    }
    if (isTuple) {
        return factory.createTupleTypeNode(
            typeContext.tuple()!.type().map(simpleCallParse),
        );
    }
    if (isArray) {
        return factory.createArrayTypeNode(
            parseType(factory, typeContext.type()[0]),
        );
    }
    if (isFunctor) {
        return factory.createFunctionTypeNode(
            typeContext.functionType()!.template()
                ? parseTemplate(factory, typeContext.functionType()!.template()!)
                : undefined,
            typeContext.functionType()!.paramType().map(param => createParameterDeclaration(factory, {
                name: param.VAR().text,
                questionToken: param.QMARK() ? factory.createToken(ts.SyntaxKind.QuestionToken) : undefined,
                type: parseType(factory, param.type()),
            })),
            parseType(factory, typeContext.functionType()!.type(), true),
        )
    }

    const referenceType = typeContext.VAR()!.text;
    return factory.createTypeReferenceNode(referenceType);
} 