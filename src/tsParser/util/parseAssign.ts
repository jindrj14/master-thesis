import { ts } from "ts-morph";
import * as p from "../../grammar/jonscriptParser";
import { parseObject } from "./parseClassBody";
import { parseExpression } from "./parseExpression";
import { varUsageDiscovery } from "./varUsageDiscovery";

type Assignment = p.AssignmentContext | p.VarAssignmentContext | p.SingleVarAssignmentContext;

const isIgnorable = (assign: Assignment): assign is p.AssignmentContext => Object.getPrototypeOf(assign)?.constructor?.name === "AssignmentContext";

export const parseAssign = (factory: typeof ts.factory, assign: Assignment, definedProperties: string[]): ts.Statement[] => {
    if (isIgnorable(assign) && assign.IGNORE()) {
        if (assign.object()) {
            throw "Assigning object to ignorable property is pointless";
        }
        return [factory.createExpressionStatement(
            parseExpression(factory, assign.expression()!),
        )];
    }
    if (assign.object()) {
        if (definedProperties.includes(assign.VAR()!.text)) {
            return [factory.createExpressionStatement(
                factory.createAssignment(
                    factory.createIdentifier(assign.VAR()!.text),
                    factory.createImmediatelyInvokedArrowFunction(
                        parseObject(
                            factory,
                            [],
                            assign.object(),
                        ),
                    ),
                ),
            )];
        }
        return [factory.createVariableStatement(
            undefined,
            [
                factory.createVariableDeclaration(
                    assign.VAR()!.text,
                    undefined,
                    undefined,
                    factory.createImmediatelyInvokedArrowFunction(
                        parseObject(
                            factory,
                            [],
                            assign.object(),
                        ),
                    ),
                ),
            ],
        )];
    } else if (assign.expression()) {
        if (definedProperties.includes(assign.VAR()!.text)) {
            return [factory.createExpressionStatement(
                factory.createAssignment(
                    factory.createIdentifier(assign.VAR()!.text),
                    factory.createCallExpression(
                        factory.createIdentifier("_sanitize"),
                        undefined,
                        [
                            parseExpression(factory, assign.expression()!),
                        ],
                    ),
                ),
            )];
        }
        if (varUsageDiscovery(assign.expression()!, assign.VAR()!.text)) {
            const randomStr = "_var" + Math.random();
            return [
                factory.createVariableStatement(
                    undefined,
                    [
                        factory.createVariableDeclaration(
                            randomStr,
                            undefined,
                            undefined,
                            factory.createCallExpression(
                                factory.createIdentifier("_sanitize"),
                                undefined,
                                [
                                    parseExpression(factory, assign.expression()!),
                                ],
                            ),
                        ),
                    ],
                ),
                factory.createVariableStatement(
                    undefined,
                    [
                        factory.createVariableDeclaration(
                            assign.VAR()!.text,
                            undefined,
                            undefined,
                            factory.createIdentifier(randomStr),
                        ),
                    ],
                ),
            ];
        }
        return [factory.createVariableStatement(
            undefined,
            [
                factory.createVariableDeclaration(
                    assign.VAR()!.text,
                    undefined,
                    undefined,
                    factory.createCallExpression(
                        factory.createIdentifier("_sanitize"),
                        undefined,
                        [
                            parseExpression(factory, assign.expression()!),
                        ],
                    ),
                ),
            ],
        )];
    } else {
        if (definedProperties.includes(assign.VAR()!.text)) {
            return [factory.createExpressionStatement(
                factory.createAssignment(
                    factory.createIdentifier(assign.VAR()!.text),
                    factory.createIdentifier(assign.VAR()!.text),
                ),
            )];
        }
        return [factory.createExpressionStatement(
            factory.createAssignment(
                factory.createIdentifier(assign.VAR()!.text),
                factory.createCallExpression(
                    factory.createIdentifier("_sanitize"),
                    undefined,
                    [
                        factory.createIdentifier(assign.VAR()!.text),
                    ],
                ),
            ),
        )];
    }
};