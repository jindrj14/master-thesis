import { ts } from "ts-morph";
import * as p from "../../grammar/jonscriptParser";
import { parseType } from "./parseType";

const parseSingleTemplateType = (factory: typeof ts.factory, template: p.TemplateContentsContext): ts.TypeParameterDeclaration => {
    return factory.createTypeParameterDeclaration(
        template.VAR().text,
        template.extendsType() ? parseType(factory, template.extendsType()!.type()) : undefined,
        template.equalsType() ? parseType(factory, template.equalsType()!.type()) : undefined,
    );
};

export const parseTemplate = (
    factory: typeof ts.factory,
    template: p.TemplateContext
): ts.TypeParameterDeclaration[] => template.templateContents().map(content => parseSingleTemplateType(factory, content));
