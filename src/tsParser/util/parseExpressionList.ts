import { ts } from "ts-morph";
import * as p from "../../grammar/jonscriptParser";
import { parseExpression } from "./parseExpression";

export const parseExpressionList = (factory: typeof ts.factory, list: p.ExpressionListContext): ts.Expression[] => {
    return list.expressionItem().map(item => {
        if (item.INHERITS()) {
            return factory.createSpreadElement(
                factory.createCallExpression(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("Object"),
                        factory.createIdentifier("values"),
                    ),
                    undefined,
                    [factory.createBinaryExpression(
                        factory.createCallExpression(
                            factory.createIdentifier("_sanitize"),
                            undefined,
                            [parseExpression(factory, item.expression()!)]
                        ),
                        ts.SyntaxKind.QuestionQuestionToken,
                        factory.createArrayLiteralExpression(),
                    )],
                )
            );
        } else {
            return factory.createCallExpression(
                factory.createIdentifier("_sanitize"),
                undefined,
                [parseExpression(factory, item.expression()!)]
            );
        }
    });
}