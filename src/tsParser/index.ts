import { printNode, ts } from "ts-morph";
import { scriptParser } from "./parsers/scriptParser";
import { injectDefaultImports } from "./util/injectDefaultImports";

export const parseJonScript = (
    jonscript: string,
    createImports: boolean = false,
) => {
    const parsed: ts.Statement[] = [];
    parsed.push(...scriptParser(jonscript));
    if (createImports) {
        injectDefaultImports(parsed);
    }
    return parsed.map(node => printNode(node)).join('\n');
};
