import { CharStreams, CommonTokenStream } from "antlr4ts";
import { ParseTreeWalker } from "antlr4ts/tree/ParseTreeWalker";
import { jonscriptLexer } from "../../grammar/jonscriptLexer";
import { jonscriptListener } from "../../grammar/jonscriptListener";
import * as p from "../../grammar/jonscriptParser";
import path from "path";
import { readFileSync, existsSync } from "fs";
import uniq from "lodash.uniq";

/**
 * Crawl through the imports of the main file and get deps
 */
export const crawlerParser = (filepath: string, jonscript: string): string[] => {
    const preParseInputStream = CharStreams.fromString(jonscript);
    const preParseLexer = new jonscriptLexer(preParseInputStream);
    const preParseTokenStream = new CommonTokenStream(preParseLexer);
    const preParser = new p.jonscriptParser(preParseTokenStream);

    const dependencies: string[] = [filepath];

    class jonscriptListenerImplCrawler implements jonscriptListener {
        enterImported(ctx: p.ImportedContext) {
            const importPath = ctx.ATOMIC().text;
            if (/\"(.*)\"/.test(importPath)) {
                const cleanPath = importPath.replace(/\"/g, "");
                if (cleanPath[0] === ".") {
                    const resolvedPath = path.resolve(path.dirname(filepath), cleanPath) + ".jons";
                    if (existsSync(resolvedPath)) {
                        dependencies.unshift(...crawlerParser(resolvedPath, readFileSync(resolvedPath, "utf-8")));
                    }
                }
            } else {
                throw `${path} contains invalid import: ${importPath}`;
            }
        }
    }

    // Create pre-parser listener -> crawl through .jons files and collect paths
    const preListener: jonscriptListener = new jonscriptListenerImplCrawler();
    ParseTreeWalker.DEFAULT.walk(preListener, preParser.parent());

    return uniq(dependencies);
};