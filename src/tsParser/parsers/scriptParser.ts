import { CharStreams, CommonTokenStream } from "antlr4ts";
import { ParseTreeWalker } from "antlr4ts/tree/ParseTreeWalker";
import { Throw } from "throw-expression";
import { ts } from "ts-morph";
import { parseIgnorableVars } from "../util/parseIgnorableVars";
import { jonscriptLexer } from "../../grammar/jonscriptLexer";
import { jonscriptListener } from "../../grammar/jonscriptListener";
import * as p from "../../grammar/jonscriptParser";
import { createArrowFunction } from "../util/betterTypedApi";
import { parseObject } from "../util/parseClassBody";
import { parseTemplate } from "../util/parseTemplate";
import { manageImport } from "../util/importPathUtil";
import path from "path";

export const scriptParser = (jonscript: string, projectPath: string = path.join(__dirname, "../../../test/ts/testproject/src")) => {
    const classes: [string, ts.ArrowFunction][] = [];

    const inputStream = CharStreams.fromString(jonscript);
    const lexer = new jonscriptLexer(inputStream);
    const tokenStream = new CommonTokenStream(lexer);
    const parser = new p.jonscriptParser(tokenStream);

    const root: ts.Statement[] = [];
    let moduleName: string = undefined as any;

    class jonscriptListenerImpl implements jonscriptListener {
        enterModule(ctx: p.ModuleContext) {
            moduleName = ctx.VAR().text;
        }
        enterImported(ctx: p.ImportedContext) {
            const importPath = ts.factory.createStringLiteral(/^\".*?\"$/.test(ctx.ATOMIC().text)
                ? manageImport(projectPath, ctx.ATOMIC().text.replace(/\"/g, ""))
                : Throw("Import must be text"));
            if (ctx.defaultImport()) {
                root.push(ts.factory.createImportDeclaration(
                    undefined,
                    undefined,
                    ts.factory.createImportClause(
                        false,
                        ts.factory.createIdentifier(ctx.defaultImport()!.VAR().text),
                        undefined,
                    ),
                    importPath,
                ));
            } else {
                root.push((ts.factory.createImportDeclaration(
                    undefined,
                    undefined,
                    ts.factory.createImportClause(
                        false,
                        undefined,
                        ts.factory.createNamedImports(
                            ctx.importVar().map(importVar => ts.factory.createImportSpecifier(
                                importVar.AS() ? ts.factory.createIdentifier(importVar.VAR()[0].text) : undefined,
                                ts.factory.createIdentifier(importVar.AS() ? importVar.VAR()[1].text : importVar.VAR()[0].text),
                            )),
                        ),
                    ),
                    importPath,
                )));
            }
        }
        enterClassdef(ctx: p.ClassdefContext) {
            classes.push([
                ctx.VAR().text,
                createArrowFunction(ts.factory, {
                    name: ctx.VAR().text,
                    typeParameters: ctx.template() ? parseTemplate(ts.factory, ctx.template()!) : undefined,
                    parameters: parseIgnorableVars(ts.factory, ctx.ignoreableVar(), Boolean(ctx.INHERITS())),
                    body: ts.factory.createBlock(
                        parseObject(
                            ts.factory,
                            ctx.ignoreableVar().filter(param => param.VAR()).map(param => param.VAR()!.text),
                            ctx.object(),
                        ),
                    ),
                }),
            ]);
        }
        exitModule() {
            root.push(...classes.map(([name, oneClass]) => ts.factory.createVariableStatement(
                undefined,
                [
                    ts.factory.createVariableDeclaration(
                        ts.factory.createIdentifier(name),
                        undefined,
                        undefined,
                        oneClass,
                    )
                ]
            )));
            root.push(ts.factory.createVariableStatement(
                [ts.factory.createToken(ts.SyntaxKind.ExportKeyword)],
                [
                    ts.factory.createVariableDeclaration(
                        moduleName,
                        undefined,
                        undefined,
                        ts.factory.createObjectLiteralExpression(
                            classes.map(([name]) => ts.factory.createPropertyAssignment(
                                name,
                                ts.factory.createIdentifier(name),
                            )),
                        )
                    )
                ]
            ))
        }
    }

    // Create the listener
    const listener: jonscriptListener = new jonscriptListenerImpl();

    // Module is topmost
    ParseTreeWalker.DEFAULT.walk(listener, parser.parent());
    return root;
};