import { CharStreams, CommonTokenStream } from "antlr4ts";
import { ParseTreeWalker } from "antlr4ts/tree/ParseTreeWalker"
import { Throw } from "throw-expression";
import { jonscriptLexer } from "../../grammar/jonscriptLexer";
import { jonscriptListener } from "../../grammar/jonscriptListener";
import * as p from "../../grammar/jonscriptParser";
import { init } from "./contextManager";
import { parseAssignments, parseExpression, parseIgnorableVars, parseInheritance, parseObject, parsePublicObject, replicateString } from "./jsParseUtils";

export const parseJonScript = (jonscript: string) => {
    // Create the lexer and parser
    let inputStream = CharStreams.fromString(jonscript);
    let lexer = new jonscriptLexer(inputStream);
    let tokenStream = new CommonTokenStream(lexer);
    let parser = new p.jonscriptParser(tokenStream);
    let parsed = "";
    let indent = 0;
    const parse = (value: string, indentation: number) => {
        parsed += replicateString(indentation, "   ") + value;
    };
    let context = init('');
    class jonscriptListenerImpl implements jonscriptListener {
        enterModule(ctx: p.ModuleContext) {
            parse(`const ${ctx.VAR().text} = {\n`, indent);
            indent++;
        }
        enterClassdef(ctx: p.ClassdefContext) {
            parse(`${ctx.VAR().text}: (${
                parseIgnorableVars(ctx.ignoreableVar())
            }) => {\n`, indent);
            indent++;
            context.addClassName(ctx);
            let nextContext = context.next();

            nextContext.parseIgnorables("", ctx.ignoreableVar());
            // First, lets put together private api
            parseAssignments(ctx.object()!.assignment(), nextContext, indent, parse);

            // Second, do inheritance
            parseInheritance(ctx.object()!.inheritance(), nextContext, indent, parse, "publicApi");

            nextContext = nextContext.next("publicApi");

            // Thirdly, build public api
            const bodyContext = ctx.object()!.publicApi();
            if (bodyContext) {
                if (!bodyContext.expression()?.functor() && !bodyContext.publicObject()) {
                    Throw("Cannot define existing public class api without functor or object! This should never happen");
                }
                if (bodyContext.expression()?.functor()) {
                    parse(`const functorApi = (${
                        parseIgnorableVars(bodyContext.expression()?.functor()?.ignoreableVar() || [])
                    }) => {\n`, indent);
                    nextContext.parseIgnorables("", bodyContext.expression()?.functor()?.ignoreableVar() || []);
                    indent += 1;
                    if (bodyContext.expression()?.functor()?.expression()) {
                        parse('return ' + parseExpression(bodyContext.expression()?.functor()?.expression()!, nextContext, indent) + ';\n', indent);
                    } else {
                        parse(parseObject(bodyContext.expression()?.functor()?.object()!, nextContext, indent, "publicFunctor"), indent - 3);
                        parse('return publicFunctor;\n', indent);
                    }
                    indent -= 1;
                    parse('};\n', indent);
                } else if (bodyContext.publicObject()) {
                    parsePublicObject(bodyContext.publicObject()!, nextContext, indent, parse, "publicApi");
                }
            }
            if (bodyContext?.expression()?.functor()) {
                parse('Object.keys(publicApi).forEach(key => functorApi[key] = publicApi[key]);\n', indent); // Add intel on public api onto functor api :)
                parse('return functorApi;\n', indent);
            } else {
                parse('return publicApi;\n', indent);
            }
            context.removeContext("publicApi");
        }
        exitClassdef() {
            indent--;
            parse('},\n', indent);
        }
        exitModule() {
            indent--;
            parse('};\n', indent);
        }
    }
    
    // Create the listener
    const listener: jonscriptListener = new jonscriptListenerImpl();

    // Module is topmost
    ParseTreeWalker.DEFAULT.walk(listener, parser.parent());
    return parsed;
};
