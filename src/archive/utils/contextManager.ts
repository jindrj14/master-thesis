import * as p from "../../grammar/jonscriptParser";

export interface IContextManager {
    /**
     * Key: variable name, Value: context
     */
    parseIgnorables: (context: string, ignorables: p.IgnoreableVarContext[]) => IContextManager;
    addClassName: (className: p.ClassdefContext) => IContextManager;
    getContext: (variable: string) => string;
    next: (nextContext?: string) => IContextManager;
    addPrivateVariable: (name: string) => IContextManager;
    destroyPrivateVariable: (name: string) => IContextManager;
    removeContext: (context: string) => IContextManager;
}

export const init = (defaultContext: string, prevVars?: Record<string, string>) => {
    const variables: Record<string, string> = prevVars || {};
    const contextManager: IContextManager = ({
        parseIgnorables: (context, ignorables) => {
            ignorables.forEach(ignorable => {
                if (ignorable.VAR()) {
                    variables[ignorable.VAR()!.text] = context;
                }
            });
            return contextManager;
        },
        addPrivateVariable: name => {
            variables[name] = "";
            return contextManager;
        },
        destroyPrivateVariable: name => {
            delete variables[name];
            return contextManager;
        },
        addClassName: className => {
            const module = className.parent as p.ModuleContext;
            variables[className.VAR().text] = module.VAR().text;
            return contextManager;
        },
        removeContext: context => {
            Object.entries(contextManager).forEach(
                ([key, value]) => {
                    if (context === value) {
                        delete variables[key];
                    }
                }
            );
            return contextManager;
        },
        getContext: variable => {
            const context = (variables[variable] === undefined ? defaultContext : variables[variable]);
            if (context) {
                return context + "?." + variable;
            }
            return variable;
        },
        next: nextContext => init(nextContext === undefined ? defaultContext : nextContext, { ...variables }),
    });
    return contextManager;
};