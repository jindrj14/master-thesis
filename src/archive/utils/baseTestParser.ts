import { CharStreams, CommonTokenStream } from "antlr4ts";
import { ParseTreeWalker } from "antlr4ts/tree/ParseTreeWalker"
import { jonscriptLexer } from "../../grammar/jonscriptLexer";
import { jonscriptListener } from "../../grammar/jonscriptListener";
import { AssignmentContext, ClassdefContext, ExpressionContext, FunctorContext, InheritanceContext, jonscriptParser, ModuleContext, ObjectContext, PublicApiContext, VarAssignmentContext } from "../../grammar/jonscriptParser";

export const parseJonScript = (jonscript: string) => {
    // Create the lexer and parser
    let inputStream = CharStreams.fromString(jonscript);
    let lexer = new jonscriptLexer(inputStream);
    let tokenStream = new CommonTokenStream(lexer);
    let parser = new jonscriptParser(tokenStream);
    let parsed = "";
    class jonscriptListenerImpl implements jonscriptListener {
        enterModule(ctx: ModuleContext) {
            parsed += "module " + ctx.VAR().text + "\n";
        }
        enterClassdef(ctx: ClassdefContext) {
            parsed += "class " + ctx.VAR().text + "\n";
        }
        enterAssignment(ctx: AssignmentContext) {
            parsed += (ctx.VAR()?.text || ctx.IGNORE()?.text || "") + ": ";
        }
        exitAssignment(_: AssignmentContext) {
            parsed += ",\n";
        }
        enterVarAssignment(ctx: VarAssignmentContext) {
            parsed += (ctx.VAR()?.text || "") + ": ";
        }
        exitVarAssignment(_: VarAssignmentContext) {
            parsed += ",\n";
        }
        enterPublicApi(_: PublicApiContext) {
            parsed += "return ";
        }
        enterFunctor(_: FunctorContext) {
            parsed += "functor\n";
        }
        enterObject(_: ObjectContext) {
            parsed += "object\n";
        }
        enterExpression(_: ExpressionContext) {
            parsed += "expr";
        }
        enterInheritance(_: InheritanceContext) {
            parsed += "inherits\n";
        }
    }
    
    // Create the listener
    const listener: jonscriptListener = new jonscriptListenerImpl();

    // Module is topmost
    ParseTreeWalker.DEFAULT.walk(listener, parser.parent());
    return parsed;
};
