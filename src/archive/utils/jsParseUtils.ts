import { Throw } from "throw-expression";
import * as p from "../../grammar/jonscriptParser";
import { IContextManager } from "./contextManager";

export const parseExpressionList = (list: p.ExpressionListContext | undefined, contextManager: IContextManager, indentation: number) => {
    if (!list) {
        return "";
    }
    return list.expressionItem().map(expr => {
        return parseExpression(expr.expression()!, contextManager, indentation);
    }).join(", ");
};

// '+'|'-'|'*'|'/'|'%'|'=='|'!='|'>'|'<'|'>='|'<='
const expressionMap: { [key: string]: string } = {
    '+': 'plus',
    '-': 'minus',
    '*': 'multiply',
    '/': 'divide',
    '%': 'modulo',
    '==': 'equals',
    '!=': 'nequals',
    '>': 'greater',
    '<': 'less',
    '>=': 'greaterOrEquals',
    '<=': 'lessOrEquals',
};

const getExprOperator = (expression: p.ExpressionContext) => {
    return (expression.PLUSMINUS_OPERATORS()
        || expression.MULTIPLYDIVIDEMOD_OPERATORS()
        || expression.EQUALITY()
        || expression.NEQUALITY()
        || expression.EQUAL_OR_GREATER()
        || expression.EQUAL_OR_LESS()
        || expression.LESS_THAN()
        || expression.MORE_THAN()
        || expression.LOGICAL_OPERATORS())?.text;
}

/**
 * ATOMIC - OK
 *   | VAR - OK
 *   | functor - OK
 *   | expression op=(PLUSMINUS_OPERATORS | MULTIPLYDIVIDEMOD_OPERATORS | OBJECT_OPERATORS) expression - OK
 *   | expression EVAL_BEGIN expressionList? EVAL_END - OK
 *   | expression LIST_BEGIN expression LIST_END - OK
 *   | EVAL_BEGIN expression EVAL_END - OK
 *   | LIST_BEGIN expressionList? LIST_END - OK
 *   | PREFIX_UNARY_OPERATORS expression - OK
 *   | expression TERNARY_THEN expression COLON expression - OK
 *   | INTERPOLATED_SEQUENCE (INTERPOLATED_SEQUENCE | expression)+; - OK
 */
export const parseExpression = (expression: p.ExpressionContext, contextManager: IContextManager, indentation: number, noContextVar: boolean = false): string => {
    const sanitized = (expr: string) => `_o.sanitize(${expr})`;
    if (expression.ATOMIC()) {
        if (expression.ATOMIC()?.text === "nil") {
            return "undefined";
        }
        return expression.ATOMIC()?.text || "";
    }
    if (expression.VAR()) {
        const variable = expression.VAR()?.text || "";
        const disallowed = ['_o', 'null', 'NaN'];
        if (disallowed.includes(variable)) {
            throw "Not allowed to use null, _o or NaN!";
        }
        if (noContextVar) {
            return variable;
        }
        return sanitized(contextManager.getContext(variable));
    }
    if (expression.functor()) {
        return parseFunctor(expression.functor()!, contextManager, indentation);
    }
    if (getExprOperator(expression)) {
        const numeric = getExprOperator(expression);
        if (numeric === "&&" || numeric === "||") {
            return sanitized(`${
                parseExpression(expression.expression()[0], contextManager, indentation)
            } ${numeric} ${
                parseExpression(expression.expression()[1], contextManager, indentation)
            }`);
        }
        // Overloadable expressions :)
        return `_o.${
            expressionMap[numeric!]
        }(${
            parseExpression(expression.expression()[0], contextManager, indentation)
        }, ${
            parseExpression(expression.expression()[1], contextManager, indentation)
        })`;
    }
    // Forgiving accessors :)
    if (expression.OBJECT_OPERATORS()) {
        return `${
            parseExpression(expression.expression()[0], contextManager, indentation, noContextVar === true)
        }?.${
            parseExpression(expression.expression()[1], contextManager, indentation, true)
        }`;
    }
    // Simple ()
    if (expression.eval()) {
        return `(${parseExpression(expression.eval()?.expression()!, contextManager, indentation)})`;
    }
    // Forgiving function call
    if (expression.EVAL_BEGIN() && expression.expression()) {
        return `_o.safeCall((..._args) => ${
            parseExpression(expression.expression()[0], contextManager, indentation)
        }(..._args))?.(${ // Tests whether there is an actuall function to be called
            parseExpressionList(expression.expressionList(), contextManager, indentation)
        })`;
    }
    // Forgiving object access
    if (expression.LIST_BEGIN() && expression.expression().length === 2) {
        return sanitized(`${
            parseExpression(expression.expression()[0], contextManager, indentation)
        }?.[${
            parseExpression(expression.expression()[1], contextManager, indentation)
        }]`);
    }
    // Array creation
    if (expression.LIST_BEGIN() && expression.expressionList()) {
        return `[${parseExpressionList(expression.expressionList(), contextManager, indentation)}]`;
    }
    if (expression.PREFIX_UNARY_OPERATORS()) {
        return `${
            expression.PREFIX_UNARY_OPERATORS()?.text
        }${sanitized(parseExpression(expression.expression()[0], contextManager, indentation))}`
    }
    // Ternary expression
    if (expression.QMARK()) {
        return sanitized(`${
            parseExpression(expression.expression()[0], contextManager, indentation)
        } ? ${
            parseExpression(expression.expression()[1], contextManager, indentation)
        } : ${
            parseExpression(expression.expression()[2], contextManager, indentation)
        }`);
    }
    // Interpolation
    if (expression.INTERPOLATED_SEQUENCE()) {
        return parseInterpolated(expression, contextManager, indentation);
    }
    return "";
};

const testStart = /^`.*\$\{$/;
const testAtomic = /^`.*`$/;
const testMiddle = /^`\}.*\$\{$/;
const testEnd = /^\}.*`$/;

export const parseInterpolated = (expression: p.ExpressionContext, contextManager: IContextManager, indentation: number) => {
    const start = expression.INTERPOLATED_SEQUENCE()[0].text;
    const end = expression.INTERPOLATED_SEQUENCE()[1].text;
    if (testAtomic.test(start + end)) {
        if (!testStart.test(start) && !testEnd.test(end)) {
            if (!expression.interpolated()) {
                return start + end;
            }   
        }
        if (testStart.test(start) && testEnd.test(end) && expression.interpolated()) {
            return start + parseInterpolatedInside(expression.interpolated()!, contextManager, indentation) + end;
        }
    }
    throw "Invalid interpolated sequence!";
};

export const parseInterpolatedInside = (
    interpolated: p.InterpolatedContext,
    contextManager: IContextManager,
    indentation: number,
    expect: "middle" | "expression" = "expression",
): string => {
    if (expect === "expression") {
        if (interpolated.expression()) {
            return parseExpression(interpolated.expression()!, contextManager, indentation) + (
                interpolated.interpolated() ? parseInterpolatedInside(interpolated.interpolated()!, contextManager, indentation, "middle") : ""
            );
        }
    } else if (interpolated.INTERPOLATED_SEQUENCE() && testMiddle.test(interpolated.INTERPOLATED_SEQUENCE()?.text!)) {
        return interpolated.INTERPOLATED_SEQUENCE()?.text! + (
            interpolated.interpolated() ? parseInterpolatedInside(interpolated.interpolated()!, contextManager, indentation, "expression") : ""
        );
    }
    throw "Invalid interpolated sequence!";
};

let functorCount = 0;

export const parseFunctor = (functor: p.FunctorContext, contextManager: IContextManager, indentation: number) => {
    functorCount += 1;
    return `(${(() => {
            contextManager.parseIgnorables("", functor.ignoreableVar());
            return parseIgnorableVars(functor.ignoreableVar());
        })()}) => { ${
        functor.expression()
            ? "return " + parseExpression(functor.expression()!, contextManager, indentation) + " }"
            : "\n" + parseObject(functor.object()!, contextManager, indentation, "functor" + functorCount) + `\n return ${"functor" + functorCount}; }`
    }`;
};

export const parseAssignments = (
    assignments: (p.AssignmentContext | p.VarAssignmentContext)[],
    contextManager: IContextManager,
    indentation: number,
    parse: (text: string, indentation: number) => void,
) => {
    assignments.forEach(assignment => {
        if (!assignment.expression() && !assignment.object()) {
            Throw("No assignment should be without expression / object!");
        }
        if ((assignment as p.AssignmentContext).IGNORE?.()) {
            if (assignment.object()) {
                Throw("There is no reason to assign object to _");
            }
            parse(parseExpression(assignment.expression()!, contextManager, indentation) + ";\n", indentation);
        } else {
            if (!assignment.VAR()) {
                Throw("No var found in assignment, this should never happen");
            }
            const variableName = assignment.VAR()?.text!;
            if (assignment.expression()) {
                parse(`const ${variableName} = ${parseExpression(assignment.expression()!, contextManager, indentation)};\n`, indentation);
            } else {
                parse(`${parseObject(assignment.object()!, contextManager, indentation, variableName)}`, indentation - 3);
            }
            contextManager.addPrivateVariable(variableName);
        }
    });
};

export const parseInheritance = (
    inheritance: p.InheritanceContext[],
    contextManager: IContextManager,
    indentation: number,
    parse: (text: string, indentation: number) => void,
    name: string,
) => {
    parse(`let ${name} = {\n`, indentation);
    indentation += 1;
    inheritance.forEach(part => {
        parse(`...${
            contextManager.getContext(part.expression()!.expression()![0].VAR()!.text)}(${parseExpressionList(part.expression().expressionList(), contextManager, indentation)
        }),\n`, indentation);
    })
    parse("};\n", indentation - 1);
}

export const parsePublicObject = (
    bodyContext: p.PublicObjectContext,
    contextManager: IContextManager,
    indentation: number,
    parse: (text: string, indentation: number) => void,
    name: string,
) => {
    parseAssignments(bodyContext.varAssignment(), contextManager, indentation, parse);
    bodyContext.varAssignment().forEach(part => {
        if (part.VAR()) {
            parse(`_o.bind(${name}, "${part.VAR()?.text}", ${part.VAR()?.text})\n`, indentation);
        }
    });
};

export const parsePublicApi = (
    bodyContext: p.PublicApiContext,
    contextManager: IContextManager,
    indentation: number,
    parse: (text: string, indentation: number) => void,
    name: string,
) => {
    if (!bodyContext.expression() && !bodyContext.publicObject()) {
        Throw("Cannot define existing public api without expression or object! This should never happen");
    }
    if (bodyContext.expression()?.functor()) {
        const functor = bodyContext.expression()?.functor();
        parse(`const functorApi = (${
            (() => { 
                contextManager.parseIgnorables("", functor?.ignoreableVar() || []);
                return parseIgnorableVars(functor?.ignoreableVar() || []);
            })()
        }) => ${functor?.expression
            ? parseExpression(functor?.expression()!, contextManager, indentation)
            : parseObject(functor?.object()!, contextManager, indentation, name + "Functor")
        };\n`, indentation);
    } else if (bodyContext.expression()) {
        parse(`return ${parseExpression(bodyContext.expression()!, contextManager, indentation)}`, indentation);
    } else if (bodyContext.publicObject()) {
        parsePublicObject(bodyContext.publicObject()!, contextManager, indentation, parse, name);
    }
}

export const parseObject = (
    object: p.ObjectContext,
    contextManager: IContextManager,
    indentation: number,
    name: string,
) => {
    let parsed: string = "";
    const parse = (value: string, indentation: number) => {
        parsed += replicateString(indentation, "   ") + value;
    };
    contextManager = contextManager.next();
    parseAssignments(object.assignment(), contextManager, indentation, parse);
    parseInheritance(object.inheritance(), contextManager, indentation, parse, name);
    if (object.publicApi()) {
        parsePublicApi(object.publicApi()!, contextManager, indentation, parse, name);
    }
    return parsed;
};

export const replicateString = (count: number, char: string) => {
    let reduce = "";
    for (let i = 0; i < count; i++) {
        reduce += char;
    }
    return reduce;
};

export const parseIgnorableVars = (vars: p.IgnoreableVarContext[]) => {
    let ignorableVars = 0;
    return vars.reduce((p, c) => {
        if (c.IGNORE()) {
            p += !p ? `_${replicateString(ignorableVars, "_")}` : p + `, _${replicateString(ignorableVars, "_")}`;
            ignorableVars++;
        } else {
            p += !p ? c.VAR()?.text : `, ${c.VAR()?.text}`;
        }
        return p;
    }, "");
};