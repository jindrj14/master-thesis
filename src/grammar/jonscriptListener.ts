// Generated from ./src/grammar/jonscript.g4 by ANTLR 4.7.3-SNAPSHOT


import { ParseTreeListener } from "antlr4ts/tree/ParseTreeListener";

import { ParentContext } from "./jonscriptParser";
import { ImportVarContext } from "./jonscriptParser";
import { DefaultImportContext } from "./jonscriptParser";
import { ImportedContext } from "./jonscriptParser";
import { ModuleContext } from "./jonscriptParser";
import { ClassdefContext } from "./jonscriptParser";
import { AssignmentContext } from "./jonscriptParser";
import { VarAssignmentContext } from "./jonscriptParser";
import { SingleVarAssignmentContext } from "./jonscriptParser";
import { PublicObjectContext } from "./jonscriptParser";
import { PublicApiContext } from "./jonscriptParser";
import { InheritanceContext } from "./jonscriptParser";
import { ObjectContext } from "./jonscriptParser";
import { FunctorContext } from "./jonscriptParser";
import { IgnoreableVarContext } from "./jonscriptParser";
import { TemplateContext } from "./jonscriptParser";
import { TemplateContentsContext } from "./jonscriptParser";
import { ExtendsTypeContext } from "./jonscriptParser";
import { EqualsTypeContext } from "./jonscriptParser";
import { TypeContext } from "./jonscriptParser";
import { AccessTypeContext } from "./jonscriptParser";
import { TypeEvalContext } from "./jonscriptParser";
import { TupleContext } from "./jonscriptParser";
import { TemplateTypeContext } from "./jonscriptParser";
import { ParamTypeContext } from "./jonscriptParser";
import { FunctionTypeContext } from "./jonscriptParser";
import { ObjectTypeContext } from "./jonscriptParser";
import { ObjectParamTypeContext } from "./jonscriptParser";
import { ExpressionContext } from "./jonscriptParser";
import { EvalContext } from "./jonscriptParser";
import { InterpolatedContext } from "./jonscriptParser";
import { ExpressionListContext } from "./jonscriptParser";
import { ExpressionItemContext } from "./jonscriptParser";


/**
 * This interface defines a complete listener for a parse tree produced by
 * `jonscriptParser`.
 */
export interface jonscriptListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by `jonscriptParser.parent`.
	 * @param ctx the parse tree
	 */
	enterParent?: (ctx: ParentContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.parent`.
	 * @param ctx the parse tree
	 */
	exitParent?: (ctx: ParentContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.importVar`.
	 * @param ctx the parse tree
	 */
	enterImportVar?: (ctx: ImportVarContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.importVar`.
	 * @param ctx the parse tree
	 */
	exitImportVar?: (ctx: ImportVarContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.defaultImport`.
	 * @param ctx the parse tree
	 */
	enterDefaultImport?: (ctx: DefaultImportContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.defaultImport`.
	 * @param ctx the parse tree
	 */
	exitDefaultImport?: (ctx: DefaultImportContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.imported`.
	 * @param ctx the parse tree
	 */
	enterImported?: (ctx: ImportedContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.imported`.
	 * @param ctx the parse tree
	 */
	exitImported?: (ctx: ImportedContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.module`.
	 * @param ctx the parse tree
	 */
	enterModule?: (ctx: ModuleContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.module`.
	 * @param ctx the parse tree
	 */
	exitModule?: (ctx: ModuleContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.classdef`.
	 * @param ctx the parse tree
	 */
	enterClassdef?: (ctx: ClassdefContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.classdef`.
	 * @param ctx the parse tree
	 */
	exitClassdef?: (ctx: ClassdefContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.assignment`.
	 * @param ctx the parse tree
	 */
	enterAssignment?: (ctx: AssignmentContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.assignment`.
	 * @param ctx the parse tree
	 */
	exitAssignment?: (ctx: AssignmentContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.varAssignment`.
	 * @param ctx the parse tree
	 */
	enterVarAssignment?: (ctx: VarAssignmentContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.varAssignment`.
	 * @param ctx the parse tree
	 */
	exitVarAssignment?: (ctx: VarAssignmentContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.singleVarAssignment`.
	 * @param ctx the parse tree
	 */
	enterSingleVarAssignment?: (ctx: SingleVarAssignmentContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.singleVarAssignment`.
	 * @param ctx the parse tree
	 */
	exitSingleVarAssignment?: (ctx: SingleVarAssignmentContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.publicObject`.
	 * @param ctx the parse tree
	 */
	enterPublicObject?: (ctx: PublicObjectContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.publicObject`.
	 * @param ctx the parse tree
	 */
	exitPublicObject?: (ctx: PublicObjectContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.publicApi`.
	 * @param ctx the parse tree
	 */
	enterPublicApi?: (ctx: PublicApiContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.publicApi`.
	 * @param ctx the parse tree
	 */
	exitPublicApi?: (ctx: PublicApiContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.inheritance`.
	 * @param ctx the parse tree
	 */
	enterInheritance?: (ctx: InheritanceContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.inheritance`.
	 * @param ctx the parse tree
	 */
	exitInheritance?: (ctx: InheritanceContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.object`.
	 * @param ctx the parse tree
	 */
	enterObject?: (ctx: ObjectContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.object`.
	 * @param ctx the parse tree
	 */
	exitObject?: (ctx: ObjectContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.functor`.
	 * @param ctx the parse tree
	 */
	enterFunctor?: (ctx: FunctorContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.functor`.
	 * @param ctx the parse tree
	 */
	exitFunctor?: (ctx: FunctorContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.ignoreableVar`.
	 * @param ctx the parse tree
	 */
	enterIgnoreableVar?: (ctx: IgnoreableVarContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.ignoreableVar`.
	 * @param ctx the parse tree
	 */
	exitIgnoreableVar?: (ctx: IgnoreableVarContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.template`.
	 * @param ctx the parse tree
	 */
	enterTemplate?: (ctx: TemplateContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.template`.
	 * @param ctx the parse tree
	 */
	exitTemplate?: (ctx: TemplateContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.templateContents`.
	 * @param ctx the parse tree
	 */
	enterTemplateContents?: (ctx: TemplateContentsContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.templateContents`.
	 * @param ctx the parse tree
	 */
	exitTemplateContents?: (ctx: TemplateContentsContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.extendsType`.
	 * @param ctx the parse tree
	 */
	enterExtendsType?: (ctx: ExtendsTypeContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.extendsType`.
	 * @param ctx the parse tree
	 */
	exitExtendsType?: (ctx: ExtendsTypeContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.equalsType`.
	 * @param ctx the parse tree
	 */
	enterEqualsType?: (ctx: EqualsTypeContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.equalsType`.
	 * @param ctx the parse tree
	 */
	exitEqualsType?: (ctx: EqualsTypeContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.type`.
	 * @param ctx the parse tree
	 */
	enterType?: (ctx: TypeContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.type`.
	 * @param ctx the parse tree
	 */
	exitType?: (ctx: TypeContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.accessType`.
	 * @param ctx the parse tree
	 */
	enterAccessType?: (ctx: AccessTypeContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.accessType`.
	 * @param ctx the parse tree
	 */
	exitAccessType?: (ctx: AccessTypeContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.typeEval`.
	 * @param ctx the parse tree
	 */
	enterTypeEval?: (ctx: TypeEvalContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.typeEval`.
	 * @param ctx the parse tree
	 */
	exitTypeEval?: (ctx: TypeEvalContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.tuple`.
	 * @param ctx the parse tree
	 */
	enterTuple?: (ctx: TupleContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.tuple`.
	 * @param ctx the parse tree
	 */
	exitTuple?: (ctx: TupleContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.templateType`.
	 * @param ctx the parse tree
	 */
	enterTemplateType?: (ctx: TemplateTypeContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.templateType`.
	 * @param ctx the parse tree
	 */
	exitTemplateType?: (ctx: TemplateTypeContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.paramType`.
	 * @param ctx the parse tree
	 */
	enterParamType?: (ctx: ParamTypeContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.paramType`.
	 * @param ctx the parse tree
	 */
	exitParamType?: (ctx: ParamTypeContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.functionType`.
	 * @param ctx the parse tree
	 */
	enterFunctionType?: (ctx: FunctionTypeContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.functionType`.
	 * @param ctx the parse tree
	 */
	exitFunctionType?: (ctx: FunctionTypeContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.objectType`.
	 * @param ctx the parse tree
	 */
	enterObjectType?: (ctx: ObjectTypeContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.objectType`.
	 * @param ctx the parse tree
	 */
	exitObjectType?: (ctx: ObjectTypeContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.objectParamType`.
	 * @param ctx the parse tree
	 */
	enterObjectParamType?: (ctx: ObjectParamTypeContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.objectParamType`.
	 * @param ctx the parse tree
	 */
	exitObjectParamType?: (ctx: ObjectParamTypeContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.expression`.
	 * @param ctx the parse tree
	 */
	enterExpression?: (ctx: ExpressionContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.expression`.
	 * @param ctx the parse tree
	 */
	exitExpression?: (ctx: ExpressionContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.eval`.
	 * @param ctx the parse tree
	 */
	enterEval?: (ctx: EvalContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.eval`.
	 * @param ctx the parse tree
	 */
	exitEval?: (ctx: EvalContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.interpolated`.
	 * @param ctx the parse tree
	 */
	enterInterpolated?: (ctx: InterpolatedContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.interpolated`.
	 * @param ctx the parse tree
	 */
	exitInterpolated?: (ctx: InterpolatedContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.expressionList`.
	 * @param ctx the parse tree
	 */
	enterExpressionList?: (ctx: ExpressionListContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.expressionList`.
	 * @param ctx the parse tree
	 */
	exitExpressionList?: (ctx: ExpressionListContext) => void;

	/**
	 * Enter a parse tree produced by `jonscriptParser.expressionItem`.
	 * @param ctx the parse tree
	 */
	enterExpressionItem?: (ctx: ExpressionItemContext) => void;
	/**
	 * Exit a parse tree produced by `jonscriptParser.expressionItem`.
	 * @param ctx the parse tree
	 */
	exitExpressionItem?: (ctx: ExpressionItemContext) => void;
}

