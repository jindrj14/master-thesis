grammar jonScriptParser ;
import jonScriptLexer ;

/*
 * Parser Rules
 */

parent : imported* module ;

importVar : VAR | (VAR AS VAR) ;
defaultImport: VAR;
imported : IMPORT (defaultImport | (OBJECT_BEGIN (importVar COMMA)* importVar OBJECT_END)) FROM ATOMIC;

module : VAR OBJECT_BEGIN
    classdef*
OBJECT_END ;

classdef : VAR template? (EVAL_BEGIN (ignoreableVar COMMA)*(INHERITS? ignoreableVar)? EVAL_END object) ;

assignment : (VAR COMMA) | ((VAR | IGNORE) COLON (object | expression) COMMA) ;
varAssignment : (VAR COMMA) | (VAR COLON (object | expression) COMMA) ;
singleVarAssignment : VAR | (VAR COLON (object | expression)) ;
publicObject : OBJECT_BEGIN (functor | singleVarAssignment | ((functor COMMA)? varAssignment*)) OBJECT_END;
publicApi : (publicObject | expression) ;
inheritance: INHERITS expression COMMA;

object : OBJECT_BEGIN
    assignment*
    inheritance*
    publicApi? // Public interface
OBJECT_END ;
functor : (VAR | (template? EVAL_BEGIN (ignoreableVar COMMA)*(INHERITS? ignoreableVar)? EVAL_END (COLON type)? )) ARROW (object | expression); // Much later we can remove the '?'

ignoreableVar : (VAR QMARK? (COLON type)? (EQUALS expression)?) | IGNORE; // Much later we can remove the '?'

template : LESS_THAN (templateContents COMMA)* templateContents MORE_THAN;
templateContents : VAR extendsType? equalsType?;
extendsType : EXTENDS type;
equalsType : EQUALS type;

type : VAR
    | type LIST_BEGIN LIST_END
    | typeEval
    | tuple
    | type op=TYPE_OPERATORS type
    | (VAR | accessType) templateType
    | ATOMIC
    | functionType
    | objectType
    | accessType ;
accessType : VAR OBJECT_OPERATORS (accessType | VAR);
typeEval : EVAL_BEGIN type EVAL_END;
tuple : LIST_BEGIN (type (COMMA type)*)? LIST_END;
templateType : LESS_THAN type (COMMA type)* MORE_THAN;
paramType : VAR QMARK? COLON type;
functionType : template? EVAL_BEGIN (paramType (COMMA paramType)*)? EVAL_END ARROW type ;
objectType : OBJECT_BEGIN (objectParamType (COMMA objectParamType)* COMMA?)? OBJECT_END ;
objectParamType : VAR QMARK? COLON type;

expression : ATOMIC
    | VAR templateType?
    | functor
    | expression obj=OBJECT_OPERATORS expression
    | expression obj=OBJECT_OPERATORS (THROW | IMPORT | AS | IS | TRY | CATCH | FINALLY | EXTENDS | FROM)
    | expression EVAL_BEGIN expressionList? EVAL_END
    | expression LIST_BEGIN expression LIST_END
    | eval
    | LIST_BEGIN expressionList? LIST_END
    | PREFIX_UNARY_OPERATORS expression
    | INTERPOLATED_SEQUENCE_EMPTY
    | INTERPOLATED_SEQUENCE_STRING
    | INTERPOLATED_SEQUENCE interpolated? INTERPOLATED_SEQUENCE
    | expression op=MULTIPLYDIVIDEMOD_OPERATORS expression
    | expression op=PLUSMINUS_OPERATORS expression
    | expression cmp=(EQUAL_OR_GREATER | EQUAL_OR_LESS | LESS_THAN | MORE_THAN) expression
    | expression cmp=(EQUALITY | NEQUALITY) expression
    | expression logic=LOGICAL_OPERATORS expression
    | expression IS expression
    | THROW expression
    | object
    | TRY expression CATCH expression (FINALLY expression)?
    | expression QMARK expression COLON expression ;

eval : EVAL_BEGIN expression EVAL_END;
interpolated : (INTERPOLATED_SEQUENCE | expression) interpolated?;
expressionList : expressionItem (COMMA expressionItem)* COMMA?;
expressionItem : (INHERITS? expression);
