// Generated from ./src/grammar/jonscript.g4 by ANTLR 4.7.3-SNAPSHOT


import { ATN } from "antlr4ts/atn/ATN";
import { ATNDeserializer } from "antlr4ts/atn/ATNDeserializer";
import { FailedPredicateException } from "antlr4ts/FailedPredicateException";
import { NotNull } from "antlr4ts/Decorators";
import { NoViableAltException } from "antlr4ts/NoViableAltException";
import { Override } from "antlr4ts/Decorators";
import { Parser } from "antlr4ts/Parser";
import { ParserRuleContext } from "antlr4ts/ParserRuleContext";
import { ParserATNSimulator } from "antlr4ts/atn/ParserATNSimulator";
import { ParseTreeListener } from "antlr4ts/tree/ParseTreeListener";
import { ParseTreeVisitor } from "antlr4ts/tree/ParseTreeVisitor";
import { RecognitionException } from "antlr4ts/RecognitionException";
import { RuleContext } from "antlr4ts/RuleContext";
//import { RuleVersion } from "antlr4ts/RuleVersion";
import { TerminalNode } from "antlr4ts/tree/TerminalNode";
import { Token } from "antlr4ts/Token";
import { TokenStream } from "antlr4ts/TokenStream";
import { Vocabulary } from "antlr4ts/Vocabulary";
import { VocabularyImpl } from "antlr4ts/VocabularyImpl";

import * as Utils from "antlr4ts/misc/Utils";

import { jonscriptListener } from "./jonscriptListener";
import { jonscriptVisitor } from "./jonscriptVisitor";


export class jonscriptParser extends Parser {
	public static readonly TRY = 1;
	public static readonly CATCH = 2;
	public static readonly FINALLY = 3;
	public static readonly THROW = 4;
	public static readonly IMPORT = 5;
	public static readonly EXTENDS = 6;
	public static readonly EQUALS = 7;
	public static readonly IS = 8;
	public static readonly AS = 9;
	public static readonly FROM = 10;
	public static readonly ARROW = 11;
	public static readonly INHERITS = 12;
	public static readonly QMARK = 13;
	public static readonly COLON = 14;
	public static readonly LIST_BEGIN = 15;
	public static readonly LIST_END = 16;
	public static readonly EVAL_BEGIN = 17;
	public static readonly EVAL_END = 18;
	public static readonly OBJECT_BEGIN = 19;
	public static readonly OBJECT_END = 20;
	public static readonly COMMA = 21;
	public static readonly ATOMIC = 22;
	public static readonly NIL = 23;
	public static readonly BOOLEAN = 24;
	public static readonly NUMBER = 25;
	public static readonly SINGLE_COMMENT = 26;
	public static readonly MULTILINE_COMMENT = 27;
	public static readonly NEWLINE = 28;
	public static readonly SPACE = 29;
	public static readonly STRING = 30;
	public static readonly INTERPOLATED_SEQUENCE_EMPTY = 31;
	public static readonly INTERPOLATED_SEQUENCE_STRING = 32;
	public static readonly INTERPOLATED_SEQUENCE = 33;
	public static readonly LOGICAL_OPERATORS = 34;
	public static readonly MULTIPLYDIVIDEMOD_OPERATORS = 35;
	public static readonly PLUSMINUS_OPERATORS = 36;
	public static readonly EQUALITY = 37;
	public static readonly NEQUALITY = 38;
	public static readonly EQUAL_OR_GREATER = 39;
	public static readonly EQUAL_OR_LESS = 40;
	public static readonly LESS_THAN = 41;
	public static readonly MORE_THAN = 42;
	public static readonly TYPE_OPERATORS = 43;
	public static readonly OBJECT_OPERATORS = 44;
	public static readonly PREFIX_UNARY_OPERATORS = 45;
	public static readonly IGNORE = 46;
	public static readonly VAR = 47;
	public static readonly RULE_parent = 0;
	public static readonly RULE_importVar = 1;
	public static readonly RULE_defaultImport = 2;
	public static readonly RULE_imported = 3;
	public static readonly RULE_module = 4;
	public static readonly RULE_classdef = 5;
	public static readonly RULE_assignment = 6;
	public static readonly RULE_varAssignment = 7;
	public static readonly RULE_singleVarAssignment = 8;
	public static readonly RULE_publicObject = 9;
	public static readonly RULE_publicApi = 10;
	public static readonly RULE_inheritance = 11;
	public static readonly RULE_object = 12;
	public static readonly RULE_functor = 13;
	public static readonly RULE_ignoreableVar = 14;
	public static readonly RULE_template = 15;
	public static readonly RULE_templateContents = 16;
	public static readonly RULE_extendsType = 17;
	public static readonly RULE_equalsType = 18;
	public static readonly RULE_type = 19;
	public static readonly RULE_accessType = 20;
	public static readonly RULE_typeEval = 21;
	public static readonly RULE_tuple = 22;
	public static readonly RULE_templateType = 23;
	public static readonly RULE_paramType = 24;
	public static readonly RULE_functionType = 25;
	public static readonly RULE_objectType = 26;
	public static readonly RULE_objectParamType = 27;
	public static readonly RULE_expression = 28;
	public static readonly RULE_eval = 29;
	public static readonly RULE_interpolated = 30;
	public static readonly RULE_expressionList = 31;
	public static readonly RULE_expressionItem = 32;
	// tslint:disable:no-trailing-whitespace
	public static readonly ruleNames: string[] = [
		"parent", "importVar", "defaultImport", "imported", "module", "classdef", 
		"assignment", "varAssignment", "singleVarAssignment", "publicObject", 
		"publicApi", "inheritance", "object", "functor", "ignoreableVar", "template", 
		"templateContents", "extendsType", "equalsType", "type", "accessType", 
		"typeEval", "tuple", "templateType", "paramType", "functionType", "objectType", 
		"objectParamType", "expression", "eval", "interpolated", "expressionList", 
		"expressionItem",
	];

	private static readonly _LITERAL_NAMES: Array<string | undefined> = [
		undefined, "'try'", "'catch'", "'finally'", "'throw'", "'import'", "'extends'", 
		"'='", "'is'", "'as'", "'from'", "'=>'", "'...'", "'?'", "':'", "'['", 
		"']'", "'('", "')'", "'{'", "'}'", "','", undefined, "'nil'", undefined, 
		undefined, undefined, undefined, undefined, undefined, undefined, "'``'", 
		undefined, undefined, undefined, undefined, undefined, "'=='", "'!='", 
		"'>='", "'<='", "'<'", "'>'", undefined, "'.'", undefined, "'_'",
	];
	private static readonly _SYMBOLIC_NAMES: Array<string | undefined> = [
		undefined, "TRY", "CATCH", "FINALLY", "THROW", "IMPORT", "EXTENDS", "EQUALS", 
		"IS", "AS", "FROM", "ARROW", "INHERITS", "QMARK", "COLON", "LIST_BEGIN", 
		"LIST_END", "EVAL_BEGIN", "EVAL_END", "OBJECT_BEGIN", "OBJECT_END", "COMMA", 
		"ATOMIC", "NIL", "BOOLEAN", "NUMBER", "SINGLE_COMMENT", "MULTILINE_COMMENT", 
		"NEWLINE", "SPACE", "STRING", "INTERPOLATED_SEQUENCE_EMPTY", "INTERPOLATED_SEQUENCE_STRING", 
		"INTERPOLATED_SEQUENCE", "LOGICAL_OPERATORS", "MULTIPLYDIVIDEMOD_OPERATORS", 
		"PLUSMINUS_OPERATORS", "EQUALITY", "NEQUALITY", "EQUAL_OR_GREATER", "EQUAL_OR_LESS", 
		"LESS_THAN", "MORE_THAN", "TYPE_OPERATORS", "OBJECT_OPERATORS", "PREFIX_UNARY_OPERATORS", 
		"IGNORE", "VAR",
	];
	public static readonly VOCABULARY: Vocabulary = new VocabularyImpl(jonscriptParser._LITERAL_NAMES, jonscriptParser._SYMBOLIC_NAMES, []);

	// @Override
	// @NotNull
	public get vocabulary(): Vocabulary {
		return jonscriptParser.VOCABULARY;
	}
	// tslint:enable:no-trailing-whitespace

	// @Override
	public get grammarFileName(): string { return "jonscript.g4"; }

	// @Override
	public get ruleNames(): string[] { return jonscriptParser.ruleNames; }

	// @Override
	public get serializedATN(): string { return jonscriptParser._serializedATN; }

	constructor(input: TokenStream) {
		super(input);
		this._interp = new ParserATNSimulator(jonscriptParser._ATN, this);
	}
	// @RuleVersion(0)
	public parent(): ParentContext {
		let _localctx: ParentContext = new ParentContext(this._ctx, this.state);
		this.enterRule(_localctx, 0, jonscriptParser.RULE_parent);
		let _la: number;
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 69;
			this._errHandler.sync(this);
			_la = this._input.LA(1);
			while (_la === jonscriptParser.IMPORT) {
				{
				{
				this.state = 66;
				this.imported();
				}
				}
				this.state = 71;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
			}
			this.state = 72;
			this.module();
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public importVar(): ImportVarContext {
		let _localctx: ImportVarContext = new ImportVarContext(this._ctx, this.state);
		this.enterRule(_localctx, 2, jonscriptParser.RULE_importVar);
		try {
			this.state = 78;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input, 1, this._ctx) ) {
			case 1:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 74;
				this.match(jonscriptParser.VAR);
				}
				break;

			case 2:
				this.enterOuterAlt(_localctx, 2);
				{
				{
				this.state = 75;
				this.match(jonscriptParser.VAR);
				this.state = 76;
				this.match(jonscriptParser.AS);
				this.state = 77;
				this.match(jonscriptParser.VAR);
				}
				}
				break;
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public defaultImport(): DefaultImportContext {
		let _localctx: DefaultImportContext = new DefaultImportContext(this._ctx, this.state);
		this.enterRule(_localctx, 4, jonscriptParser.RULE_defaultImport);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 80;
			this.match(jonscriptParser.VAR);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public imported(): ImportedContext {
		let _localctx: ImportedContext = new ImportedContext(this._ctx, this.state);
		this.enterRule(_localctx, 6, jonscriptParser.RULE_imported);
		try {
			let _alt: number;
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 82;
			this.match(jonscriptParser.IMPORT);
			this.state = 96;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case jonscriptParser.VAR:
				{
				this.state = 83;
				this.defaultImport();
				}
				break;
			case jonscriptParser.OBJECT_BEGIN:
				{
				{
				this.state = 84;
				this.match(jonscriptParser.OBJECT_BEGIN);
				this.state = 90;
				this._errHandler.sync(this);
				_alt = this.interpreter.adaptivePredict(this._input, 2, this._ctx);
				while (_alt !== 2 && _alt !== ATN.INVALID_ALT_NUMBER) {
					if (_alt === 1) {
						{
						{
						this.state = 85;
						this.importVar();
						this.state = 86;
						this.match(jonscriptParser.COMMA);
						}
						}
					}
					this.state = 92;
					this._errHandler.sync(this);
					_alt = this.interpreter.adaptivePredict(this._input, 2, this._ctx);
				}
				this.state = 93;
				this.importVar();
				this.state = 94;
				this.match(jonscriptParser.OBJECT_END);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			this.state = 98;
			this.match(jonscriptParser.FROM);
			this.state = 99;
			this.match(jonscriptParser.ATOMIC);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public module(): ModuleContext {
		let _localctx: ModuleContext = new ModuleContext(this._ctx, this.state);
		this.enterRule(_localctx, 8, jonscriptParser.RULE_module);
		let _la: number;
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 101;
			this.match(jonscriptParser.VAR);
			this.state = 102;
			this.match(jonscriptParser.OBJECT_BEGIN);
			this.state = 106;
			this._errHandler.sync(this);
			_la = this._input.LA(1);
			while (_la === jonscriptParser.VAR) {
				{
				{
				this.state = 103;
				this.classdef();
				}
				}
				this.state = 108;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
			}
			this.state = 109;
			this.match(jonscriptParser.OBJECT_END);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public classdef(): ClassdefContext {
		let _localctx: ClassdefContext = new ClassdefContext(this._ctx, this.state);
		this.enterRule(_localctx, 10, jonscriptParser.RULE_classdef);
		let _la: number;
		try {
			let _alt: number;
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 111;
			this.match(jonscriptParser.VAR);
			this.state = 113;
			this._errHandler.sync(this);
			_la = this._input.LA(1);
			if (_la === jonscriptParser.LESS_THAN) {
				{
				this.state = 112;
				this.template();
				}
			}

			{
			this.state = 115;
			this.match(jonscriptParser.EVAL_BEGIN);
			this.state = 121;
			this._errHandler.sync(this);
			_alt = this.interpreter.adaptivePredict(this._input, 6, this._ctx);
			while (_alt !== 2 && _alt !== ATN.INVALID_ALT_NUMBER) {
				if (_alt === 1) {
					{
					{
					this.state = 116;
					this.ignoreableVar();
					this.state = 117;
					this.match(jonscriptParser.COMMA);
					}
					}
				}
				this.state = 123;
				this._errHandler.sync(this);
				_alt = this.interpreter.adaptivePredict(this._input, 6, this._ctx);
			}
			this.state = 128;
			this._errHandler.sync(this);
			_la = this._input.LA(1);
			if (_la === jonscriptParser.INHERITS || _la === jonscriptParser.IGNORE || _la === jonscriptParser.VAR) {
				{
				this.state = 125;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				if (_la === jonscriptParser.INHERITS) {
					{
					this.state = 124;
					this.match(jonscriptParser.INHERITS);
					}
				}

				this.state = 127;
				this.ignoreableVar();
				}
			}

			this.state = 130;
			this.match(jonscriptParser.EVAL_END);
			this.state = 131;
			this.object();
			}
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public assignment(): AssignmentContext {
		let _localctx: AssignmentContext = new AssignmentContext(this._ctx, this.state);
		this.enterRule(_localctx, 12, jonscriptParser.RULE_assignment);
		let _la: number;
		try {
			this.state = 143;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input, 10, this._ctx) ) {
			case 1:
				this.enterOuterAlt(_localctx, 1);
				{
				{
				this.state = 133;
				this.match(jonscriptParser.VAR);
				this.state = 134;
				this.match(jonscriptParser.COMMA);
				}
				}
				break;

			case 2:
				this.enterOuterAlt(_localctx, 2);
				{
				{
				this.state = 135;
				_la = this._input.LA(1);
				if (!(_la === jonscriptParser.IGNORE || _la === jonscriptParser.VAR)) {
				this._errHandler.recoverInline(this);
				} else {
					if (this._input.LA(1) === Token.EOF) {
						this.matchedEOF = true;
					}

					this._errHandler.reportMatch(this);
					this.consume();
				}
				this.state = 136;
				this.match(jonscriptParser.COLON);
				this.state = 139;
				this._errHandler.sync(this);
				switch ( this.interpreter.adaptivePredict(this._input, 9, this._ctx) ) {
				case 1:
					{
					this.state = 137;
					this.object();
					}
					break;

				case 2:
					{
					this.state = 138;
					this.expression(0);
					}
					break;
				}
				this.state = 141;
				this.match(jonscriptParser.COMMA);
				}
				}
				break;
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public varAssignment(): VarAssignmentContext {
		let _localctx: VarAssignmentContext = new VarAssignmentContext(this._ctx, this.state);
		this.enterRule(_localctx, 14, jonscriptParser.RULE_varAssignment);
		try {
			this.state = 155;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input, 12, this._ctx) ) {
			case 1:
				this.enterOuterAlt(_localctx, 1);
				{
				{
				this.state = 145;
				this.match(jonscriptParser.VAR);
				this.state = 146;
				this.match(jonscriptParser.COMMA);
				}
				}
				break;

			case 2:
				this.enterOuterAlt(_localctx, 2);
				{
				{
				this.state = 147;
				this.match(jonscriptParser.VAR);
				this.state = 148;
				this.match(jonscriptParser.COLON);
				this.state = 151;
				this._errHandler.sync(this);
				switch ( this.interpreter.adaptivePredict(this._input, 11, this._ctx) ) {
				case 1:
					{
					this.state = 149;
					this.object();
					}
					break;

				case 2:
					{
					this.state = 150;
					this.expression(0);
					}
					break;
				}
				this.state = 153;
				this.match(jonscriptParser.COMMA);
				}
				}
				break;
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public singleVarAssignment(): SingleVarAssignmentContext {
		let _localctx: SingleVarAssignmentContext = new SingleVarAssignmentContext(this._ctx, this.state);
		this.enterRule(_localctx, 16, jonscriptParser.RULE_singleVarAssignment);
		try {
			this.state = 164;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input, 14, this._ctx) ) {
			case 1:
				this.enterOuterAlt(_localctx, 1);
				{
				this.state = 157;
				this.match(jonscriptParser.VAR);
				}
				break;

			case 2:
				this.enterOuterAlt(_localctx, 2);
				{
				{
				this.state = 158;
				this.match(jonscriptParser.VAR);
				this.state = 159;
				this.match(jonscriptParser.COLON);
				this.state = 162;
				this._errHandler.sync(this);
				switch ( this.interpreter.adaptivePredict(this._input, 13, this._ctx) ) {
				case 1:
					{
					this.state = 160;
					this.object();
					}
					break;

				case 2:
					{
					this.state = 161;
					this.expression(0);
					}
					break;
				}
				}
				}
				break;
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public publicObject(): PublicObjectContext {
		let _localctx: PublicObjectContext = new PublicObjectContext(this._ctx, this.state);
		this.enterRule(_localctx, 18, jonscriptParser.RULE_publicObject);
		let _la: number;
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 166;
			this.match(jonscriptParser.OBJECT_BEGIN);
			this.state = 180;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input, 17, this._ctx) ) {
			case 1:
				{
				this.state = 167;
				this.functor();
				}
				break;

			case 2:
				{
				this.state = 168;
				this.singleVarAssignment();
				}
				break;

			case 3:
				{
				{
				this.state = 172;
				this._errHandler.sync(this);
				switch ( this.interpreter.adaptivePredict(this._input, 15, this._ctx) ) {
				case 1:
					{
					this.state = 169;
					this.functor();
					this.state = 170;
					this.match(jonscriptParser.COMMA);
					}
					break;
				}
				this.state = 177;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				while (_la === jonscriptParser.VAR) {
					{
					{
					this.state = 174;
					this.varAssignment();
					}
					}
					this.state = 179;
					this._errHandler.sync(this);
					_la = this._input.LA(1);
				}
				}
				}
				break;
			}
			this.state = 182;
			this.match(jonscriptParser.OBJECT_END);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public publicApi(): PublicApiContext {
		let _localctx: PublicApiContext = new PublicApiContext(this._ctx, this.state);
		this.enterRule(_localctx, 20, jonscriptParser.RULE_publicApi);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 186;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input, 18, this._ctx) ) {
			case 1:
				{
				this.state = 184;
				this.publicObject();
				}
				break;

			case 2:
				{
				this.state = 185;
				this.expression(0);
				}
				break;
			}
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public inheritance(): InheritanceContext {
		let _localctx: InheritanceContext = new InheritanceContext(this._ctx, this.state);
		this.enterRule(_localctx, 22, jonscriptParser.RULE_inheritance);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 188;
			this.match(jonscriptParser.INHERITS);
			this.state = 189;
			this.expression(0);
			this.state = 190;
			this.match(jonscriptParser.COMMA);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public object(): ObjectContext {
		let _localctx: ObjectContext = new ObjectContext(this._ctx, this.state);
		this.enterRule(_localctx, 24, jonscriptParser.RULE_object);
		let _la: number;
		try {
			let _alt: number;
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 192;
			this.match(jonscriptParser.OBJECT_BEGIN);
			this.state = 196;
			this._errHandler.sync(this);
			_alt = this.interpreter.adaptivePredict(this._input, 19, this._ctx);
			while (_alt !== 2 && _alt !== ATN.INVALID_ALT_NUMBER) {
				if (_alt === 1) {
					{
					{
					this.state = 193;
					this.assignment();
					}
					}
				}
				this.state = 198;
				this._errHandler.sync(this);
				_alt = this.interpreter.adaptivePredict(this._input, 19, this._ctx);
			}
			this.state = 202;
			this._errHandler.sync(this);
			_la = this._input.LA(1);
			while (_la === jonscriptParser.INHERITS) {
				{
				{
				this.state = 199;
				this.inheritance();
				}
				}
				this.state = 204;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
			}
			this.state = 206;
			this._errHandler.sync(this);
			_la = this._input.LA(1);
			if ((((_la) & ~0x1F) === 0 && ((1 << _la) & ((1 << jonscriptParser.TRY) | (1 << jonscriptParser.THROW) | (1 << jonscriptParser.LIST_BEGIN) | (1 << jonscriptParser.EVAL_BEGIN) | (1 << jonscriptParser.OBJECT_BEGIN) | (1 << jonscriptParser.ATOMIC) | (1 << jonscriptParser.INTERPOLATED_SEQUENCE_EMPTY))) !== 0) || ((((_la - 32)) & ~0x1F) === 0 && ((1 << (_la - 32)) & ((1 << (jonscriptParser.INTERPOLATED_SEQUENCE_STRING - 32)) | (1 << (jonscriptParser.INTERPOLATED_SEQUENCE - 32)) | (1 << (jonscriptParser.LESS_THAN - 32)) | (1 << (jonscriptParser.PREFIX_UNARY_OPERATORS - 32)) | (1 << (jonscriptParser.VAR - 32)))) !== 0)) {
				{
				this.state = 205;
				this.publicApi();
				}
			}

			this.state = 208;
			this.match(jonscriptParser.OBJECT_END);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public functor(): FunctorContext {
		let _localctx: FunctorContext = new FunctorContext(this._ctx, this.state);
		this.enterRule(_localctx, 26, jonscriptParser.RULE_functor);
		let _la: number;
		try {
			let _alt: number;
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 234;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case jonscriptParser.VAR:
				{
				this.state = 210;
				this.match(jonscriptParser.VAR);
				}
				break;
			case jonscriptParser.EVAL_BEGIN:
			case jonscriptParser.LESS_THAN:
				{
				{
				this.state = 212;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				if (_la === jonscriptParser.LESS_THAN) {
					{
					this.state = 211;
					this.template();
					}
				}

				this.state = 214;
				this.match(jonscriptParser.EVAL_BEGIN);
				this.state = 220;
				this._errHandler.sync(this);
				_alt = this.interpreter.adaptivePredict(this._input, 23, this._ctx);
				while (_alt !== 2 && _alt !== ATN.INVALID_ALT_NUMBER) {
					if (_alt === 1) {
						{
						{
						this.state = 215;
						this.ignoreableVar();
						this.state = 216;
						this.match(jonscriptParser.COMMA);
						}
						}
					}
					this.state = 222;
					this._errHandler.sync(this);
					_alt = this.interpreter.adaptivePredict(this._input, 23, this._ctx);
				}
				this.state = 227;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				if (_la === jonscriptParser.INHERITS || _la === jonscriptParser.IGNORE || _la === jonscriptParser.VAR) {
					{
					this.state = 224;
					this._errHandler.sync(this);
					_la = this._input.LA(1);
					if (_la === jonscriptParser.INHERITS) {
						{
						this.state = 223;
						this.match(jonscriptParser.INHERITS);
						}
					}

					this.state = 226;
					this.ignoreableVar();
					}
				}

				this.state = 229;
				this.match(jonscriptParser.EVAL_END);
				this.state = 232;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				if (_la === jonscriptParser.COLON) {
					{
					this.state = 230;
					this.match(jonscriptParser.COLON);
					this.state = 231;
					this.type(0);
					}
				}

				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			this.state = 236;
			this.match(jonscriptParser.ARROW);
			this.state = 239;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input, 28, this._ctx) ) {
			case 1:
				{
				this.state = 237;
				this.object();
				}
				break;

			case 2:
				{
				this.state = 238;
				this.expression(0);
				}
				break;
			}
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public ignoreableVar(): IgnoreableVarContext {
		let _localctx: IgnoreableVarContext = new IgnoreableVarContext(this._ctx, this.state);
		this.enterRule(_localctx, 28, jonscriptParser.RULE_ignoreableVar);
		let _la: number;
		try {
			this.state = 254;
			this._errHandler.sync(this);
			switch (this._input.LA(1)) {
			case jonscriptParser.VAR:
				this.enterOuterAlt(_localctx, 1);
				{
				{
				this.state = 241;
				this.match(jonscriptParser.VAR);
				this.state = 243;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				if (_la === jonscriptParser.QMARK) {
					{
					this.state = 242;
					this.match(jonscriptParser.QMARK);
					}
				}

				this.state = 247;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				if (_la === jonscriptParser.COLON) {
					{
					this.state = 245;
					this.match(jonscriptParser.COLON);
					this.state = 246;
					this.type(0);
					}
				}

				this.state = 251;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				if (_la === jonscriptParser.EQUALS) {
					{
					this.state = 249;
					this.match(jonscriptParser.EQUALS);
					this.state = 250;
					this.expression(0);
					}
				}

				}
				}
				break;
			case jonscriptParser.IGNORE:
				this.enterOuterAlt(_localctx, 2);
				{
				this.state = 253;
				this.match(jonscriptParser.IGNORE);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public template(): TemplateContext {
		let _localctx: TemplateContext = new TemplateContext(this._ctx, this.state);
		this.enterRule(_localctx, 30, jonscriptParser.RULE_template);
		try {
			let _alt: number;
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 256;
			this.match(jonscriptParser.LESS_THAN);
			this.state = 262;
			this._errHandler.sync(this);
			_alt = this.interpreter.adaptivePredict(this._input, 33, this._ctx);
			while (_alt !== 2 && _alt !== ATN.INVALID_ALT_NUMBER) {
				if (_alt === 1) {
					{
					{
					this.state = 257;
					this.templateContents();
					this.state = 258;
					this.match(jonscriptParser.COMMA);
					}
					}
				}
				this.state = 264;
				this._errHandler.sync(this);
				_alt = this.interpreter.adaptivePredict(this._input, 33, this._ctx);
			}
			this.state = 265;
			this.templateContents();
			this.state = 266;
			this.match(jonscriptParser.MORE_THAN);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public templateContents(): TemplateContentsContext {
		let _localctx: TemplateContentsContext = new TemplateContentsContext(this._ctx, this.state);
		this.enterRule(_localctx, 32, jonscriptParser.RULE_templateContents);
		let _la: number;
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 268;
			this.match(jonscriptParser.VAR);
			this.state = 270;
			this._errHandler.sync(this);
			_la = this._input.LA(1);
			if (_la === jonscriptParser.EXTENDS) {
				{
				this.state = 269;
				this.extendsType();
				}
			}

			this.state = 273;
			this._errHandler.sync(this);
			_la = this._input.LA(1);
			if (_la === jonscriptParser.EQUALS) {
				{
				this.state = 272;
				this.equalsType();
				}
			}

			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public extendsType(): ExtendsTypeContext {
		let _localctx: ExtendsTypeContext = new ExtendsTypeContext(this._ctx, this.state);
		this.enterRule(_localctx, 34, jonscriptParser.RULE_extendsType);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 275;
			this.match(jonscriptParser.EXTENDS);
			this.state = 276;
			this.type(0);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public equalsType(): EqualsTypeContext {
		let _localctx: EqualsTypeContext = new EqualsTypeContext(this._ctx, this.state);
		this.enterRule(_localctx, 36, jonscriptParser.RULE_equalsType);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 278;
			this.match(jonscriptParser.EQUALS);
			this.state = 279;
			this.type(0);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}

	public type(): TypeContext;
	public type(_p: number): TypeContext;
	// @RuleVersion(0)
	public type(_p?: number): TypeContext {
		if (_p === undefined) {
			_p = 0;
		}

		let _parentctx: ParserRuleContext = this._ctx;
		let _parentState: number = this.state;
		let _localctx: TypeContext = new TypeContext(this._ctx, _parentState);
		let _prevctx: TypeContext = _localctx;
		let _startState: number = 38;
		this.enterRecursionRule(_localctx, 38, jonscriptParser.RULE_type, _p);
		try {
			let _alt: number;
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 294;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input, 37, this._ctx) ) {
			case 1:
				{
				this.state = 282;
				this.match(jonscriptParser.VAR);
				}
				break;

			case 2:
				{
				this.state = 283;
				this.typeEval();
				}
				break;

			case 3:
				{
				this.state = 284;
				this.tuple();
				}
				break;

			case 4:
				{
				this.state = 287;
				this._errHandler.sync(this);
				switch ( this.interpreter.adaptivePredict(this._input, 36, this._ctx) ) {
				case 1:
					{
					this.state = 285;
					this.match(jonscriptParser.VAR);
					}
					break;

				case 2:
					{
					this.state = 286;
					this.accessType();
					}
					break;
				}
				this.state = 289;
				this.templateType();
				}
				break;

			case 5:
				{
				this.state = 290;
				this.match(jonscriptParser.ATOMIC);
				}
				break;

			case 6:
				{
				this.state = 291;
				this.functionType();
				}
				break;

			case 7:
				{
				this.state = 292;
				this.objectType();
				}
				break;

			case 8:
				{
				this.state = 293;
				this.accessType();
				}
				break;
			}
			this._ctx._stop = this._input.tryLT(-1);
			this.state = 304;
			this._errHandler.sync(this);
			_alt = this.interpreter.adaptivePredict(this._input, 39, this._ctx);
			while (_alt !== 2 && _alt !== ATN.INVALID_ALT_NUMBER) {
				if (_alt === 1) {
					if (this._parseListeners != null) {
						this.triggerExitRuleEvent();
					}
					_prevctx = _localctx;
					{
					this.state = 302;
					this._errHandler.sync(this);
					switch ( this.interpreter.adaptivePredict(this._input, 38, this._ctx) ) {
					case 1:
						{
						_localctx = new TypeContext(_parentctx, _parentState);
						this.pushNewRecursionContext(_localctx, _startState, jonscriptParser.RULE_type);
						this.state = 296;
						if (!(this.precpred(this._ctx, 6))) {
							throw new FailedPredicateException(this, "this.precpred(this._ctx, 6)");
						}
						this.state = 297;
						_localctx._op = this.match(jonscriptParser.TYPE_OPERATORS);
						this.state = 298;
						this.type(7);
						}
						break;

					case 2:
						{
						_localctx = new TypeContext(_parentctx, _parentState);
						this.pushNewRecursionContext(_localctx, _startState, jonscriptParser.RULE_type);
						this.state = 299;
						if (!(this.precpred(this._ctx, 9))) {
							throw new FailedPredicateException(this, "this.precpred(this._ctx, 9)");
						}
						this.state = 300;
						this.match(jonscriptParser.LIST_BEGIN);
						this.state = 301;
						this.match(jonscriptParser.LIST_END);
						}
						break;
					}
					}
				}
				this.state = 306;
				this._errHandler.sync(this);
				_alt = this.interpreter.adaptivePredict(this._input, 39, this._ctx);
			}
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public accessType(): AccessTypeContext {
		let _localctx: AccessTypeContext = new AccessTypeContext(this._ctx, this.state);
		this.enterRule(_localctx, 40, jonscriptParser.RULE_accessType);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 307;
			this.match(jonscriptParser.VAR);
			this.state = 308;
			this.match(jonscriptParser.OBJECT_OPERATORS);
			this.state = 311;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input, 40, this._ctx) ) {
			case 1:
				{
				this.state = 309;
				this.accessType();
				}
				break;

			case 2:
				{
				this.state = 310;
				this.match(jonscriptParser.VAR);
				}
				break;
			}
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public typeEval(): TypeEvalContext {
		let _localctx: TypeEvalContext = new TypeEvalContext(this._ctx, this.state);
		this.enterRule(_localctx, 42, jonscriptParser.RULE_typeEval);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 313;
			this.match(jonscriptParser.EVAL_BEGIN);
			this.state = 314;
			this.type(0);
			this.state = 315;
			this.match(jonscriptParser.EVAL_END);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public tuple(): TupleContext {
		let _localctx: TupleContext = new TupleContext(this._ctx, this.state);
		this.enterRule(_localctx, 44, jonscriptParser.RULE_tuple);
		let _la: number;
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 317;
			this.match(jonscriptParser.LIST_BEGIN);
			this.state = 326;
			this._errHandler.sync(this);
			_la = this._input.LA(1);
			if ((((_la) & ~0x1F) === 0 && ((1 << _la) & ((1 << jonscriptParser.LIST_BEGIN) | (1 << jonscriptParser.EVAL_BEGIN) | (1 << jonscriptParser.OBJECT_BEGIN) | (1 << jonscriptParser.ATOMIC))) !== 0) || _la === jonscriptParser.LESS_THAN || _la === jonscriptParser.VAR) {
				{
				this.state = 318;
				this.type(0);
				this.state = 323;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				while (_la === jonscriptParser.COMMA) {
					{
					{
					this.state = 319;
					this.match(jonscriptParser.COMMA);
					this.state = 320;
					this.type(0);
					}
					}
					this.state = 325;
					this._errHandler.sync(this);
					_la = this._input.LA(1);
				}
				}
			}

			this.state = 328;
			this.match(jonscriptParser.LIST_END);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public templateType(): TemplateTypeContext {
		let _localctx: TemplateTypeContext = new TemplateTypeContext(this._ctx, this.state);
		this.enterRule(_localctx, 46, jonscriptParser.RULE_templateType);
		let _la: number;
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 330;
			this.match(jonscriptParser.LESS_THAN);
			this.state = 331;
			this.type(0);
			this.state = 336;
			this._errHandler.sync(this);
			_la = this._input.LA(1);
			while (_la === jonscriptParser.COMMA) {
				{
				{
				this.state = 332;
				this.match(jonscriptParser.COMMA);
				this.state = 333;
				this.type(0);
				}
				}
				this.state = 338;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
			}
			this.state = 339;
			this.match(jonscriptParser.MORE_THAN);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public paramType(): ParamTypeContext {
		let _localctx: ParamTypeContext = new ParamTypeContext(this._ctx, this.state);
		this.enterRule(_localctx, 48, jonscriptParser.RULE_paramType);
		let _la: number;
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 341;
			this.match(jonscriptParser.VAR);
			this.state = 343;
			this._errHandler.sync(this);
			_la = this._input.LA(1);
			if (_la === jonscriptParser.QMARK) {
				{
				this.state = 342;
				this.match(jonscriptParser.QMARK);
				}
			}

			this.state = 345;
			this.match(jonscriptParser.COLON);
			this.state = 346;
			this.type(0);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public functionType(): FunctionTypeContext {
		let _localctx: FunctionTypeContext = new FunctionTypeContext(this._ctx, this.state);
		this.enterRule(_localctx, 50, jonscriptParser.RULE_functionType);
		let _la: number;
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 349;
			this._errHandler.sync(this);
			_la = this._input.LA(1);
			if (_la === jonscriptParser.LESS_THAN) {
				{
				this.state = 348;
				this.template();
				}
			}

			this.state = 351;
			this.match(jonscriptParser.EVAL_BEGIN);
			this.state = 360;
			this._errHandler.sync(this);
			_la = this._input.LA(1);
			if (_la === jonscriptParser.VAR) {
				{
				this.state = 352;
				this.paramType();
				this.state = 357;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				while (_la === jonscriptParser.COMMA) {
					{
					{
					this.state = 353;
					this.match(jonscriptParser.COMMA);
					this.state = 354;
					this.paramType();
					}
					}
					this.state = 359;
					this._errHandler.sync(this);
					_la = this._input.LA(1);
				}
				}
			}

			this.state = 362;
			this.match(jonscriptParser.EVAL_END);
			this.state = 363;
			this.match(jonscriptParser.ARROW);
			this.state = 364;
			this.type(0);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public objectType(): ObjectTypeContext {
		let _localctx: ObjectTypeContext = new ObjectTypeContext(this._ctx, this.state);
		this.enterRule(_localctx, 52, jonscriptParser.RULE_objectType);
		let _la: number;
		try {
			let _alt: number;
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 366;
			this.match(jonscriptParser.OBJECT_BEGIN);
			this.state = 378;
			this._errHandler.sync(this);
			_la = this._input.LA(1);
			if (_la === jonscriptParser.VAR) {
				{
				this.state = 367;
				this.objectParamType();
				this.state = 372;
				this._errHandler.sync(this);
				_alt = this.interpreter.adaptivePredict(this._input, 48, this._ctx);
				while (_alt !== 2 && _alt !== ATN.INVALID_ALT_NUMBER) {
					if (_alt === 1) {
						{
						{
						this.state = 368;
						this.match(jonscriptParser.COMMA);
						this.state = 369;
						this.objectParamType();
						}
						}
					}
					this.state = 374;
					this._errHandler.sync(this);
					_alt = this.interpreter.adaptivePredict(this._input, 48, this._ctx);
				}
				this.state = 376;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				if (_la === jonscriptParser.COMMA) {
					{
					this.state = 375;
					this.match(jonscriptParser.COMMA);
					}
				}

				}
			}

			this.state = 380;
			this.match(jonscriptParser.OBJECT_END);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public objectParamType(): ObjectParamTypeContext {
		let _localctx: ObjectParamTypeContext = new ObjectParamTypeContext(this._ctx, this.state);
		this.enterRule(_localctx, 54, jonscriptParser.RULE_objectParamType);
		let _la: number;
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 382;
			this.match(jonscriptParser.VAR);
			this.state = 384;
			this._errHandler.sync(this);
			_la = this._input.LA(1);
			if (_la === jonscriptParser.QMARK) {
				{
				this.state = 383;
				this.match(jonscriptParser.QMARK);
				}
			}

			this.state = 386;
			this.match(jonscriptParser.COLON);
			this.state = 387;
			this.type(0);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}

	public expression(): ExpressionContext;
	public expression(_p: number): ExpressionContext;
	// @RuleVersion(0)
	public expression(_p?: number): ExpressionContext {
		if (_p === undefined) {
			_p = 0;
		}

		let _parentctx: ParserRuleContext = this._ctx;
		let _parentState: number = this.state;
		let _localctx: ExpressionContext = new ExpressionContext(this._ctx, _parentState);
		let _prevctx: ExpressionContext = _localctx;
		let _startState: number = 56;
		this.enterRecursionRule(_localctx, 56, jonscriptParser.RULE_expression, _p);
		let _la: number;
		try {
			let _alt: number;
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 422;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input, 56, this._ctx) ) {
			case 1:
				{
				this.state = 390;
				this.match(jonscriptParser.ATOMIC);
				}
				break;

			case 2:
				{
				this.state = 391;
				this.match(jonscriptParser.VAR);
				this.state = 393;
				this._errHandler.sync(this);
				switch ( this.interpreter.adaptivePredict(this._input, 52, this._ctx) ) {
				case 1:
					{
					this.state = 392;
					this.templateType();
					}
					break;
				}
				}
				break;

			case 3:
				{
				this.state = 395;
				this.functor();
				}
				break;

			case 4:
				{
				this.state = 396;
				this.eval();
				}
				break;

			case 5:
				{
				this.state = 397;
				this.match(jonscriptParser.LIST_BEGIN);
				this.state = 399;
				this._errHandler.sync(this);
				_la = this._input.LA(1);
				if ((((_la) & ~0x1F) === 0 && ((1 << _la) & ((1 << jonscriptParser.TRY) | (1 << jonscriptParser.THROW) | (1 << jonscriptParser.INHERITS) | (1 << jonscriptParser.LIST_BEGIN) | (1 << jonscriptParser.EVAL_BEGIN) | (1 << jonscriptParser.OBJECT_BEGIN) | (1 << jonscriptParser.ATOMIC) | (1 << jonscriptParser.INTERPOLATED_SEQUENCE_EMPTY))) !== 0) || ((((_la - 32)) & ~0x1F) === 0 && ((1 << (_la - 32)) & ((1 << (jonscriptParser.INTERPOLATED_SEQUENCE_STRING - 32)) | (1 << (jonscriptParser.INTERPOLATED_SEQUENCE - 32)) | (1 << (jonscriptParser.LESS_THAN - 32)) | (1 << (jonscriptParser.PREFIX_UNARY_OPERATORS - 32)) | (1 << (jonscriptParser.VAR - 32)))) !== 0)) {
					{
					this.state = 398;
					this.expressionList();
					}
				}

				this.state = 401;
				this.match(jonscriptParser.LIST_END);
				}
				break;

			case 6:
				{
				this.state = 402;
				this.match(jonscriptParser.PREFIX_UNARY_OPERATORS);
				this.state = 403;
				this.expression(14);
				}
				break;

			case 7:
				{
				this.state = 404;
				this.match(jonscriptParser.INTERPOLATED_SEQUENCE_EMPTY);
				}
				break;

			case 8:
				{
				this.state = 405;
				this.match(jonscriptParser.INTERPOLATED_SEQUENCE_STRING);
				}
				break;

			case 9:
				{
				this.state = 406;
				this.match(jonscriptParser.INTERPOLATED_SEQUENCE);
				this.state = 408;
				this._errHandler.sync(this);
				switch ( this.interpreter.adaptivePredict(this._input, 54, this._ctx) ) {
				case 1:
					{
					this.state = 407;
					this.interpolated();
					}
					break;
				}
				this.state = 410;
				this.match(jonscriptParser.INTERPOLATED_SEQUENCE);
				}
				break;

			case 10:
				{
				this.state = 411;
				this.match(jonscriptParser.THROW);
				this.state = 412;
				this.expression(4);
				}
				break;

			case 11:
				{
				this.state = 413;
				this.object();
				}
				break;

			case 12:
				{
				this.state = 414;
				this.match(jonscriptParser.TRY);
				this.state = 415;
				this.expression(0);
				this.state = 416;
				this.match(jonscriptParser.CATCH);
				this.state = 417;
				this.expression(0);
				this.state = 420;
				this._errHandler.sync(this);
				switch ( this.interpreter.adaptivePredict(this._input, 55, this._ctx) ) {
				case 1:
					{
					this.state = 418;
					this.match(jonscriptParser.FINALLY);
					this.state = 419;
					this.expression(0);
					}
					break;
				}
				}
				break;
			}
			this._ctx._stop = this._input.tryLT(-1);
			this.state = 467;
			this._errHandler.sync(this);
			_alt = this.interpreter.adaptivePredict(this._input, 59, this._ctx);
			while (_alt !== 2 && _alt !== ATN.INVALID_ALT_NUMBER) {
				if (_alt === 1) {
					if (this._parseListeners != null) {
						this.triggerExitRuleEvent();
					}
					_prevctx = _localctx;
					{
					this.state = 465;
					this._errHandler.sync(this);
					switch ( this.interpreter.adaptivePredict(this._input, 58, this._ctx) ) {
					case 1:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						this.pushNewRecursionContext(_localctx, _startState, jonscriptParser.RULE_expression);
						this.state = 424;
						if (!(this.precpred(this._ctx, 20))) {
							throw new FailedPredicateException(this, "this.precpred(this._ctx, 20)");
						}
						this.state = 425;
						_localctx._obj = this.match(jonscriptParser.OBJECT_OPERATORS);
						this.state = 426;
						this.expression(21);
						}
						break;

					case 2:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						this.pushNewRecursionContext(_localctx, _startState, jonscriptParser.RULE_expression);
						this.state = 427;
						if (!(this.precpred(this._ctx, 10))) {
							throw new FailedPredicateException(this, "this.precpred(this._ctx, 10)");
						}
						this.state = 428;
						_localctx._op = this.match(jonscriptParser.MULTIPLYDIVIDEMOD_OPERATORS);
						this.state = 429;
						this.expression(11);
						}
						break;

					case 3:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						this.pushNewRecursionContext(_localctx, _startState, jonscriptParser.RULE_expression);
						this.state = 430;
						if (!(this.precpred(this._ctx, 9))) {
							throw new FailedPredicateException(this, "this.precpred(this._ctx, 9)");
						}
						this.state = 431;
						_localctx._op = this.match(jonscriptParser.PLUSMINUS_OPERATORS);
						this.state = 432;
						this.expression(10);
						}
						break;

					case 4:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						this.pushNewRecursionContext(_localctx, _startState, jonscriptParser.RULE_expression);
						this.state = 433;
						if (!(this.precpred(this._ctx, 8))) {
							throw new FailedPredicateException(this, "this.precpred(this._ctx, 8)");
						}
						this.state = 434;
						_localctx._cmp = this._input.LT(1);
						_la = this._input.LA(1);
						if (!(((((_la - 39)) & ~0x1F) === 0 && ((1 << (_la - 39)) & ((1 << (jonscriptParser.EQUAL_OR_GREATER - 39)) | (1 << (jonscriptParser.EQUAL_OR_LESS - 39)) | (1 << (jonscriptParser.LESS_THAN - 39)) | (1 << (jonscriptParser.MORE_THAN - 39)))) !== 0))) {
							_localctx._cmp = this._errHandler.recoverInline(this);
						} else {
							if (this._input.LA(1) === Token.EOF) {
								this.matchedEOF = true;
							}

							this._errHandler.reportMatch(this);
							this.consume();
						}
						this.state = 435;
						this.expression(9);
						}
						break;

					case 5:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						this.pushNewRecursionContext(_localctx, _startState, jonscriptParser.RULE_expression);
						this.state = 436;
						if (!(this.precpred(this._ctx, 7))) {
							throw new FailedPredicateException(this, "this.precpred(this._ctx, 7)");
						}
						this.state = 437;
						_localctx._cmp = this._input.LT(1);
						_la = this._input.LA(1);
						if (!(_la === jonscriptParser.EQUALITY || _la === jonscriptParser.NEQUALITY)) {
							_localctx._cmp = this._errHandler.recoverInline(this);
						} else {
							if (this._input.LA(1) === Token.EOF) {
								this.matchedEOF = true;
							}

							this._errHandler.reportMatch(this);
							this.consume();
						}
						this.state = 438;
						this.expression(8);
						}
						break;

					case 6:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						this.pushNewRecursionContext(_localctx, _startState, jonscriptParser.RULE_expression);
						this.state = 439;
						if (!(this.precpred(this._ctx, 6))) {
							throw new FailedPredicateException(this, "this.precpred(this._ctx, 6)");
						}
						this.state = 440;
						_localctx._logic = this.match(jonscriptParser.LOGICAL_OPERATORS);
						this.state = 441;
						this.expression(7);
						}
						break;

					case 7:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						this.pushNewRecursionContext(_localctx, _startState, jonscriptParser.RULE_expression);
						this.state = 442;
						if (!(this.precpred(this._ctx, 5))) {
							throw new FailedPredicateException(this, "this.precpred(this._ctx, 5)");
						}
						this.state = 443;
						this.match(jonscriptParser.IS);
						this.state = 444;
						this.expression(6);
						}
						break;

					case 8:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						this.pushNewRecursionContext(_localctx, _startState, jonscriptParser.RULE_expression);
						this.state = 445;
						if (!(this.precpred(this._ctx, 1))) {
							throw new FailedPredicateException(this, "this.precpred(this._ctx, 1)");
						}
						this.state = 446;
						this.match(jonscriptParser.QMARK);
						this.state = 447;
						this.expression(0);
						this.state = 448;
						this.match(jonscriptParser.COLON);
						this.state = 449;
						this.expression(2);
						}
						break;

					case 9:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						this.pushNewRecursionContext(_localctx, _startState, jonscriptParser.RULE_expression);
						this.state = 451;
						if (!(this.precpred(this._ctx, 19))) {
							throw new FailedPredicateException(this, "this.precpred(this._ctx, 19)");
						}
						this.state = 452;
						_localctx._obj = this.match(jonscriptParser.OBJECT_OPERATORS);
						this.state = 453;
						_la = this._input.LA(1);
						if (!((((_la) & ~0x1F) === 0 && ((1 << _la) & ((1 << jonscriptParser.TRY) | (1 << jonscriptParser.CATCH) | (1 << jonscriptParser.FINALLY) | (1 << jonscriptParser.THROW) | (1 << jonscriptParser.IMPORT) | (1 << jonscriptParser.EXTENDS) | (1 << jonscriptParser.IS) | (1 << jonscriptParser.AS) | (1 << jonscriptParser.FROM))) !== 0))) {
						this._errHandler.recoverInline(this);
						} else {
							if (this._input.LA(1) === Token.EOF) {
								this.matchedEOF = true;
							}

							this._errHandler.reportMatch(this);
							this.consume();
						}
						}
						break;

					case 10:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						this.pushNewRecursionContext(_localctx, _startState, jonscriptParser.RULE_expression);
						this.state = 454;
						if (!(this.precpred(this._ctx, 18))) {
							throw new FailedPredicateException(this, "this.precpred(this._ctx, 18)");
						}
						this.state = 455;
						this.match(jonscriptParser.EVAL_BEGIN);
						this.state = 457;
						this._errHandler.sync(this);
						_la = this._input.LA(1);
						if ((((_la) & ~0x1F) === 0 && ((1 << _la) & ((1 << jonscriptParser.TRY) | (1 << jonscriptParser.THROW) | (1 << jonscriptParser.INHERITS) | (1 << jonscriptParser.LIST_BEGIN) | (1 << jonscriptParser.EVAL_BEGIN) | (1 << jonscriptParser.OBJECT_BEGIN) | (1 << jonscriptParser.ATOMIC) | (1 << jonscriptParser.INTERPOLATED_SEQUENCE_EMPTY))) !== 0) || ((((_la - 32)) & ~0x1F) === 0 && ((1 << (_la - 32)) & ((1 << (jonscriptParser.INTERPOLATED_SEQUENCE_STRING - 32)) | (1 << (jonscriptParser.INTERPOLATED_SEQUENCE - 32)) | (1 << (jonscriptParser.LESS_THAN - 32)) | (1 << (jonscriptParser.PREFIX_UNARY_OPERATORS - 32)) | (1 << (jonscriptParser.VAR - 32)))) !== 0)) {
							{
							this.state = 456;
							this.expressionList();
							}
						}

						this.state = 459;
						this.match(jonscriptParser.EVAL_END);
						}
						break;

					case 11:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						this.pushNewRecursionContext(_localctx, _startState, jonscriptParser.RULE_expression);
						this.state = 460;
						if (!(this.precpred(this._ctx, 17))) {
							throw new FailedPredicateException(this, "this.precpred(this._ctx, 17)");
						}
						this.state = 461;
						this.match(jonscriptParser.LIST_BEGIN);
						this.state = 462;
						this.expression(0);
						this.state = 463;
						this.match(jonscriptParser.LIST_END);
						}
						break;
					}
					}
				}
				this.state = 469;
				this._errHandler.sync(this);
				_alt = this.interpreter.adaptivePredict(this._input, 59, this._ctx);
			}
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public eval(): EvalContext {
		let _localctx: EvalContext = new EvalContext(this._ctx, this.state);
		this.enterRule(_localctx, 58, jonscriptParser.RULE_eval);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 470;
			this.match(jonscriptParser.EVAL_BEGIN);
			this.state = 471;
			this.expression(0);
			this.state = 472;
			this.match(jonscriptParser.EVAL_END);
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public interpolated(): InterpolatedContext {
		let _localctx: InterpolatedContext = new InterpolatedContext(this._ctx, this.state);
		this.enterRule(_localctx, 60, jonscriptParser.RULE_interpolated);
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 476;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input, 60, this._ctx) ) {
			case 1:
				{
				this.state = 474;
				this.match(jonscriptParser.INTERPOLATED_SEQUENCE);
				}
				break;

			case 2:
				{
				this.state = 475;
				this.expression(0);
				}
				break;
			}
			this.state = 479;
			this._errHandler.sync(this);
			switch ( this.interpreter.adaptivePredict(this._input, 61, this._ctx) ) {
			case 1:
				{
				this.state = 478;
				this.interpolated();
				}
				break;
			}
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public expressionList(): ExpressionListContext {
		let _localctx: ExpressionListContext = new ExpressionListContext(this._ctx, this.state);
		this.enterRule(_localctx, 62, jonscriptParser.RULE_expressionList);
		let _la: number;
		try {
			let _alt: number;
			this.enterOuterAlt(_localctx, 1);
			{
			this.state = 481;
			this.expressionItem();
			this.state = 486;
			this._errHandler.sync(this);
			_alt = this.interpreter.adaptivePredict(this._input, 62, this._ctx);
			while (_alt !== 2 && _alt !== ATN.INVALID_ALT_NUMBER) {
				if (_alt === 1) {
					{
					{
					this.state = 482;
					this.match(jonscriptParser.COMMA);
					this.state = 483;
					this.expressionItem();
					}
					}
				}
				this.state = 488;
				this._errHandler.sync(this);
				_alt = this.interpreter.adaptivePredict(this._input, 62, this._ctx);
			}
			this.state = 490;
			this._errHandler.sync(this);
			_la = this._input.LA(1);
			if (_la === jonscriptParser.COMMA) {
				{
				this.state = 489;
				this.match(jonscriptParser.COMMA);
				}
			}

			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}
	// @RuleVersion(0)
	public expressionItem(): ExpressionItemContext {
		let _localctx: ExpressionItemContext = new ExpressionItemContext(this._ctx, this.state);
		this.enterRule(_localctx, 64, jonscriptParser.RULE_expressionItem);
		let _la: number;
		try {
			this.enterOuterAlt(_localctx, 1);
			{
			{
			this.state = 493;
			this._errHandler.sync(this);
			_la = this._input.LA(1);
			if (_la === jonscriptParser.INHERITS) {
				{
				this.state = 492;
				this.match(jonscriptParser.INHERITS);
				}
			}

			this.state = 495;
			this.expression(0);
			}
			}
		}
		catch (re) {
			if (re instanceof RecognitionException) {
				_localctx.exception = re;
				this._errHandler.reportError(this, re);
				this._errHandler.recover(this, re);
			} else {
				throw re;
			}
		}
		finally {
			this.exitRule();
		}
		return _localctx;
	}

	public sempred(_localctx: RuleContext, ruleIndex: number, predIndex: number): boolean {
		switch (ruleIndex) {
		case 19:
			return this.type_sempred(_localctx as TypeContext, predIndex);

		case 28:
			return this.expression_sempred(_localctx as ExpressionContext, predIndex);
		}
		return true;
	}
	private type_sempred(_localctx: TypeContext, predIndex: number): boolean {
		switch (predIndex) {
		case 0:
			return this.precpred(this._ctx, 6);

		case 1:
			return this.precpred(this._ctx, 9);
		}
		return true;
	}
	private expression_sempred(_localctx: ExpressionContext, predIndex: number): boolean {
		switch (predIndex) {
		case 2:
			return this.precpred(this._ctx, 20);

		case 3:
			return this.precpred(this._ctx, 10);

		case 4:
			return this.precpred(this._ctx, 9);

		case 5:
			return this.precpred(this._ctx, 8);

		case 6:
			return this.precpred(this._ctx, 7);

		case 7:
			return this.precpred(this._ctx, 6);

		case 8:
			return this.precpred(this._ctx, 5);

		case 9:
			return this.precpred(this._ctx, 1);

		case 10:
			return this.precpred(this._ctx, 19);

		case 11:
			return this.precpred(this._ctx, 18);

		case 12:
			return this.precpred(this._ctx, 17);
		}
		return true;
	}

	public static readonly _serializedATN: string =
		"\x03\uC91D\uCABA\u058D\uAFBA\u4F53\u0607\uEA8B\uC241\x031\u01F4\x04\x02" +
		"\t\x02\x04\x03\t\x03\x04\x04\t\x04\x04\x05\t\x05\x04\x06\t\x06\x04\x07" +
		"\t\x07\x04\b\t\b\x04\t\t\t\x04\n\t\n\x04\v\t\v\x04\f\t\f\x04\r\t\r\x04" +
		"\x0E\t\x0E\x04\x0F\t\x0F\x04\x10\t\x10\x04\x11\t\x11\x04\x12\t\x12\x04" +
		"\x13\t\x13\x04\x14\t\x14\x04\x15\t\x15\x04\x16\t\x16\x04\x17\t\x17\x04" +
		"\x18\t\x18\x04\x19\t\x19\x04\x1A\t\x1A\x04\x1B\t\x1B\x04\x1C\t\x1C\x04" +
		"\x1D\t\x1D\x04\x1E\t\x1E\x04\x1F\t\x1F\x04 \t \x04!\t!\x04\"\t\"\x03\x02" +
		"\x07\x02F\n\x02\f\x02\x0E\x02I\v\x02\x03\x02\x03\x02\x03\x03\x03\x03\x03" +
		"\x03\x03\x03\x05\x03Q\n\x03\x03\x04\x03\x04\x03\x05\x03\x05\x03\x05\x03" +
		"\x05\x03\x05\x03\x05\x07\x05[\n\x05\f\x05\x0E\x05^\v\x05\x03\x05\x03\x05" +
		"\x03\x05\x05\x05c\n\x05\x03\x05\x03\x05\x03\x05\x03\x06\x03\x06\x03\x06" +
		"\x07\x06k\n\x06\f\x06\x0E\x06n\v\x06\x03\x06\x03\x06\x03\x07\x03\x07\x05" +
		"\x07t\n\x07\x03\x07\x03\x07\x03\x07\x03\x07\x07\x07z\n\x07\f\x07\x0E\x07" +
		"}\v\x07\x03\x07\x05\x07\x80\n\x07\x03\x07\x05\x07\x83\n\x07\x03\x07\x03" +
		"\x07\x03\x07\x03\b\x03\b\x03\b\x03\b\x03\b\x03\b\x05\b\x8E\n\b\x03\b\x03" +
		"\b\x05\b\x92\n\b\x03\t\x03\t\x03\t\x03\t\x03\t\x03\t\x05\t\x9A\n\t\x03" +
		"\t\x03\t\x05\t\x9E\n\t\x03\n\x03\n\x03\n\x03\n\x03\n\x05\n\xA5\n\n\x05" +
		"\n\xA7\n\n\x03\v\x03\v\x03\v\x03\v\x03\v\x03\v\x05\v\xAF\n\v\x03\v\x07" +
		"\v\xB2\n\v\f\v\x0E\v\xB5\v\v\x05\v\xB7\n\v\x03\v\x03\v\x03\f\x03\f\x05" +
		"\f\xBD\n\f\x03\r\x03\r\x03\r\x03\r\x03\x0E\x03\x0E\x07\x0E\xC5\n\x0E\f" +
		"\x0E\x0E\x0E\xC8\v\x0E\x03\x0E\x07\x0E\xCB\n\x0E\f\x0E\x0E\x0E\xCE\v\x0E" +
		"\x03\x0E\x05\x0E\xD1\n\x0E\x03\x0E\x03\x0E\x03\x0F\x03\x0F\x05\x0F\xD7" +
		"\n\x0F\x03\x0F\x03\x0F\x03\x0F\x03\x0F\x07\x0F\xDD\n\x0F\f\x0F\x0E\x0F" +
		"\xE0\v\x0F\x03\x0F\x05\x0F\xE3\n\x0F\x03\x0F\x05\x0F\xE6\n\x0F\x03\x0F" +
		"\x03\x0F\x03\x0F\x05\x0F\xEB\n\x0F\x05\x0F\xED\n\x0F\x03\x0F\x03\x0F\x03" +
		"\x0F\x05\x0F\xF2\n\x0F\x03\x10\x03\x10\x05\x10\xF6\n\x10\x03\x10\x03\x10" +
		"\x05\x10\xFA\n\x10\x03\x10\x03\x10\x05\x10\xFE\n\x10\x03\x10\x05\x10\u0101" +
		"\n\x10\x03\x11\x03\x11\x03\x11\x03\x11\x07\x11\u0107\n\x11\f\x11\x0E\x11" +
		"\u010A\v\x11\x03\x11\x03\x11\x03\x11\x03\x12\x03\x12\x05\x12\u0111\n\x12" +
		"\x03\x12\x05\x12\u0114\n\x12\x03\x13\x03\x13\x03\x13\x03\x14\x03\x14\x03" +
		"\x14\x03\x15\x03\x15\x03\x15\x03\x15\x03\x15\x03\x15\x05\x15\u0122\n\x15" +
		"\x03\x15\x03\x15\x03\x15\x03\x15\x03\x15\x05\x15\u0129\n\x15\x03\x15\x03" +
		"\x15\x03\x15\x03\x15\x03\x15\x03\x15\x07\x15\u0131\n\x15\f\x15\x0E\x15" +
		"\u0134\v\x15\x03\x16\x03\x16\x03\x16\x03\x16\x05\x16\u013A\n\x16\x03\x17" +
		"\x03\x17\x03\x17\x03\x17\x03\x18\x03\x18\x03\x18\x03\x18\x07\x18\u0144" +
		"\n\x18\f\x18\x0E\x18\u0147\v\x18\x05\x18\u0149\n\x18\x03\x18\x03\x18\x03" +
		"\x19\x03\x19\x03\x19\x03\x19\x07\x19\u0151\n\x19\f\x19\x0E\x19\u0154\v" +
		"\x19\x03\x19\x03\x19\x03\x1A\x03\x1A\x05\x1A\u015A\n\x1A\x03\x1A\x03\x1A" +
		"\x03\x1A\x03\x1B\x05\x1B\u0160\n\x1B\x03\x1B\x03\x1B\x03\x1B\x03\x1B\x07" +
		"\x1B\u0166\n\x1B\f\x1B\x0E\x1B\u0169\v\x1B\x05\x1B\u016B\n\x1B\x03\x1B" +
		"\x03\x1B\x03\x1B\x03\x1B\x03\x1C\x03\x1C\x03\x1C\x03\x1C\x07\x1C\u0175" +
		"\n\x1C\f\x1C\x0E\x1C\u0178\v\x1C\x03\x1C\x05\x1C\u017B\n\x1C\x05\x1C\u017D" +
		"\n\x1C\x03\x1C\x03\x1C\x03\x1D\x03\x1D\x05\x1D\u0183\n\x1D\x03\x1D\x03" +
		"\x1D\x03\x1D\x03\x1E\x03\x1E\x03\x1E\x03\x1E\x05\x1E\u018C\n\x1E\x03\x1E" +
		"\x03\x1E\x03\x1E\x03\x1E\x05\x1E\u0192\n\x1E\x03\x1E\x03\x1E\x03\x1E\x03" +
		"\x1E\x03\x1E\x03\x1E\x03\x1E\x05\x1E\u019B\n\x1E\x03\x1E\x03\x1E\x03\x1E" +
		"\x03\x1E\x03\x1E\x03\x1E\x03\x1E\x03\x1E\x03\x1E\x03\x1E\x05\x1E\u01A7" +
		"\n\x1E\x05\x1E\u01A9\n\x1E\x03\x1E\x03\x1E\x03\x1E\x03\x1E\x03\x1E\x03" +
		"\x1E\x03\x1E\x03\x1E\x03\x1E\x03\x1E\x03\x1E\x03\x1E\x03\x1E\x03\x1E\x03" +
		"\x1E\x03\x1E\x03\x1E\x03\x1E\x03\x1E\x03\x1E\x03\x1E\x03\x1E\x03\x1E\x03" +
		"\x1E\x03\x1E\x03\x1E\x03\x1E\x03\x1E\x03\x1E\x03\x1E\x03\x1E\x03\x1E\x03" +
		"\x1E\x05\x1E\u01CC\n\x1E\x03\x1E\x03\x1E\x03\x1E\x03\x1E\x03\x1E\x03\x1E" +
		"\x07\x1E\u01D4\n\x1E\f\x1E\x0E\x1E\u01D7\v\x1E\x03\x1F\x03\x1F\x03\x1F" +
		"\x03\x1F\x03 \x03 \x05 \u01DF\n \x03 \x05 \u01E2\n \x03!\x03!\x03!\x07" +
		"!\u01E7\n!\f!\x0E!\u01EA\v!\x03!\x05!\u01ED\n!\x03\"\x05\"\u01F0\n\"\x03" +
		"\"\x03\"\x03\"\x02\x02\x04(:#\x02\x02\x04\x02\x06\x02\b\x02\n\x02\f\x02" +
		"\x0E\x02\x10\x02\x12\x02\x14\x02\x16\x02\x18\x02\x1A\x02\x1C\x02\x1E\x02" +
		" \x02\"\x02$\x02&\x02(\x02*\x02,\x02.\x020\x022\x024\x026\x028\x02:\x02" +
		"<\x02>\x02@\x02B\x02\x02\x06\x03\x0201\x03\x02),\x03\x02\'(\x04\x02\x03" +
		"\b\n\f\x02\u022D\x02G\x03\x02\x02\x02\x04P\x03\x02\x02\x02\x06R\x03\x02" +
		"\x02\x02\bT\x03\x02\x02\x02\ng\x03\x02\x02\x02\fq\x03\x02\x02\x02\x0E" +
		"\x91\x03\x02\x02\x02\x10\x9D\x03\x02\x02\x02\x12\xA6\x03\x02\x02\x02\x14" +
		"\xA8\x03\x02\x02\x02\x16\xBC\x03\x02\x02\x02\x18\xBE\x03\x02\x02\x02\x1A" +
		"\xC2\x03\x02\x02\x02\x1C\xEC\x03\x02\x02\x02\x1E\u0100\x03\x02\x02\x02" +
		" \u0102\x03\x02\x02\x02\"\u010E\x03\x02\x02\x02$\u0115\x03\x02\x02\x02" +
		"&\u0118\x03\x02\x02\x02(\u0128\x03\x02\x02\x02*\u0135\x03\x02\x02\x02" +
		",\u013B\x03\x02\x02\x02.\u013F\x03\x02\x02\x020\u014C\x03\x02\x02\x02" +
		"2\u0157\x03\x02\x02\x024\u015F\x03\x02\x02\x026\u0170\x03\x02\x02\x02" +
		"8\u0180\x03\x02\x02\x02:\u01A8\x03\x02\x02\x02<\u01D8\x03\x02\x02\x02" +
		">\u01DE\x03\x02\x02\x02@\u01E3\x03\x02\x02\x02B\u01EF\x03\x02\x02\x02" +
		"DF\x05\b\x05\x02ED\x03\x02\x02\x02FI\x03\x02\x02\x02GE\x03\x02\x02\x02" +
		"GH\x03\x02\x02\x02HJ\x03\x02\x02\x02IG\x03\x02\x02\x02JK\x05\n\x06\x02" +
		"K\x03\x03\x02\x02\x02LQ\x071\x02\x02MN\x071\x02\x02NO\x07\v\x02\x02OQ" +
		"\x071\x02\x02PL\x03\x02\x02\x02PM\x03\x02\x02\x02Q\x05\x03\x02\x02\x02" +
		"RS\x071\x02\x02S\x07\x03\x02\x02\x02Tb\x07\x07\x02\x02Uc\x05\x06\x04\x02" +
		"V\\\x07\x15\x02\x02WX\x05\x04\x03\x02XY\x07\x17\x02\x02Y[\x03\x02\x02" +
		"\x02ZW\x03\x02\x02\x02[^\x03\x02\x02\x02\\Z\x03\x02\x02\x02\\]\x03\x02" +
		"\x02\x02]_\x03\x02\x02\x02^\\\x03\x02\x02\x02_`\x05\x04\x03\x02`a\x07" +
		"\x16\x02\x02ac\x03\x02\x02\x02bU\x03\x02\x02\x02bV\x03\x02\x02\x02cd\x03" +
		"\x02\x02\x02de\x07\f\x02\x02ef\x07\x18\x02\x02f\t\x03\x02\x02\x02gh\x07" +
		"1\x02\x02hl\x07\x15\x02\x02ik\x05\f\x07\x02ji\x03\x02\x02\x02kn\x03\x02" +
		"\x02\x02lj\x03\x02\x02\x02lm\x03\x02\x02\x02mo\x03\x02\x02\x02nl\x03\x02" +
		"\x02\x02op\x07\x16\x02\x02p\v\x03\x02\x02\x02qs\x071\x02\x02rt\x05 \x11" +
		"\x02sr\x03\x02\x02\x02st\x03\x02\x02\x02tu\x03\x02\x02\x02u{\x07\x13\x02" +
		"\x02vw\x05\x1E\x10\x02wx\x07\x17\x02\x02xz\x03\x02\x02\x02yv\x03\x02\x02" +
		"\x02z}\x03\x02\x02\x02{y\x03\x02\x02\x02{|\x03\x02\x02\x02|\x82\x03\x02" +
		"\x02\x02}{\x03\x02\x02\x02~\x80\x07\x0E\x02\x02\x7F~\x03\x02\x02\x02\x7F" +
		"\x80\x03\x02\x02\x02\x80\x81\x03\x02\x02\x02\x81\x83\x05\x1E\x10\x02\x82" +
		"\x7F\x03\x02\x02\x02\x82\x83\x03\x02\x02\x02\x83\x84\x03\x02\x02\x02\x84" +
		"\x85\x07\x14\x02\x02\x85\x86\x05\x1A\x0E\x02\x86\r\x03\x02\x02\x02\x87" +
		"\x88\x071\x02\x02\x88\x92\x07\x17\x02\x02\x89\x8A\t\x02\x02\x02\x8A\x8D" +
		"\x07\x10\x02\x02\x8B\x8E\x05\x1A\x0E\x02\x8C\x8E\x05:\x1E\x02\x8D\x8B" +
		"\x03\x02\x02\x02\x8D\x8C\x03\x02\x02\x02\x8E\x8F\x03\x02\x02\x02\x8F\x90" +
		"\x07\x17\x02\x02\x90\x92\x03\x02\x02\x02\x91\x87\x03\x02\x02\x02\x91\x89" +
		"\x03\x02\x02\x02\x92\x0F\x03\x02\x02\x02\x93\x94\x071\x02\x02\x94\x9E" +
		"\x07\x17\x02\x02\x95\x96\x071\x02\x02\x96\x99\x07\x10\x02\x02\x97\x9A" +
		"\x05\x1A\x0E\x02\x98\x9A\x05:\x1E\x02\x99\x97\x03\x02\x02\x02\x99\x98" +
		"\x03\x02\x02\x02\x9A\x9B\x03\x02\x02\x02\x9B\x9C\x07\x17\x02\x02\x9C\x9E" +
		"\x03\x02\x02\x02\x9D\x93\x03\x02\x02\x02\x9D\x95\x03\x02\x02\x02\x9E\x11" +
		"\x03\x02\x02\x02\x9F\xA7\x071\x02\x02\xA0\xA1\x071\x02\x02\xA1\xA4\x07" +
		"\x10\x02\x02\xA2\xA5\x05\x1A\x0E\x02\xA3\xA5\x05:\x1E\x02\xA4\xA2\x03" +
		"\x02\x02\x02\xA4\xA3\x03\x02\x02\x02\xA5\xA7\x03\x02\x02\x02\xA6\x9F\x03" +
		"\x02\x02\x02\xA6\xA0\x03\x02\x02\x02\xA7\x13\x03\x02\x02\x02\xA8\xB6\x07" +
		"\x15\x02\x02\xA9\xB7\x05\x1C\x0F\x02\xAA\xB7\x05\x12\n\x02\xAB\xAC\x05" +
		"\x1C\x0F\x02\xAC\xAD\x07\x17\x02\x02\xAD\xAF\x03\x02\x02\x02\xAE\xAB\x03" +
		"\x02\x02\x02\xAE\xAF\x03\x02\x02\x02\xAF\xB3\x03\x02\x02\x02\xB0\xB2\x05" +
		"\x10\t\x02\xB1\xB0\x03\x02\x02\x02\xB2\xB5\x03\x02\x02\x02\xB3\xB1\x03" +
		"\x02\x02\x02\xB3\xB4\x03\x02\x02\x02\xB4\xB7\x03\x02\x02\x02\xB5\xB3\x03" +
		"\x02\x02\x02\xB6\xA9\x03\x02\x02\x02\xB6\xAA\x03\x02\x02\x02\xB6\xAE\x03" +
		"\x02\x02\x02\xB7\xB8\x03\x02\x02\x02\xB8\xB9\x07\x16\x02\x02\xB9\x15\x03" +
		"\x02\x02\x02\xBA\xBD\x05\x14\v\x02\xBB\xBD\x05:\x1E\x02\xBC\xBA\x03\x02" +
		"\x02\x02\xBC\xBB\x03\x02\x02\x02\xBD\x17\x03\x02\x02\x02\xBE\xBF\x07\x0E" +
		"\x02\x02\xBF\xC0\x05:\x1E\x02\xC0\xC1\x07\x17\x02\x02\xC1\x19\x03\x02" +
		"\x02\x02\xC2\xC6\x07\x15\x02\x02\xC3\xC5\x05\x0E\b\x02\xC4\xC3\x03\x02" +
		"\x02\x02\xC5\xC8\x03\x02\x02\x02\xC6\xC4\x03\x02\x02\x02\xC6\xC7\x03\x02" +
		"\x02\x02\xC7\xCC\x03\x02\x02\x02\xC8\xC6\x03\x02\x02\x02\xC9\xCB\x05\x18" +
		"\r\x02\xCA\xC9\x03\x02\x02\x02\xCB\xCE\x03\x02\x02\x02\xCC\xCA\x03\x02" +
		"\x02\x02\xCC\xCD\x03\x02\x02\x02\xCD\xD0\x03\x02\x02\x02\xCE\xCC\x03\x02" +
		"\x02\x02\xCF\xD1\x05\x16\f\x02\xD0\xCF\x03\x02\x02\x02\xD0\xD1\x03\x02" +
		"\x02\x02\xD1\xD2\x03\x02\x02\x02\xD2\xD3\x07\x16\x02\x02\xD3\x1B\x03\x02" +
		"\x02\x02\xD4\xED\x071\x02\x02\xD5\xD7\x05 \x11\x02\xD6\xD5\x03\x02\x02" +
		"\x02\xD6\xD7\x03\x02\x02\x02\xD7\xD8\x03\x02\x02\x02\xD8\xDE\x07\x13\x02" +
		"\x02\xD9\xDA\x05\x1E\x10\x02\xDA\xDB\x07\x17\x02\x02\xDB\xDD\x03\x02\x02" +
		"\x02\xDC\xD9\x03\x02\x02\x02\xDD\xE0\x03\x02\x02\x02\xDE\xDC\x03\x02\x02" +
		"\x02\xDE\xDF\x03\x02\x02\x02\xDF\xE5\x03\x02\x02\x02\xE0\xDE\x03\x02\x02" +
		"\x02\xE1\xE3\x07\x0E\x02\x02\xE2\xE1\x03\x02\x02\x02\xE2\xE3\x03\x02\x02" +
		"\x02\xE3\xE4\x03\x02\x02\x02\xE4\xE6\x05\x1E\x10\x02\xE5\xE2\x03\x02\x02" +
		"\x02\xE5\xE6\x03\x02\x02\x02\xE6\xE7\x03\x02\x02\x02\xE7\xEA\x07\x14\x02" +
		"\x02\xE8\xE9\x07\x10\x02\x02\xE9\xEB\x05(\x15\x02\xEA\xE8\x03\x02\x02" +
		"\x02\xEA\xEB\x03\x02\x02\x02\xEB\xED\x03\x02\x02\x02\xEC\xD4\x03\x02\x02" +
		"\x02\xEC\xD6\x03\x02\x02\x02\xED\xEE\x03\x02\x02\x02\xEE\xF1\x07\r\x02" +
		"\x02\xEF\xF2\x05\x1A\x0E\x02\xF0\xF2\x05:\x1E\x02\xF1\xEF\x03\x02\x02" +
		"\x02\xF1\xF0\x03\x02\x02\x02\xF2\x1D\x03\x02\x02\x02\xF3\xF5\x071\x02" +
		"\x02\xF4\xF6\x07\x0F\x02\x02\xF5\xF4\x03\x02\x02\x02\xF5\xF6\x03\x02\x02" +
		"\x02\xF6\xF9\x03\x02\x02\x02\xF7\xF8\x07\x10\x02\x02\xF8\xFA\x05(\x15" +
		"\x02\xF9\xF7\x03\x02\x02\x02\xF9\xFA\x03\x02\x02\x02\xFA\xFD\x03\x02\x02" +
		"\x02\xFB\xFC\x07\t\x02\x02\xFC\xFE\x05:\x1E\x02\xFD\xFB\x03\x02\x02\x02" +
		"\xFD\xFE\x03\x02\x02\x02\xFE\u0101\x03\x02\x02\x02\xFF\u0101\x070\x02" +
		"\x02\u0100\xF3\x03\x02\x02\x02\u0100\xFF\x03\x02\x02\x02\u0101\x1F\x03" +
		"\x02\x02\x02\u0102\u0108\x07+\x02\x02\u0103\u0104\x05\"\x12\x02\u0104" +
		"\u0105\x07\x17\x02\x02\u0105\u0107\x03\x02\x02\x02\u0106\u0103\x03\x02" +
		"\x02\x02\u0107\u010A\x03\x02\x02\x02\u0108\u0106\x03\x02\x02\x02\u0108" +
		"\u0109\x03\x02\x02\x02\u0109\u010B\x03\x02\x02\x02\u010A\u0108\x03\x02" +
		"\x02\x02\u010B\u010C\x05\"\x12\x02\u010C\u010D\x07,\x02\x02\u010D!\x03" +
		"\x02\x02\x02\u010E\u0110\x071\x02\x02\u010F\u0111\x05$\x13\x02\u0110\u010F" +
		"\x03\x02\x02\x02\u0110\u0111\x03\x02\x02\x02\u0111\u0113\x03\x02\x02\x02" +
		"\u0112\u0114\x05&\x14\x02\u0113\u0112\x03\x02\x02\x02\u0113\u0114\x03" +
		"\x02\x02\x02\u0114#\x03\x02\x02\x02\u0115\u0116\x07\b\x02\x02\u0116\u0117" +
		"\x05(\x15\x02\u0117%\x03\x02\x02\x02\u0118\u0119\x07\t\x02\x02\u0119\u011A" +
		"\x05(\x15\x02\u011A\'\x03\x02\x02\x02\u011B\u011C\b\x15\x01\x02\u011C" +
		"\u0129\x071\x02\x02\u011D\u0129\x05,\x17\x02\u011E\u0129\x05.\x18\x02" +
		"\u011F\u0122\x071\x02\x02\u0120\u0122\x05*\x16\x02\u0121\u011F\x03\x02" +
		"\x02\x02\u0121\u0120\x03\x02\x02\x02\u0122\u0123\x03\x02\x02\x02\u0123" +
		"\u0129\x050\x19\x02\u0124\u0129\x07\x18\x02\x02\u0125\u0129\x054\x1B\x02" +
		"\u0126\u0129\x056\x1C\x02\u0127\u0129\x05*\x16\x02\u0128\u011B\x03\x02" +
		"\x02\x02\u0128\u011D\x03\x02\x02\x02\u0128\u011E\x03\x02\x02\x02\u0128" +
		"\u0121\x03\x02\x02\x02\u0128\u0124\x03\x02\x02\x02\u0128\u0125\x03\x02" +
		"\x02\x02\u0128\u0126\x03\x02\x02\x02\u0128\u0127\x03\x02\x02\x02\u0129" +
		"\u0132\x03\x02\x02\x02\u012A\u012B\f\b\x02\x02\u012B\u012C\x07-\x02\x02" +
		"\u012C\u0131\x05(\x15\t\u012D\u012E\f\v\x02\x02\u012E\u012F\x07\x11\x02" +
		"\x02\u012F\u0131\x07\x12\x02\x02\u0130\u012A\x03\x02\x02\x02\u0130\u012D" +
		"\x03\x02\x02\x02\u0131\u0134\x03\x02\x02\x02\u0132\u0130\x03\x02\x02\x02" +
		"\u0132\u0133\x03\x02\x02\x02\u0133)\x03\x02\x02\x02\u0134\u0132\x03\x02" +
		"\x02\x02\u0135\u0136\x071\x02\x02\u0136\u0139\x07.\x02\x02\u0137\u013A" +
		"\x05*\x16\x02\u0138\u013A\x071\x02\x02\u0139\u0137\x03\x02\x02\x02\u0139" +
		"\u0138\x03\x02\x02\x02\u013A+\x03\x02\x02\x02\u013B\u013C\x07\x13\x02" +
		"\x02\u013C\u013D\x05(\x15\x02\u013D\u013E\x07\x14\x02\x02\u013E-\x03\x02" +
		"\x02\x02\u013F\u0148\x07\x11\x02\x02\u0140\u0145\x05(\x15\x02\u0141\u0142" +
		"\x07\x17\x02\x02\u0142\u0144\x05(\x15\x02\u0143\u0141\x03\x02\x02\x02" +
		"\u0144\u0147\x03\x02\x02\x02\u0145\u0143\x03\x02\x02\x02\u0145\u0146\x03" +
		"\x02\x02\x02\u0146\u0149\x03\x02\x02\x02\u0147\u0145\x03\x02\x02\x02\u0148" +
		"\u0140\x03\x02\x02\x02\u0148\u0149\x03\x02\x02\x02\u0149\u014A\x03\x02" +
		"\x02\x02\u014A\u014B\x07\x12\x02\x02\u014B/\x03\x02\x02\x02\u014C\u014D" +
		"\x07+\x02\x02\u014D\u0152\x05(\x15\x02\u014E\u014F\x07\x17\x02\x02\u014F" +
		"\u0151\x05(\x15\x02\u0150\u014E\x03\x02\x02\x02\u0151\u0154\x03\x02\x02" +
		"\x02\u0152\u0150\x03\x02\x02\x02\u0152\u0153\x03\x02\x02\x02\u0153\u0155" +
		"\x03\x02\x02\x02\u0154\u0152\x03\x02\x02\x02\u0155\u0156\x07,\x02\x02" +
		"\u01561\x03\x02\x02\x02\u0157\u0159\x071\x02\x02\u0158\u015A\x07\x0F\x02" +
		"\x02\u0159\u0158\x03\x02\x02\x02\u0159\u015A\x03\x02\x02\x02\u015A\u015B" +
		"\x03\x02\x02\x02\u015B\u015C\x07\x10\x02\x02\u015C\u015D\x05(\x15\x02" +
		"\u015D3\x03\x02\x02\x02\u015E\u0160\x05 \x11\x02\u015F\u015E\x03\x02\x02" +
		"\x02\u015F\u0160\x03\x02\x02\x02\u0160\u0161\x03\x02\x02\x02\u0161\u016A" +
		"\x07\x13\x02\x02\u0162\u0167\x052\x1A\x02\u0163\u0164\x07\x17\x02\x02" +
		"\u0164\u0166\x052\x1A\x02\u0165\u0163\x03\x02\x02\x02\u0166\u0169\x03" +
		"\x02\x02\x02\u0167\u0165\x03\x02\x02\x02\u0167\u0168\x03\x02\x02\x02\u0168" +
		"\u016B\x03\x02\x02\x02\u0169\u0167\x03\x02\x02\x02\u016A\u0162\x03\x02" +
		"\x02\x02\u016A\u016B\x03\x02\x02\x02\u016B\u016C\x03\x02\x02\x02\u016C" +
		"\u016D\x07\x14\x02\x02\u016D\u016E\x07\r\x02\x02\u016E\u016F\x05(\x15" +
		"\x02\u016F5\x03\x02\x02\x02\u0170\u017C\x07\x15\x02\x02\u0171\u0176\x05" +
		"8\x1D\x02\u0172\u0173\x07\x17\x02\x02\u0173\u0175\x058\x1D\x02\u0174\u0172" +
		"\x03\x02\x02\x02\u0175\u0178\x03\x02\x02\x02\u0176\u0174\x03\x02\x02\x02" +
		"\u0176\u0177\x03\x02\x02\x02\u0177\u017A\x03\x02\x02\x02\u0178\u0176\x03" +
		"\x02\x02\x02\u0179\u017B\x07\x17\x02\x02\u017A\u0179\x03\x02\x02\x02\u017A" +
		"\u017B\x03\x02\x02\x02\u017B\u017D\x03\x02\x02\x02\u017C\u0171\x03\x02" +
		"\x02\x02\u017C\u017D\x03\x02\x02\x02\u017D\u017E\x03\x02\x02\x02\u017E" +
		"\u017F\x07\x16\x02\x02\u017F7\x03\x02\x02\x02\u0180\u0182\x071\x02\x02" +
		"\u0181\u0183\x07\x0F\x02\x02\u0182\u0181\x03\x02\x02\x02\u0182\u0183\x03" +
		"\x02\x02\x02\u0183\u0184\x03\x02\x02\x02\u0184\u0185\x07\x10\x02\x02\u0185" +
		"\u0186\x05(\x15\x02\u01869\x03\x02\x02\x02\u0187\u0188\b\x1E\x01\x02\u0188" +
		"\u01A9\x07\x18\x02\x02\u0189\u018B\x071\x02\x02\u018A\u018C\x050\x19\x02" +
		"\u018B\u018A\x03\x02\x02\x02\u018B\u018C\x03\x02\x02\x02\u018C\u01A9\x03" +
		"\x02\x02\x02\u018D\u01A9\x05\x1C\x0F\x02\u018E\u01A9\x05<\x1F\x02\u018F" +
		"\u0191\x07\x11\x02\x02\u0190\u0192\x05@!\x02\u0191\u0190\x03\x02\x02\x02" +
		"\u0191\u0192\x03\x02\x02\x02\u0192\u0193\x03\x02\x02\x02\u0193\u01A9\x07" +
		"\x12\x02\x02\u0194\u0195\x07/\x02\x02\u0195\u01A9\x05:\x1E\x10\u0196\u01A9" +
		"\x07!\x02\x02\u0197\u01A9\x07\"\x02\x02\u0198\u019A\x07#\x02\x02\u0199" +
		"\u019B\x05> \x02\u019A\u0199\x03\x02\x02\x02\u019A\u019B\x03\x02\x02\x02" +
		"\u019B\u019C\x03\x02\x02\x02\u019C\u01A9\x07#\x02\x02\u019D\u019E\x07" +
		"\x06\x02\x02\u019E\u01A9\x05:\x1E\x06\u019F\u01A9\x05\x1A\x0E\x02\u01A0" +
		"\u01A1\x07\x03\x02\x02\u01A1\u01A2\x05:\x1E\x02\u01A2\u01A3\x07\x04\x02" +
		"\x02\u01A3\u01A6\x05:\x1E\x02\u01A4\u01A5\x07\x05\x02\x02\u01A5\u01A7" +
		"\x05:\x1E\x02\u01A6\u01A4\x03\x02\x02\x02\u01A6\u01A7\x03\x02\x02\x02" +
		"\u01A7\u01A9\x03\x02\x02\x02\u01A8\u0187\x03\x02\x02\x02\u01A8\u0189\x03" +
		"\x02\x02\x02\u01A8\u018D\x03\x02\x02\x02\u01A8\u018E\x03\x02\x02\x02\u01A8" +
		"\u018F\x03\x02\x02\x02\u01A8\u0194\x03\x02\x02\x02\u01A8\u0196\x03\x02" +
		"\x02\x02\u01A8\u0197\x03\x02\x02\x02\u01A8\u0198\x03\x02\x02\x02\u01A8" +
		"\u019D\x03\x02\x02\x02\u01A8\u019F\x03\x02\x02\x02\u01A8\u01A0\x03\x02" +
		"\x02\x02\u01A9\u01D5\x03\x02\x02\x02\u01AA\u01AB\f\x16\x02\x02\u01AB\u01AC" +
		"\x07.\x02\x02\u01AC\u01D4\x05:\x1E\x17\u01AD\u01AE\f\f\x02\x02\u01AE\u01AF" +
		"\x07%\x02\x02\u01AF\u01D4\x05:\x1E\r\u01B0\u01B1\f\v\x02\x02\u01B1\u01B2" +
		"\x07&\x02\x02\u01B2\u01D4\x05:\x1E\f\u01B3\u01B4\f\n\x02\x02\u01B4\u01B5" +
		"\t\x03\x02\x02\u01B5\u01D4\x05:\x1E\v\u01B6\u01B7\f\t\x02\x02\u01B7\u01B8" +
		"\t\x04\x02\x02\u01B8\u01D4\x05:\x1E\n\u01B9\u01BA\f\b\x02\x02\u01BA\u01BB" +
		"\x07$\x02\x02\u01BB\u01D4\x05:\x1E\t\u01BC\u01BD\f\x07\x02\x02\u01BD\u01BE" +
		"\x07\n\x02\x02\u01BE\u01D4\x05:\x1E\b\u01BF\u01C0\f\x03\x02\x02\u01C0" +
		"\u01C1\x07\x0F\x02\x02\u01C1\u01C2\x05:\x1E\x02\u01C2\u01C3\x07\x10\x02" +
		"\x02\u01C3\u01C4\x05:\x1E\x04\u01C4\u01D4\x03\x02\x02\x02\u01C5\u01C6" +
		"\f\x15\x02\x02\u01C6\u01C7\x07.\x02\x02\u01C7\u01D4\t\x05\x02\x02\u01C8" +
		"\u01C9\f\x14\x02\x02\u01C9\u01CB\x07\x13\x02\x02\u01CA\u01CC\x05@!\x02" +
		"\u01CB\u01CA\x03\x02\x02\x02\u01CB\u01CC\x03\x02\x02\x02\u01CC\u01CD\x03" +
		"\x02\x02\x02\u01CD\u01D4\x07\x14\x02\x02\u01CE\u01CF\f\x13\x02\x02\u01CF" +
		"\u01D0\x07\x11\x02\x02\u01D0\u01D1\x05:\x1E\x02\u01D1\u01D2\x07\x12\x02" +
		"\x02\u01D2\u01D4\x03\x02\x02\x02\u01D3\u01AA\x03\x02\x02\x02\u01D3\u01AD" +
		"\x03\x02\x02\x02\u01D3\u01B0\x03\x02\x02\x02\u01D3\u01B3\x03\x02\x02\x02" +
		"\u01D3\u01B6\x03\x02\x02\x02\u01D3\u01B9\x03\x02\x02\x02\u01D3\u01BC\x03" +
		"\x02\x02\x02\u01D3\u01BF\x03\x02\x02\x02\u01D3\u01C5\x03\x02\x02\x02\u01D3" +
		"\u01C8\x03\x02\x02\x02\u01D3\u01CE\x03\x02\x02\x02\u01D4\u01D7\x03\x02" +
		"\x02\x02\u01D5\u01D3\x03\x02\x02\x02\u01D5\u01D6\x03\x02\x02\x02\u01D6" +
		";\x03\x02\x02\x02\u01D7\u01D5\x03\x02\x02\x02\u01D8\u01D9\x07\x13\x02" +
		"\x02\u01D9\u01DA\x05:\x1E\x02\u01DA\u01DB\x07\x14\x02\x02\u01DB=\x03\x02" +
		"\x02\x02\u01DC\u01DF\x07#\x02\x02\u01DD\u01DF\x05:\x1E\x02\u01DE\u01DC" +
		"\x03\x02\x02\x02\u01DE\u01DD\x03\x02\x02\x02\u01DF\u01E1\x03\x02\x02\x02" +
		"\u01E0\u01E2\x05> \x02\u01E1\u01E0\x03\x02\x02\x02\u01E1\u01E2\x03\x02" +
		"\x02\x02\u01E2?\x03\x02\x02\x02\u01E3\u01E8\x05B\"\x02\u01E4\u01E5\x07" +
		"\x17\x02\x02\u01E5\u01E7\x05B\"\x02\u01E6\u01E4\x03\x02\x02\x02\u01E7" +
		"\u01EA\x03\x02\x02\x02\u01E8\u01E6\x03\x02\x02\x02\u01E8\u01E9\x03\x02" +
		"\x02\x02\u01E9\u01EC\x03\x02\x02\x02\u01EA\u01E8\x03\x02\x02\x02\u01EB" +
		"\u01ED\x07\x17\x02\x02\u01EC\u01EB\x03\x02\x02\x02\u01EC\u01ED\x03\x02" +
		"\x02\x02\u01EDA\x03\x02\x02\x02\u01EE\u01F0\x07\x0E\x02\x02\u01EF\u01EE" +
		"\x03\x02\x02\x02\u01EF\u01F0\x03\x02\x02\x02\u01F0\u01F1\x03\x02\x02\x02" +
		"\u01F1\u01F2\x05:\x1E\x02\u01F2C\x03\x02\x02\x02CGP\\bls{\x7F\x82\x8D" +
		"\x91\x99\x9D\xA4\xA6\xAE\xB3\xB6\xBC\xC6\xCC\xD0\xD6\xDE\xE2\xE5\xEA\xEC" +
		"\xF1\xF5\xF9\xFD\u0100\u0108\u0110\u0113\u0121\u0128\u0130\u0132\u0139" +
		"\u0145\u0148\u0152\u0159\u015F\u0167\u016A\u0176\u017A\u017C\u0182\u018B" +
		"\u0191\u019A\u01A6\u01A8\u01CB\u01D3\u01D5\u01DE\u01E1\u01E8\u01EC\u01EF";
	public static __ATN: ATN;
	public static get _ATN(): ATN {
		if (!jonscriptParser.__ATN) {
			jonscriptParser.__ATN = new ATNDeserializer().deserialize(Utils.toCharArray(jonscriptParser._serializedATN));
		}

		return jonscriptParser.__ATN;
	}

}

export class ParentContext extends ParserRuleContext {
	public module(): ModuleContext {
		return this.getRuleContext(0, ModuleContext);
	}
	public imported(): ImportedContext[];
	public imported(i: number): ImportedContext;
	public imported(i?: number): ImportedContext | ImportedContext[] {
		if (i === undefined) {
			return this.getRuleContexts(ImportedContext);
		} else {
			return this.getRuleContext(i, ImportedContext);
		}
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_parent; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterParent) {
			listener.enterParent(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitParent) {
			listener.exitParent(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitParent) {
			return visitor.visitParent(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class ImportVarContext extends ParserRuleContext {
	public VAR(): TerminalNode[];
	public VAR(i: number): TerminalNode;
	public VAR(i?: number): TerminalNode | TerminalNode[] {
		if (i === undefined) {
			return this.getTokens(jonscriptParser.VAR);
		} else {
			return this.getToken(jonscriptParser.VAR, i);
		}
	}
	public AS(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.AS, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_importVar; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterImportVar) {
			listener.enterImportVar(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitImportVar) {
			listener.exitImportVar(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitImportVar) {
			return visitor.visitImportVar(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class DefaultImportContext extends ParserRuleContext {
	public VAR(): TerminalNode { return this.getToken(jonscriptParser.VAR, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_defaultImport; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterDefaultImport) {
			listener.enterDefaultImport(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitDefaultImport) {
			listener.exitDefaultImport(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitDefaultImport) {
			return visitor.visitDefaultImport(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class ImportedContext extends ParserRuleContext {
	public IMPORT(): TerminalNode { return this.getToken(jonscriptParser.IMPORT, 0); }
	public FROM(): TerminalNode { return this.getToken(jonscriptParser.FROM, 0); }
	public ATOMIC(): TerminalNode { return this.getToken(jonscriptParser.ATOMIC, 0); }
	public defaultImport(): DefaultImportContext | undefined {
		return this.tryGetRuleContext(0, DefaultImportContext);
	}
	public OBJECT_BEGIN(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.OBJECT_BEGIN, 0); }
	public importVar(): ImportVarContext[];
	public importVar(i: number): ImportVarContext;
	public importVar(i?: number): ImportVarContext | ImportVarContext[] {
		if (i === undefined) {
			return this.getRuleContexts(ImportVarContext);
		} else {
			return this.getRuleContext(i, ImportVarContext);
		}
	}
	public OBJECT_END(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.OBJECT_END, 0); }
	public COMMA(): TerminalNode[];
	public COMMA(i: number): TerminalNode;
	public COMMA(i?: number): TerminalNode | TerminalNode[] {
		if (i === undefined) {
			return this.getTokens(jonscriptParser.COMMA);
		} else {
			return this.getToken(jonscriptParser.COMMA, i);
		}
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_imported; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterImported) {
			listener.enterImported(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitImported) {
			listener.exitImported(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitImported) {
			return visitor.visitImported(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class ModuleContext extends ParserRuleContext {
	public VAR(): TerminalNode { return this.getToken(jonscriptParser.VAR, 0); }
	public OBJECT_BEGIN(): TerminalNode { return this.getToken(jonscriptParser.OBJECT_BEGIN, 0); }
	public OBJECT_END(): TerminalNode { return this.getToken(jonscriptParser.OBJECT_END, 0); }
	public classdef(): ClassdefContext[];
	public classdef(i: number): ClassdefContext;
	public classdef(i?: number): ClassdefContext | ClassdefContext[] {
		if (i === undefined) {
			return this.getRuleContexts(ClassdefContext);
		} else {
			return this.getRuleContext(i, ClassdefContext);
		}
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_module; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterModule) {
			listener.enterModule(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitModule) {
			listener.exitModule(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitModule) {
			return visitor.visitModule(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class ClassdefContext extends ParserRuleContext {
	public VAR(): TerminalNode { return this.getToken(jonscriptParser.VAR, 0); }
	public EVAL_BEGIN(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.EVAL_BEGIN, 0); }
	public EVAL_END(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.EVAL_END, 0); }
	public object(): ObjectContext | undefined {
		return this.tryGetRuleContext(0, ObjectContext);
	}
	public template(): TemplateContext | undefined {
		return this.tryGetRuleContext(0, TemplateContext);
	}
	public ignoreableVar(): IgnoreableVarContext[];
	public ignoreableVar(i: number): IgnoreableVarContext;
	public ignoreableVar(i?: number): IgnoreableVarContext | IgnoreableVarContext[] {
		if (i === undefined) {
			return this.getRuleContexts(IgnoreableVarContext);
		} else {
			return this.getRuleContext(i, IgnoreableVarContext);
		}
	}
	public COMMA(): TerminalNode[];
	public COMMA(i: number): TerminalNode;
	public COMMA(i?: number): TerminalNode | TerminalNode[] {
		if (i === undefined) {
			return this.getTokens(jonscriptParser.COMMA);
		} else {
			return this.getToken(jonscriptParser.COMMA, i);
		}
	}
	public INHERITS(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.INHERITS, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_classdef; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterClassdef) {
			listener.enterClassdef(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitClassdef) {
			listener.exitClassdef(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitClassdef) {
			return visitor.visitClassdef(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class AssignmentContext extends ParserRuleContext {
	public VAR(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.VAR, 0); }
	public COMMA(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.COMMA, 0); }
	public COLON(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.COLON, 0); }
	public IGNORE(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.IGNORE, 0); }
	public object(): ObjectContext | undefined {
		return this.tryGetRuleContext(0, ObjectContext);
	}
	public expression(): ExpressionContext | undefined {
		return this.tryGetRuleContext(0, ExpressionContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_assignment; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterAssignment) {
			listener.enterAssignment(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitAssignment) {
			listener.exitAssignment(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitAssignment) {
			return visitor.visitAssignment(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class VarAssignmentContext extends ParserRuleContext {
	public VAR(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.VAR, 0); }
	public COMMA(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.COMMA, 0); }
	public COLON(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.COLON, 0); }
	public object(): ObjectContext | undefined {
		return this.tryGetRuleContext(0, ObjectContext);
	}
	public expression(): ExpressionContext | undefined {
		return this.tryGetRuleContext(0, ExpressionContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_varAssignment; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterVarAssignment) {
			listener.enterVarAssignment(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitVarAssignment) {
			listener.exitVarAssignment(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitVarAssignment) {
			return visitor.visitVarAssignment(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class SingleVarAssignmentContext extends ParserRuleContext {
	public VAR(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.VAR, 0); }
	public COLON(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.COLON, 0); }
	public object(): ObjectContext | undefined {
		return this.tryGetRuleContext(0, ObjectContext);
	}
	public expression(): ExpressionContext | undefined {
		return this.tryGetRuleContext(0, ExpressionContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_singleVarAssignment; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterSingleVarAssignment) {
			listener.enterSingleVarAssignment(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitSingleVarAssignment) {
			listener.exitSingleVarAssignment(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitSingleVarAssignment) {
			return visitor.visitSingleVarAssignment(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class PublicObjectContext extends ParserRuleContext {
	public OBJECT_BEGIN(): TerminalNode { return this.getToken(jonscriptParser.OBJECT_BEGIN, 0); }
	public OBJECT_END(): TerminalNode { return this.getToken(jonscriptParser.OBJECT_END, 0); }
	public functor(): FunctorContext | undefined {
		return this.tryGetRuleContext(0, FunctorContext);
	}
	public singleVarAssignment(): SingleVarAssignmentContext | undefined {
		return this.tryGetRuleContext(0, SingleVarAssignmentContext);
	}
	public COMMA(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.COMMA, 0); }
	public varAssignment(): VarAssignmentContext[];
	public varAssignment(i: number): VarAssignmentContext;
	public varAssignment(i?: number): VarAssignmentContext | VarAssignmentContext[] {
		if (i === undefined) {
			return this.getRuleContexts(VarAssignmentContext);
		} else {
			return this.getRuleContext(i, VarAssignmentContext);
		}
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_publicObject; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterPublicObject) {
			listener.enterPublicObject(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitPublicObject) {
			listener.exitPublicObject(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitPublicObject) {
			return visitor.visitPublicObject(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class PublicApiContext extends ParserRuleContext {
	public publicObject(): PublicObjectContext | undefined {
		return this.tryGetRuleContext(0, PublicObjectContext);
	}
	public expression(): ExpressionContext | undefined {
		return this.tryGetRuleContext(0, ExpressionContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_publicApi; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterPublicApi) {
			listener.enterPublicApi(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitPublicApi) {
			listener.exitPublicApi(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitPublicApi) {
			return visitor.visitPublicApi(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class InheritanceContext extends ParserRuleContext {
	public INHERITS(): TerminalNode { return this.getToken(jonscriptParser.INHERITS, 0); }
	public expression(): ExpressionContext {
		return this.getRuleContext(0, ExpressionContext);
	}
	public COMMA(): TerminalNode { return this.getToken(jonscriptParser.COMMA, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_inheritance; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterInheritance) {
			listener.enterInheritance(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitInheritance) {
			listener.exitInheritance(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitInheritance) {
			return visitor.visitInheritance(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class ObjectContext extends ParserRuleContext {
	public OBJECT_BEGIN(): TerminalNode { return this.getToken(jonscriptParser.OBJECT_BEGIN, 0); }
	public OBJECT_END(): TerminalNode { return this.getToken(jonscriptParser.OBJECT_END, 0); }
	public assignment(): AssignmentContext[];
	public assignment(i: number): AssignmentContext;
	public assignment(i?: number): AssignmentContext | AssignmentContext[] {
		if (i === undefined) {
			return this.getRuleContexts(AssignmentContext);
		} else {
			return this.getRuleContext(i, AssignmentContext);
		}
	}
	public inheritance(): InheritanceContext[];
	public inheritance(i: number): InheritanceContext;
	public inheritance(i?: number): InheritanceContext | InheritanceContext[] {
		if (i === undefined) {
			return this.getRuleContexts(InheritanceContext);
		} else {
			return this.getRuleContext(i, InheritanceContext);
		}
	}
	public publicApi(): PublicApiContext | undefined {
		return this.tryGetRuleContext(0, PublicApiContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_object; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterObject) {
			listener.enterObject(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitObject) {
			listener.exitObject(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitObject) {
			return visitor.visitObject(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class FunctorContext extends ParserRuleContext {
	public ARROW(): TerminalNode { return this.getToken(jonscriptParser.ARROW, 0); }
	public VAR(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.VAR, 0); }
	public object(): ObjectContext | undefined {
		return this.tryGetRuleContext(0, ObjectContext);
	}
	public expression(): ExpressionContext | undefined {
		return this.tryGetRuleContext(0, ExpressionContext);
	}
	public EVAL_BEGIN(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.EVAL_BEGIN, 0); }
	public EVAL_END(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.EVAL_END, 0); }
	public template(): TemplateContext | undefined {
		return this.tryGetRuleContext(0, TemplateContext);
	}
	public ignoreableVar(): IgnoreableVarContext[];
	public ignoreableVar(i: number): IgnoreableVarContext;
	public ignoreableVar(i?: number): IgnoreableVarContext | IgnoreableVarContext[] {
		if (i === undefined) {
			return this.getRuleContexts(IgnoreableVarContext);
		} else {
			return this.getRuleContext(i, IgnoreableVarContext);
		}
	}
	public COMMA(): TerminalNode[];
	public COMMA(i: number): TerminalNode;
	public COMMA(i?: number): TerminalNode | TerminalNode[] {
		if (i === undefined) {
			return this.getTokens(jonscriptParser.COMMA);
		} else {
			return this.getToken(jonscriptParser.COMMA, i);
		}
	}
	public COLON(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.COLON, 0); }
	public type(): TypeContext | undefined {
		return this.tryGetRuleContext(0, TypeContext);
	}
	public INHERITS(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.INHERITS, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_functor; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterFunctor) {
			listener.enterFunctor(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitFunctor) {
			listener.exitFunctor(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitFunctor) {
			return visitor.visitFunctor(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class IgnoreableVarContext extends ParserRuleContext {
	public VAR(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.VAR, 0); }
	public QMARK(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.QMARK, 0); }
	public COLON(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.COLON, 0); }
	public type(): TypeContext | undefined {
		return this.tryGetRuleContext(0, TypeContext);
	}
	public EQUALS(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.EQUALS, 0); }
	public expression(): ExpressionContext | undefined {
		return this.tryGetRuleContext(0, ExpressionContext);
	}
	public IGNORE(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.IGNORE, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_ignoreableVar; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterIgnoreableVar) {
			listener.enterIgnoreableVar(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitIgnoreableVar) {
			listener.exitIgnoreableVar(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitIgnoreableVar) {
			return visitor.visitIgnoreableVar(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class TemplateContext extends ParserRuleContext {
	public LESS_THAN(): TerminalNode { return this.getToken(jonscriptParser.LESS_THAN, 0); }
	public templateContents(): TemplateContentsContext[];
	public templateContents(i: number): TemplateContentsContext;
	public templateContents(i?: number): TemplateContentsContext | TemplateContentsContext[] {
		if (i === undefined) {
			return this.getRuleContexts(TemplateContentsContext);
		} else {
			return this.getRuleContext(i, TemplateContentsContext);
		}
	}
	public MORE_THAN(): TerminalNode { return this.getToken(jonscriptParser.MORE_THAN, 0); }
	public COMMA(): TerminalNode[];
	public COMMA(i: number): TerminalNode;
	public COMMA(i?: number): TerminalNode | TerminalNode[] {
		if (i === undefined) {
			return this.getTokens(jonscriptParser.COMMA);
		} else {
			return this.getToken(jonscriptParser.COMMA, i);
		}
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_template; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterTemplate) {
			listener.enterTemplate(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitTemplate) {
			listener.exitTemplate(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitTemplate) {
			return visitor.visitTemplate(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class TemplateContentsContext extends ParserRuleContext {
	public VAR(): TerminalNode { return this.getToken(jonscriptParser.VAR, 0); }
	public extendsType(): ExtendsTypeContext | undefined {
		return this.tryGetRuleContext(0, ExtendsTypeContext);
	}
	public equalsType(): EqualsTypeContext | undefined {
		return this.tryGetRuleContext(0, EqualsTypeContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_templateContents; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterTemplateContents) {
			listener.enterTemplateContents(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitTemplateContents) {
			listener.exitTemplateContents(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitTemplateContents) {
			return visitor.visitTemplateContents(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class ExtendsTypeContext extends ParserRuleContext {
	public EXTENDS(): TerminalNode { return this.getToken(jonscriptParser.EXTENDS, 0); }
	public type(): TypeContext {
		return this.getRuleContext(0, TypeContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_extendsType; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterExtendsType) {
			listener.enterExtendsType(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitExtendsType) {
			listener.exitExtendsType(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitExtendsType) {
			return visitor.visitExtendsType(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class EqualsTypeContext extends ParserRuleContext {
	public EQUALS(): TerminalNode { return this.getToken(jonscriptParser.EQUALS, 0); }
	public type(): TypeContext {
		return this.getRuleContext(0, TypeContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_equalsType; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterEqualsType) {
			listener.enterEqualsType(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitEqualsType) {
			listener.exitEqualsType(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitEqualsType) {
			return visitor.visitEqualsType(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class TypeContext extends ParserRuleContext {
	public _op: Token;
	public VAR(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.VAR, 0); }
	public type(): TypeContext[];
	public type(i: number): TypeContext;
	public type(i?: number): TypeContext | TypeContext[] {
		if (i === undefined) {
			return this.getRuleContexts(TypeContext);
		} else {
			return this.getRuleContext(i, TypeContext);
		}
	}
	public LIST_BEGIN(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.LIST_BEGIN, 0); }
	public LIST_END(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.LIST_END, 0); }
	public typeEval(): TypeEvalContext | undefined {
		return this.tryGetRuleContext(0, TypeEvalContext);
	}
	public tuple(): TupleContext | undefined {
		return this.tryGetRuleContext(0, TupleContext);
	}
	public TYPE_OPERATORS(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.TYPE_OPERATORS, 0); }
	public templateType(): TemplateTypeContext | undefined {
		return this.tryGetRuleContext(0, TemplateTypeContext);
	}
	public accessType(): AccessTypeContext | undefined {
		return this.tryGetRuleContext(0, AccessTypeContext);
	}
	public ATOMIC(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.ATOMIC, 0); }
	public functionType(): FunctionTypeContext | undefined {
		return this.tryGetRuleContext(0, FunctionTypeContext);
	}
	public objectType(): ObjectTypeContext | undefined {
		return this.tryGetRuleContext(0, ObjectTypeContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_type; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterType) {
			listener.enterType(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitType) {
			listener.exitType(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitType) {
			return visitor.visitType(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class AccessTypeContext extends ParserRuleContext {
	public VAR(): TerminalNode[];
	public VAR(i: number): TerminalNode;
	public VAR(i?: number): TerminalNode | TerminalNode[] {
		if (i === undefined) {
			return this.getTokens(jonscriptParser.VAR);
		} else {
			return this.getToken(jonscriptParser.VAR, i);
		}
	}
	public OBJECT_OPERATORS(): TerminalNode { return this.getToken(jonscriptParser.OBJECT_OPERATORS, 0); }
	public accessType(): AccessTypeContext | undefined {
		return this.tryGetRuleContext(0, AccessTypeContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_accessType; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterAccessType) {
			listener.enterAccessType(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitAccessType) {
			listener.exitAccessType(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitAccessType) {
			return visitor.visitAccessType(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class TypeEvalContext extends ParserRuleContext {
	public EVAL_BEGIN(): TerminalNode { return this.getToken(jonscriptParser.EVAL_BEGIN, 0); }
	public type(): TypeContext {
		return this.getRuleContext(0, TypeContext);
	}
	public EVAL_END(): TerminalNode { return this.getToken(jonscriptParser.EVAL_END, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_typeEval; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterTypeEval) {
			listener.enterTypeEval(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitTypeEval) {
			listener.exitTypeEval(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitTypeEval) {
			return visitor.visitTypeEval(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class TupleContext extends ParserRuleContext {
	public LIST_BEGIN(): TerminalNode { return this.getToken(jonscriptParser.LIST_BEGIN, 0); }
	public LIST_END(): TerminalNode { return this.getToken(jonscriptParser.LIST_END, 0); }
	public type(): TypeContext[];
	public type(i: number): TypeContext;
	public type(i?: number): TypeContext | TypeContext[] {
		if (i === undefined) {
			return this.getRuleContexts(TypeContext);
		} else {
			return this.getRuleContext(i, TypeContext);
		}
	}
	public COMMA(): TerminalNode[];
	public COMMA(i: number): TerminalNode;
	public COMMA(i?: number): TerminalNode | TerminalNode[] {
		if (i === undefined) {
			return this.getTokens(jonscriptParser.COMMA);
		} else {
			return this.getToken(jonscriptParser.COMMA, i);
		}
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_tuple; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterTuple) {
			listener.enterTuple(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitTuple) {
			listener.exitTuple(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitTuple) {
			return visitor.visitTuple(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class TemplateTypeContext extends ParserRuleContext {
	public LESS_THAN(): TerminalNode { return this.getToken(jonscriptParser.LESS_THAN, 0); }
	public type(): TypeContext[];
	public type(i: number): TypeContext;
	public type(i?: number): TypeContext | TypeContext[] {
		if (i === undefined) {
			return this.getRuleContexts(TypeContext);
		} else {
			return this.getRuleContext(i, TypeContext);
		}
	}
	public MORE_THAN(): TerminalNode { return this.getToken(jonscriptParser.MORE_THAN, 0); }
	public COMMA(): TerminalNode[];
	public COMMA(i: number): TerminalNode;
	public COMMA(i?: number): TerminalNode | TerminalNode[] {
		if (i === undefined) {
			return this.getTokens(jonscriptParser.COMMA);
		} else {
			return this.getToken(jonscriptParser.COMMA, i);
		}
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_templateType; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterTemplateType) {
			listener.enterTemplateType(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitTemplateType) {
			listener.exitTemplateType(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitTemplateType) {
			return visitor.visitTemplateType(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class ParamTypeContext extends ParserRuleContext {
	public VAR(): TerminalNode { return this.getToken(jonscriptParser.VAR, 0); }
	public COLON(): TerminalNode { return this.getToken(jonscriptParser.COLON, 0); }
	public type(): TypeContext {
		return this.getRuleContext(0, TypeContext);
	}
	public QMARK(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.QMARK, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_paramType; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterParamType) {
			listener.enterParamType(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitParamType) {
			listener.exitParamType(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitParamType) {
			return visitor.visitParamType(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class FunctionTypeContext extends ParserRuleContext {
	public EVAL_BEGIN(): TerminalNode { return this.getToken(jonscriptParser.EVAL_BEGIN, 0); }
	public EVAL_END(): TerminalNode { return this.getToken(jonscriptParser.EVAL_END, 0); }
	public ARROW(): TerminalNode { return this.getToken(jonscriptParser.ARROW, 0); }
	public type(): TypeContext {
		return this.getRuleContext(0, TypeContext);
	}
	public template(): TemplateContext | undefined {
		return this.tryGetRuleContext(0, TemplateContext);
	}
	public paramType(): ParamTypeContext[];
	public paramType(i: number): ParamTypeContext;
	public paramType(i?: number): ParamTypeContext | ParamTypeContext[] {
		if (i === undefined) {
			return this.getRuleContexts(ParamTypeContext);
		} else {
			return this.getRuleContext(i, ParamTypeContext);
		}
	}
	public COMMA(): TerminalNode[];
	public COMMA(i: number): TerminalNode;
	public COMMA(i?: number): TerminalNode | TerminalNode[] {
		if (i === undefined) {
			return this.getTokens(jonscriptParser.COMMA);
		} else {
			return this.getToken(jonscriptParser.COMMA, i);
		}
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_functionType; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterFunctionType) {
			listener.enterFunctionType(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitFunctionType) {
			listener.exitFunctionType(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitFunctionType) {
			return visitor.visitFunctionType(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class ObjectTypeContext extends ParserRuleContext {
	public OBJECT_BEGIN(): TerminalNode { return this.getToken(jonscriptParser.OBJECT_BEGIN, 0); }
	public OBJECT_END(): TerminalNode { return this.getToken(jonscriptParser.OBJECT_END, 0); }
	public objectParamType(): ObjectParamTypeContext[];
	public objectParamType(i: number): ObjectParamTypeContext;
	public objectParamType(i?: number): ObjectParamTypeContext | ObjectParamTypeContext[] {
		if (i === undefined) {
			return this.getRuleContexts(ObjectParamTypeContext);
		} else {
			return this.getRuleContext(i, ObjectParamTypeContext);
		}
	}
	public COMMA(): TerminalNode[];
	public COMMA(i: number): TerminalNode;
	public COMMA(i?: number): TerminalNode | TerminalNode[] {
		if (i === undefined) {
			return this.getTokens(jonscriptParser.COMMA);
		} else {
			return this.getToken(jonscriptParser.COMMA, i);
		}
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_objectType; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterObjectType) {
			listener.enterObjectType(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitObjectType) {
			listener.exitObjectType(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitObjectType) {
			return visitor.visitObjectType(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class ObjectParamTypeContext extends ParserRuleContext {
	public VAR(): TerminalNode { return this.getToken(jonscriptParser.VAR, 0); }
	public COLON(): TerminalNode { return this.getToken(jonscriptParser.COLON, 0); }
	public type(): TypeContext {
		return this.getRuleContext(0, TypeContext);
	}
	public QMARK(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.QMARK, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_objectParamType; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterObjectParamType) {
			listener.enterObjectParamType(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitObjectParamType) {
			listener.exitObjectParamType(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitObjectParamType) {
			return visitor.visitObjectParamType(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class ExpressionContext extends ParserRuleContext {
	public _obj: Token;
	public _op: Token;
	public _cmp: Token;
	public _logic: Token;
	public ATOMIC(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.ATOMIC, 0); }
	public VAR(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.VAR, 0); }
	public templateType(): TemplateTypeContext | undefined {
		return this.tryGetRuleContext(0, TemplateTypeContext);
	}
	public functor(): FunctorContext | undefined {
		return this.tryGetRuleContext(0, FunctorContext);
	}
	public expression(): ExpressionContext[];
	public expression(i: number): ExpressionContext;
	public expression(i?: number): ExpressionContext | ExpressionContext[] {
		if (i === undefined) {
			return this.getRuleContexts(ExpressionContext);
		} else {
			return this.getRuleContext(i, ExpressionContext);
		}
	}
	public OBJECT_OPERATORS(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.OBJECT_OPERATORS, 0); }
	public THROW(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.THROW, 0); }
	public IMPORT(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.IMPORT, 0); }
	public AS(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.AS, 0); }
	public IS(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.IS, 0); }
	public TRY(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.TRY, 0); }
	public CATCH(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.CATCH, 0); }
	public FINALLY(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.FINALLY, 0); }
	public EXTENDS(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.EXTENDS, 0); }
	public FROM(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.FROM, 0); }
	public EVAL_BEGIN(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.EVAL_BEGIN, 0); }
	public EVAL_END(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.EVAL_END, 0); }
	public expressionList(): ExpressionListContext | undefined {
		return this.tryGetRuleContext(0, ExpressionListContext);
	}
	public LIST_BEGIN(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.LIST_BEGIN, 0); }
	public LIST_END(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.LIST_END, 0); }
	public eval(): EvalContext | undefined {
		return this.tryGetRuleContext(0, EvalContext);
	}
	public PREFIX_UNARY_OPERATORS(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.PREFIX_UNARY_OPERATORS, 0); }
	public INTERPOLATED_SEQUENCE_EMPTY(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.INTERPOLATED_SEQUENCE_EMPTY, 0); }
	public INTERPOLATED_SEQUENCE_STRING(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.INTERPOLATED_SEQUENCE_STRING, 0); }
	public INTERPOLATED_SEQUENCE(): TerminalNode[];
	public INTERPOLATED_SEQUENCE(i: number): TerminalNode;
	public INTERPOLATED_SEQUENCE(i?: number): TerminalNode | TerminalNode[] {
		if (i === undefined) {
			return this.getTokens(jonscriptParser.INTERPOLATED_SEQUENCE);
		} else {
			return this.getToken(jonscriptParser.INTERPOLATED_SEQUENCE, i);
		}
	}
	public interpolated(): InterpolatedContext | undefined {
		return this.tryGetRuleContext(0, InterpolatedContext);
	}
	public MULTIPLYDIVIDEMOD_OPERATORS(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.MULTIPLYDIVIDEMOD_OPERATORS, 0); }
	public PLUSMINUS_OPERATORS(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.PLUSMINUS_OPERATORS, 0); }
	public EQUAL_OR_GREATER(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.EQUAL_OR_GREATER, 0); }
	public EQUAL_OR_LESS(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.EQUAL_OR_LESS, 0); }
	public LESS_THAN(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.LESS_THAN, 0); }
	public MORE_THAN(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.MORE_THAN, 0); }
	public EQUALITY(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.EQUALITY, 0); }
	public NEQUALITY(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.NEQUALITY, 0); }
	public LOGICAL_OPERATORS(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.LOGICAL_OPERATORS, 0); }
	public object(): ObjectContext | undefined {
		return this.tryGetRuleContext(0, ObjectContext);
	}
	public QMARK(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.QMARK, 0); }
	public COLON(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.COLON, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_expression; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterExpression) {
			listener.enterExpression(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitExpression) {
			listener.exitExpression(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitExpression) {
			return visitor.visitExpression(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class EvalContext extends ParserRuleContext {
	public EVAL_BEGIN(): TerminalNode { return this.getToken(jonscriptParser.EVAL_BEGIN, 0); }
	public expression(): ExpressionContext {
		return this.getRuleContext(0, ExpressionContext);
	}
	public EVAL_END(): TerminalNode { return this.getToken(jonscriptParser.EVAL_END, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_eval; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterEval) {
			listener.enterEval(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitEval) {
			listener.exitEval(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitEval) {
			return visitor.visitEval(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class InterpolatedContext extends ParserRuleContext {
	public INTERPOLATED_SEQUENCE(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.INTERPOLATED_SEQUENCE, 0); }
	public expression(): ExpressionContext | undefined {
		return this.tryGetRuleContext(0, ExpressionContext);
	}
	public interpolated(): InterpolatedContext | undefined {
		return this.tryGetRuleContext(0, InterpolatedContext);
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_interpolated; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterInterpolated) {
			listener.enterInterpolated(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitInterpolated) {
			listener.exitInterpolated(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitInterpolated) {
			return visitor.visitInterpolated(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class ExpressionListContext extends ParserRuleContext {
	public expressionItem(): ExpressionItemContext[];
	public expressionItem(i: number): ExpressionItemContext;
	public expressionItem(i?: number): ExpressionItemContext | ExpressionItemContext[] {
		if (i === undefined) {
			return this.getRuleContexts(ExpressionItemContext);
		} else {
			return this.getRuleContext(i, ExpressionItemContext);
		}
	}
	public COMMA(): TerminalNode[];
	public COMMA(i: number): TerminalNode;
	public COMMA(i?: number): TerminalNode | TerminalNode[] {
		if (i === undefined) {
			return this.getTokens(jonscriptParser.COMMA);
		} else {
			return this.getToken(jonscriptParser.COMMA, i);
		}
	}
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_expressionList; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterExpressionList) {
			listener.enterExpressionList(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitExpressionList) {
			listener.exitExpressionList(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitExpressionList) {
			return visitor.visitExpressionList(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


export class ExpressionItemContext extends ParserRuleContext {
	public expression(): ExpressionContext | undefined {
		return this.tryGetRuleContext(0, ExpressionContext);
	}
	public INHERITS(): TerminalNode | undefined { return this.tryGetToken(jonscriptParser.INHERITS, 0); }
	constructor(parent: ParserRuleContext | undefined, invokingState: number) {
		super(parent, invokingState);
	}
	// @Override
	public get ruleIndex(): number { return jonscriptParser.RULE_expressionItem; }
	// @Override
	public enterRule(listener: jonscriptListener): void {
		if (listener.enterExpressionItem) {
			listener.enterExpressionItem(this);
		}
	}
	// @Override
	public exitRule(listener: jonscriptListener): void {
		if (listener.exitExpressionItem) {
			listener.exitExpressionItem(this);
		}
	}
	// @Override
	public accept<Result>(visitor: jonscriptVisitor<Result>): Result {
		if (visitor.visitExpressionItem) {
			return visitor.visitExpressionItem(this);
		} else {
			return visitor.visitChildren(this);
		}
	}
}


