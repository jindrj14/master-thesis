// Generated from ./src/grammar/jonscript.g4 by ANTLR 4.7.3-SNAPSHOT


import { ParseTreeVisitor } from "antlr4ts/tree/ParseTreeVisitor";

import { ParentContext } from "./jonscriptParser";
import { ImportVarContext } from "./jonscriptParser";
import { DefaultImportContext } from "./jonscriptParser";
import { ImportedContext } from "./jonscriptParser";
import { ModuleContext } from "./jonscriptParser";
import { ClassdefContext } from "./jonscriptParser";
import { AssignmentContext } from "./jonscriptParser";
import { VarAssignmentContext } from "./jonscriptParser";
import { SingleVarAssignmentContext } from "./jonscriptParser";
import { PublicObjectContext } from "./jonscriptParser";
import { PublicApiContext } from "./jonscriptParser";
import { InheritanceContext } from "./jonscriptParser";
import { ObjectContext } from "./jonscriptParser";
import { FunctorContext } from "./jonscriptParser";
import { IgnoreableVarContext } from "./jonscriptParser";
import { TemplateContext } from "./jonscriptParser";
import { TemplateContentsContext } from "./jonscriptParser";
import { ExtendsTypeContext } from "./jonscriptParser";
import { EqualsTypeContext } from "./jonscriptParser";
import { TypeContext } from "./jonscriptParser";
import { AccessTypeContext } from "./jonscriptParser";
import { TypeEvalContext } from "./jonscriptParser";
import { TupleContext } from "./jonscriptParser";
import { TemplateTypeContext } from "./jonscriptParser";
import { ParamTypeContext } from "./jonscriptParser";
import { FunctionTypeContext } from "./jonscriptParser";
import { ObjectTypeContext } from "./jonscriptParser";
import { ObjectParamTypeContext } from "./jonscriptParser";
import { ExpressionContext } from "./jonscriptParser";
import { EvalContext } from "./jonscriptParser";
import { InterpolatedContext } from "./jonscriptParser";
import { ExpressionListContext } from "./jonscriptParser";
import { ExpressionItemContext } from "./jonscriptParser";


/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by `jonscriptParser`.
 *
 * @param <Result> The return type of the visit operation. Use `void` for
 * operations with no return type.
 */
export interface jonscriptVisitor<Result> extends ParseTreeVisitor<Result> {
	/**
	 * Visit a parse tree produced by `jonscriptParser.parent`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitParent?: (ctx: ParentContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.importVar`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitImportVar?: (ctx: ImportVarContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.defaultImport`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDefaultImport?: (ctx: DefaultImportContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.imported`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitImported?: (ctx: ImportedContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.module`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitModule?: (ctx: ModuleContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.classdef`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitClassdef?: (ctx: ClassdefContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.assignment`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAssignment?: (ctx: AssignmentContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.varAssignment`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitVarAssignment?: (ctx: VarAssignmentContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.singleVarAssignment`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitSingleVarAssignment?: (ctx: SingleVarAssignmentContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.publicObject`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitPublicObject?: (ctx: PublicObjectContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.publicApi`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitPublicApi?: (ctx: PublicApiContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.inheritance`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitInheritance?: (ctx: InheritanceContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.object`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitObject?: (ctx: ObjectContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.functor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitFunctor?: (ctx: FunctorContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.ignoreableVar`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitIgnoreableVar?: (ctx: IgnoreableVarContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.template`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitTemplate?: (ctx: TemplateContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.templateContents`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitTemplateContents?: (ctx: TemplateContentsContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.extendsType`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitExtendsType?: (ctx: ExtendsTypeContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.equalsType`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEqualsType?: (ctx: EqualsTypeContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.type`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitType?: (ctx: TypeContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.accessType`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAccessType?: (ctx: AccessTypeContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.typeEval`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitTypeEval?: (ctx: TypeEvalContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.tuple`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitTuple?: (ctx: TupleContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.templateType`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitTemplateType?: (ctx: TemplateTypeContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.paramType`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitParamType?: (ctx: ParamTypeContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.functionType`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitFunctionType?: (ctx: FunctionTypeContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.objectType`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitObjectType?: (ctx: ObjectTypeContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.objectParamType`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitObjectParamType?: (ctx: ObjectParamTypeContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.expression`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitExpression?: (ctx: ExpressionContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.eval`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEval?: (ctx: EvalContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.interpolated`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitInterpolated?: (ctx: InterpolatedContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.expressionList`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitExpressionList?: (ctx: ExpressionListContext) => Result;

	/**
	 * Visit a parse tree produced by `jonscriptParser.expressionItem`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitExpressionItem?: (ctx: ExpressionItemContext) => Result;
}

