import { parseJonScript as parseJonScriptTest } from './archive/utils/baseTestParser';
import { parseJonScript as parseJonScriptJS } from './archive/utils/javascriptTestParser';
import { parseJonScript as parseJonScriptTS } from './tsParser/index';
import { processTs } from "./tsParser/postprocessing/processTs";
export { parseJonScriptTest, parseJonScriptJS, parseJonScriptTS, processTs };