**Jonscript**

1. Introduction
    JonScript is a newly created language is transpiled into Typescript and Javascript.
    This package builds JonScript with the help of webpack.
    Jonscript is an OOP language with prototypal inheritance.
2. Build
    ```javascript
    // Webpack excerpt
      entry: "./src/index.jons",
        module: {
            rules: [
            {
                test: /\.jons$/,
                use: "jonscript",
                exclude: /node_modules/,
            },
            {
                test: /\.tsx?$/,
                use: "ts-loader",
                exclude: /node_modules/,
            },
            ],
        },
        resolve: {
            extensions: [ ".tsx", ".ts", ".js", ".jons" ],
        },
    ```
3. More information
    https://medium.com/@jindrj14/jonscript-a-better-javascript-a7ebe55707b9
