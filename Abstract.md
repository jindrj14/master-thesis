**Prototypal inheritance language transpiled into Typescript**


A number of concepts can be written in ECMAScript 2020 in different ways which may decrease how understandable the resulting code is. This ambiguity can introduce errors into ECMAScript applications. Example of these include multiple kinds of function definition, class definition or an empty/null value.

This work will study the creation of a new Typescript transpiled language which limits the number of ways of defining language constructs, while also simplifying object access, scope management, adding double dispatch and operator overloading.

Analyze other, similar works based in ECMAScript and propose syntax, semantics and parser for the new language. Design language syntax related to currently existing ECMAScript concepts.

Implement language parser that reduces resulting Typescript bundle after transpilation, create test suites and proper documentation. Use a focus group of experienced current Typescript and ECMAScript developers to measure whether the language is self-explanatory after short presentation from their point of view. Analyze compatibility with currently existing Typescript packages in NPM and design compatible package structures.
