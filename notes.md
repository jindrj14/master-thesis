1. Why should you care?
2. What does it do?

1. Abstract
2. Intro
3. Main ideas
4. Technical meat
5. Related work

1. Context - JS sucks
2. Gap - Plenty of frameworks, libraries, transpilers
3. Innovation - Transpiler which fixes concrete problems that others do not and is compatible

1. Start with concrete example?

Chapters:

1. JS is important (most used language, only way to write a program that user executes automatically)
2. Most people think they know JS
3. Most people are wrong
4. How it should work, vs. how does it work
5. What would

Principles:
1. Old to new
2. One paragraph, one point
3. Name your baby
4. Just in time
5. CGI
6. Main ideas
7. Compare with other work

The main idea of this paper is that there should be one, safe and concise way of writing JS code!
Give an example of the problem

```typescript

export const Module = { // var? let?
    Example: () => { // class? function()? class Example? function Example()?
        const map = [1, 2, 3].map; // new Array? Array?
        return {
            result: map(i => i + 1), // Throws an error, cannot call .map on undefined!
        };
    },
};

```

Jonscript way:

```

Test {
    Example() {
        map: [1, 2, 3].map,
        {
            result: map(i => i + 1), // This just works
        }
    }
}

```